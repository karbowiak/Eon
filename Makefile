.DEFAULT_GOAL := help

help:
	@echo "-- docker"
	@echo " start                         start containers"
	@echo " stop                          stop containers"
	@echo " restart                       restart containers"
	@echo " build                         build containers (start also does this step)"
	@echo ""
	@echo "-- commands"
	@echo " dev                           enter the php container"
	@echo " lint                          run php-cs-fixer"
	@echo " fix                           fix things php-cs-fixer found"

lint:
	@vendor/bin/php-cs-fixer fix --diff --dry-run

fix:
	@vendor/bin/php-cs-fixer fix

restart: | stop start
start:
	@docker-compose --project-name eon build php
	@docker-compose --project-name eon pull mongodb redis
	@docker-compose --project-name eon create server cron redisq queue mongodb redis
	@docker-compose --project-name eon start server cron redisq queue mongodb redis

stop:
	@docker-compose --project-name eon stop server cron redisq queue mongodb redis

build: | create
pull: | create
create:
	@docker-compose --project-name eon build php
	@docker-compose --project-name eon pull mongodb redis
	@docker-compose --project-name eon create server cron redisq queue mongodb redis

dev:
	@docker-compose --project-name eon run php sh
