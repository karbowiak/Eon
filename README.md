# Eon
## Warning
This is early beta, so things might break at any moment - beware of that.

# Eon
Eon is a killboard for EVE-Online.

It's built using Swoole, MongoDB and Redis as core technologies, but also depends on other things like Slim Framework and Symfony Console.
Eon is also its own HTTP Server, Queue and Cron runner/manager - all using Swoole.

## Requirements
- PHP7.4+
- Redis
- MongoDB
- PHP Extensions: MongoDB, Redis, Swoole, Yaml, Json

## Developer Requirements
- Docker or Kubernetes + Devspace

## Installation
#### Production
- Install MongoDB and Redis (Cluster or standalone)
- Clone source
- `composer install -o`
- Run `php bin/Eon update:sde` to update the SDE
- Run `php bin/eon backfill:prices` to backfill all the prices going back to 2016
- Setup systemd/supervisor to run `php bin/Eon server --host 0.0.0.0 --port 8080` for server
- Setup systemd/supervisor to run `php bin/Eon queue --workers 1` for the queue
- Setup systemd/supervisor to run `php bin/Eon cron` for the cron scheduler
- Setup systemd/supervisor to run `php bin/Eon redisq` for the redisq listener

#### Development
- Install Docker and Docker-Compose
- `docker-compose create redis mongodb && docker-compose start redis mongodb`
- You can either use `make dev` to open up a docker-container containing all the dependencies to run Eon, or you can install them locally and use `php bin/Eon` to start it
- `composer install -o`
- `php bin/Eon update:sde` to update the SDE
- `php bin/Eon backfill:prices` to backfill prices

#### Devspace
- Have a Kubernetes cluster setup and ready to roll
- Ensure you can talk to it with `kubectl` (`kubectl get po -A` if output you gucci)
- Install `devspace` - refer to https://devspace.sh/cli/docs/getting-started/installation for instructions
- Edit `devspace.yaml` and change the registry urls to match yours (Should ideally be defined via ENV in the future)
- Run `devspace build` to build the image
- Run `devspace dev` to initialize a deployment and a startup of the dev environment

#### Devspace Production
- Same steps as above
- For deployment to production `devspace deploy -p production` (This means restart injector won't work - so dev mode is going to be a little broken)

##### Devspace CLI
There are currently three commands to run via devspace, that will get you into the containers running in Kube.
Eon CLI: `devspace run cli`
MongoDB: `devspace run mongodb`
Redis: `devspace run redis`

## Usage
- Your code should be in the `app/` directory
- Models are automatically loaded from the Models/ directory
- Most class instantiation is defined by .yml files under settings.

## Server
Server uses Swoole to run, to start it simply do: `php bin/Eon server --host 0.0.0.0 --port 8080 --log`
- CSS, JS and Images are located in the /public directory (which is treated as /)

## LICENSE
> &copy; [MIT](./LICENSE) | 2020, Karbowiak/Eon
