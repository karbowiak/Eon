<?php

use Eon\Bootstrap;

$autoloaderPath = __DIR__ . '/../vendor/autoload.php';
if(!file_exists($autoloaderPath)) {
    throw new Exception('Error loading autoloader, please run composer install');
}
require($autoloaderPath);

/** @var Bootstrap $container */
$container = new Bootstrap();

// Init App
$container->initContainer();
$container->initSlim(true);
$app = $container->getSlim();

// Start Slim
$app->run();
