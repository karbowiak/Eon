<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;

class System extends ControllerInterface
{
    /**
     * @route GET | /system/[{systemID:[0-9]+}] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index()
    {
        $systemID = (int) $this->arguments->get('systemID');
        /** @var \Eon\Models\SolarSystems $systemModel */
        $systemModel = $this->container->get('model/solarsystems');

        $name = $systemModel->findOne(['solarSystemID' => $systemID])->get('solarSystemName');
        $menu = [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => '#',
                'Next Page' => '#',
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
            ],
        ];

        return $this->render('pages/system.twig', ['solarSystemID' => $systemID, 'menu' => $menu, 'name' => $name]);
    }
}
