<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Character extends ControllerInterface
{
    /**
     * @route GET | /character/{characterID:[0-9]+}[/page/{page:[0-9]+}[/]] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(): ResponseInterface
    {
        $characterID = (int) $this->arguments->get('characterID');
        $page = (int) $this->arguments->get('page', 1);
        $collection = $this->container->get('model/characters');
        $name = $collection->findOne(['characterID' => $characterID])['characterName'];
        return $this->render('pages/character.twig', ['characterID' => $characterID, 'menu' => $this->generateMenu($characterID, $page), 'name' => $name, 'page' => $page]);
    }

    /**
     * @route GET | /character/{characterID:[0-9]+}/history | history
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function history(): ResponseInterface
    {
        $characterID = (int) $this->arguments->get('characterID');
        $collection = $this->container->get('model/characters');
        $name = $collection->findOne(['characterID' => $characterID])->get('characterName');
        return $this->render('pages/character/history.twig', ['characterID' => $characterID, 'menu' => $this->generateMenu($characterID), 'name' => $name]);
    }

    private function generateMenu(int $characterID = 0, int $page = 1)
    {
        $previousPage = $page > 1 ? explode('/page/', $this->request->getUri()->getPath())[0] . '/page/' . ($page - 1) : false;
        $nextPage = $page >= 1 ? explode('/page/', $this->request->getUri()->getPath())[0] . '/page/' . ($page + 1) : false;
        return [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => $previousPage !== false ? $previousPage : '#',
                'Next Page' => $nextPage !== false ? $nextPage : '#',
            ],
            'Information' => [
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
                'History' => "/character/{$characterID}/history/",
            ],
        ];
    }
}
