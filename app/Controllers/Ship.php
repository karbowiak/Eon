<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;

class Ship extends ControllerInterface
{
    /**
     * @route GET | /ship/[{shipID:[0-9]+}] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index()
    {
        $shipID = (int) $this->arguments->get('shipID');
        /** @var \Eon\Models\TypeIDs $shipModel */
        $shipModel = $this->container->get('model/typeids');

        $name = $shipModel->findOne(['typeID' => $shipID])->get('typeName');
        $menu = [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => '#',
                'Next Page' => '#',
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
            ],
        ];

        return $this->render('pages/ship.twig', ['shipTypeID' => $shipID, 'menu' => $menu, 'name' => $name]);
    }
}
