<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;

class Battles extends ControllerInterface
{
    /**
     * @route GET | /battles[/page/{page:[0-9]+}] | showBattles
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function showBattles()
    {
        $page = (int) $this->arguments->get('page');
        if ($page === 1) {
            $menu = [
                'Navigation' => [
                    'Next Page' => $this->getFullHost() . 'battles/page/' . ($page + 1) . '/',
                ],
            ];
        } else {
            $menu = [
                'Navigation' => [
                    'Previous Page' => $this->getFullHost() . 'battles/page/' . ($page - 1) . '/',
                    'Next Page' => $this->getFullHost() . 'battles/page/' . ($page + 1) . '/',
                ],
            ];
        }

        return $this->render('/pages/battles.twig', ['menu' => $menu]);
    }

    /**
     * @route GET | /battles/{battleID:[a-zA-Z0-9]+} | showBattle
     */
    public function showBattle()
    {
        $battleID = $this->arguments->get('battleID');
        /** @var \Eon\Models\Battles $battleModel */
        $battleModel = $this->container->get('model/battles');
        $data = $battleModel->findOne(['battleID' => $battleID]);
        return $this->json($data);
    }
}
