<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class About extends ControllerInterface
{
    /**
     * @route GET | /about | about
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function about(): ResponseInterface
    {
        return $this->render('/pages/about.twig', ['menu' => []]);
    }

    /**
     * @route GET | /about/stats | stats
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function stats(): ResponseInterface
    {
        /** @var \Eon\Models\Killmails $killmail */
        $killmail = $this->container->get('model/killmails');
        $esiKillmails = $this->container->get('model/esikillmails');
        $characters = $this->container->get('model/characters');
        $corporations = $this->container->get('model/corporations');
        $alliances = $this->container->get('model/alliances');
        $battles = $this->container->get('model/battles');
        $prices = $this->container->get('model/prices');
        $comments = $this->container->get('model/comments');

        $stats = ['count' => [
            'killmails' => $killmail->count(),
            'characters' => $characters->count(),
            'corporations' => $corporations->count(),
            'alliances' => $alliances->count(),
            'battles' => $battles->count(),
            'prices' => $prices->count(),
            'comments' => $comments->count(),
            'esikillmails' => $esiKillmails->count(),
        ]];
        return $this->render('/pages/about/stats.twig', ['stats' => $stats]);
    }

    /**
     * @route GET | /about/faq | faq
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function faq(): ResponseInterface
    {
        return $this->render('/pages/about/faq.twig', ['menu' => []]);
    }
}
