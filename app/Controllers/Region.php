<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;

class Region extends ControllerInterface
{
    /**
     * @route GET | /region/[{regionID:[0-9]+}] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index()
    {
        $regionID = (int) $this->arguments->get('regionID');
        /** @var \Eon\Models\Alliances $regionModel */
        $regionModel = $this->container->get('model/regions');

        $name = $regionModel->findOne(['regionID' => $regionID])->get('regionName');
        $menu = [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => '#',
                'Next Page' => '#',
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
            ],
        ];

        return $this->render('pages/region.twig', ['regionID' => $regionID, 'menu' => $menu, 'name' => $name]);
    }
}
