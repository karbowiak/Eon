<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Index extends ControllerInterface
{
    /**
     * @route GET | / | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(): ResponseInterface
    {
        return $this->render('/pages/frontpage.twig', ['menu' => []]);
    }

    /**
     * @route GET | /kill/{killID:[0-9]+} | kill
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function kill(): ResponseInterface
    {
        return $this->render('/pages/kill.twig', ['killID' => $this->arguments->get('killID'), 'menu' => [], 'related' => false]);
    }
}
