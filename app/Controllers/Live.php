<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Live extends ControllerInterface
{
    /**
     * @route GET | /live | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(): ResponseInterface
    {
        return $this->render('/pages/live.twig', ['menu' => []]);
    }
}
