<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Corporation extends ControllerInterface
{
    /**
     * @route GET | /corporation/[{corporationID:[0-9]+}] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(): ResponseInterface
    {
        $corporationID = (int) $this->arguments->get('corporationID');
        $collection = $this->container->get('model/corporations');
        $name = $collection->findOne(['corporationID' => $corporationID])->get('corporationName');
        $menu = [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => '#',
                'Next Page' => '#',
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
            ],
        ];
        return $this->render('pages/corporation.twig', ['corporationID' => $corporationID, 'menu' => $menu, 'name' => $name]);
    }
}
