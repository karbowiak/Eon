<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Alliance extends ControllerInterface
{
    /**
     * @route GET | /alliance/[{allianceID:[0-9]+}] | index
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function index(): ResponseInterface
    {
        $allianceID = (int) $this->arguments->get('allianceID');
        /** @var \Eon\Models\Alliances $allianceModel */
        $allianceModel = $this->container->get('model/alliances');

        $name = $allianceModel->findOne(['allianceID' => $allianceID])->get('allianceName');
        $menu = [
            'EVE-Board' => '#',
            'EVE-Search' => '#',
            'EVE-Gate' => '#',
            'EVEWho' => '#',
            'EVE-Hunt' => '#',
            'Navigation' => [
                'Previous Page' => '#',
                'Next Page' => '#',
                'Kills' => '#',
                'Losses' => '#',
                'Solo' => '#',
                'Trophies' => '#',
                'Top' => '#',
                'Ranks' => '#',
                'Stats' => '#',
            ],
        ];

        return $this->render('pages/alliance.twig', ['allianceID' => $allianceID, 'menu' => $menu, 'name' => $name]);
    }
}
