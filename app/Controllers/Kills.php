<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Kills extends ControllerInterface
{
    /**
     * @route GET | /latest[/{page:[0-9]+}] | latest
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function latest(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Latest', 'type' => 'latest', 'page' => $page]);
    }

    /**
     * @route GET | /bigkills[/{page:[0-9]+}] | bigKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function bigKills(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Big Kills', 'type' => 'bigkills', 'page' => $page]);
    }

    /**
     * @route GET | /wspace[/{page:[0-9]+}] | wormholeSpace
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function wormholeSpace(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Wormhole Space', 'type' => 'wspace', 'page' => $page]);
    }

    /**
     * @route GET | /abyssal[/{page:[0-9]+}] | abyssalSpace
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function abyssalSpace(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Abyssal Space', 'type' => 'abyssal', 'page' => $page]);
    }

    /**
     * @route GET | /highsec[/{page:[0-9]+}] | highSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function highSec(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'High Sec', 'type' => 'highsec', 'page' => $page]);
    }

    /**
     * @route GET | /lowsec[/{page:[0-9]+}] | lowSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function lowSec(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Low Sec', 'type' => 'lowsec', 'page' => $page]);
    }

    /**
     * @route GET | /nullsec[/{page:[0-9]+}] | nullSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function nullSec(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Null Sec', 'type' => 'nullsec', 'page' => $page]);
    }

    /**
     * @route GET | /solo[/{page:[0-9]+}] | soloKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function soloKills(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Solo Kills', 'type' => 'solo', 'page' => $page]);
    }

    /**
     * @route GET | /npc[/{page:[0-9]+}] | npcKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function npcKills(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'NPC Kills', 'type' => 'npc', 'page' => $page]);
    }

    /**
     * @route GET | /5b[/{page:[0-9]+}] | fiveBKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function fiveBKills(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => '+5b Kills', 'type' => '5b', 'page' => $page]);
    }

    /**
     * @route GET | /10b[/{page:[0-9]+}] | tenBKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function tenBKills(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => '+10b Kills', 'type' => '10b', 'page' => $page]);
    }

    /**
     * @route GET | /citadels[/{page:[0-9]+}] | citadels
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function citadels(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Citadels', 'type' => 'citadels', 'page' => $page]);
    }

    /**
     * @route GET | /t1[/{page:[0-9]+}] | t1
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function t1(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'T1 Ships', 'type' => 't1', 'page' => $page]);
    }

    /**
     * @route GET | /t2[/{page:[0-9]+}] | t2
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function t2(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'T2 Ships', 'type' => 't2', 'page' => $page]);
    }

    /**
     * @route GET | /t3[/{page:[0-9]+}] | t3
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function t3(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'T3 Ships', 'type' => 't3', 'page' => $page]);
    }

    /**
     * @route GET | /frigates[/{page:[0-9]+}] | frigates
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function frigates(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Frigates', 'type' => 'frigates', 'page' => $page]);
    }

    /**
     * @route GET | /destroyers[/{page:[0-9]+}] | destroyers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function destroyers(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Destroyers', 'type' => 'destroyers', 'page' => $page]);
    }

    /**
     * @route GET | /cruisers[/{page:[0-9]+}] | cruisers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function cruisers(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Cruisers', 'type' => 'cruisers', 'page' => $page]);
    }

    /**
     * @route GET | /battlecruisers[/{page:[0-9]+}] | battleCruisers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function battleCruisers(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'BattleCruisers', 'type' => 'battlecruisers', 'page' => $page]);
    }

    /**
     * @route GET | /battleships[/{page:[0-9]+}] | battleships
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function battleships(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'BattleShips', 'type' => 'battleships', 'page' => $page]);
    }

    /**
     * @route GET | /capitals[/{page:[0-9]+}] | capitals
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function capitals(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Capitals', 'type' => 'capitals', 'page' => $page]);
    }

    /**
     * @route GET | /freighters[/{page:[0-9]+}] | freighters
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function freighters(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Freighters', 'type' => 'freighters', 'page' => $page]);
    }

    /**
     * @route GET | /supercarriers[/{page:[0-9]+}] | superCarriers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function superCarriers(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'SuperCarriers', 'type' => 'supercarriers', 'page' => $page]);
    }

    /**
     * @route GET | /titans[/{page:[0-9]+}] | titans
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function titans(): ResponseInterface
    {
        $page = (int) $this->arguments->get('page');
        return $this->render('pages/kills.twig', ['name' => 'Titans', 'type' => 'titans', 'page' => $page]);
    }
}
