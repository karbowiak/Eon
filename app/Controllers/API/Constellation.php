<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Constellation extends ControllerInterface
{
    /** @var \Eon\Models\Constellations */
    protected $constellation;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->constellation = $this->container->get('model/constellations');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->constellation->collection->count();
        return $this->json(['constellationCount' => $count], 360);
    }

    /**
     * @route GET | /information/{constellationID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->constellation->findOne(['constellationID' => $this->arguments->get('constellationID')], ['projection' => ['_id' => 0]]);
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['constellation']);
    }
}
