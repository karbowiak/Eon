<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class SolarSystem extends ControllerInterface
{
    /** @var \Eon\Models\SolarSystems */
    protected $solarsystem;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->solarsystem = $this->container->get('model/solarsystems');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->solarsystem->collection->count();
        return $this->json(['solarSystemCount' => $count], 360);
    }

    /**
     * @route GET | /information/{solarSystemID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->solarsystem->find(['solarSystemID' => $this->arguments->get('solarSystemID')], ['projection' => ['_id' => 0]]);
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['system']);
    }
}
