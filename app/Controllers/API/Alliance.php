<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Alliance extends ControllerInterface
{
    /** @var \Eon\Models\Alliances */
    protected $alliance;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->alliance = $this->container->get('model/alliances');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->alliance->count();
        return $this->json(['allianceCount' => $count], 360);
    }

    /**
     * @route GET | /information/{allianceID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->alliance->getInformation($this->arguments->get('allianceID'));
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['alliance'], 360);
    }

    /**
     * @route GET | /members/{allianceID:[0-9]+} | members
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function members()
    {
        $members = $this->alliance->getMembers($this->arguments->get('allianceID'));
        return $this->json($members->toArray(), 360);
    }

    /**
     * @route GET | /top/characters/{allianceID:[0-9]+}[/{limit:[0-9]+}] | topCharacters
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCharacters()
    {
        $data = $this->top->topCharacters('allianceID', $this->arguments->get('allianceID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/corporations/{allianceID:[0-9]+}[/{limit:[0-9]+}] | topCorporations
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCorporations()
    {
        $data = $this->top->topCorporations('allianceID', $this->arguments->get('allianceID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/ships/{allianceID:[0-9]+}[/{limit:[0-9]+}] | topShips
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topShips()
    {
        $data = $this->top->topShips('allianceID', $this->arguments->get('allianceID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/systems/{allianceID:[0-9]+}[/{limit:[0-9]+}] | topSystems
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topSystems()
    {
        $data = $this->top->topSystems('allianceID', $this->arguments->get('allianceID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/regions/{allianceID:[0-9]+}[/{limit:[0-9]+}] | topRegions
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topRegions()
    {
        $data = $this->top->topRegions('allianceID', $this->arguments->get('allianceID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }
}
