<?php

namespace Eon\Controllers\API;

use DateTime;
use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Battles extends ControllerInterface
{
    /** @var \Eon\Models\Battles */
    protected $battles;

    public function __construct(
        Bootstrap $bootstrap
    ) {
        parent::__construct($bootstrap);
        $this->battles = $bootstrap->get('model/battles');
    }

    /**
     * @route GET | /{battleID:[a-zA-Z0-9]+} | getBattle
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBattle(): ResponseInterface
    {
        $battleID = $this->arguments->get('battleID');
        $data = $this->battles->findOne(['battleID' => (int) $battleID], ['projection' => ['_id' => 0]]);
        if ($data->isNotEmpty()) {
            $teamRed = $data->get('teamRed');
            $teamBlue = $data->get('teamBlue');

            $data['killCount'] = count((array) $teamRed->kills) + count((array) $teamBlue->kills);
            $data['startTime'] = date(DateTime::ATOM, (string) $data->get('startTime') / 1000);
            $data['endTime'] = date(DateTime::ATOM, (string) $data->get('endTime') / 1000);
        }

        return $this->json($data);
    }

    /**
     * @route GET | /list/[page/{page:[0-9]+}] | getBattles
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBattles(): ResponseInterface
    {
        $limit = 100;
        $page = $this->arguments->get('page') ?? 1;
        $offset = $limit * ($page - 1);
        $battles = [];

        $data = $this->battles->find([], ['sort' => ['startTime' => -1], 'projection' => ['_id' => 0, 'killData' => 0], 'limit' => $limit, 'skip' => $offset]);
        foreach ($data as $d) {
            $tmp = [];
            $tmp['battleID'] = $d->battleID;
            $tmp['startTime'] = date(DateTime::ATOM, (string) $d->startTime / 1000);
            $tmp['endTime'] = date(DateTime::ATOM, (string) $d->endTime / 1000);
            $tmp['solarSystemID'] = $d->solarSystemInfo->solarSystemID;
            $tmp['solarSystemName'] = $d->solarSystemInfo->solarSystemName;
            $tmp['regionID'] = $d->solarSystemInfo->regionID;
            $tmp['regionName'] = $d->solarSystemInfo->regionName;
            $tmp['killCount'] = count((array) $d->teamBlue->kills) + count((array) $d->teamRed->kills);
            $tmp['involvedCount'] = [
                'characters' => count((array) $d->teamRed->characters) + count((array) $d->teamBlue->characters),
                'corporations' => count((array) $d->teamRed->corporations) + count((array) $d->teamBlue->corporations),
                'alliances' => count((array) $d->teamRed->alliances) + count((array) $d->teamBlue->alliances),
            ];

            if (!empty($d->teamRed->alliances) && !empty($d->teamBlue->alliances)) {
                $tmp['involvedAlliances'] = [
                    'teamRed' => $d->teamRed->alliances,
                    'teamBlue' => $d->teamBlue->alliances,
                ];
            } elseif (!empty($d->teamRed->corporations) && !empty($d->teamBlue->corporations)) {
                $tmp['involvedCorporations'] = [
                    'teamRed' => $d->teamRed->corporations,
                    'teamBlue' => $d->teamBlue->corporations,
                ];
            }

            $battles[] = $tmp;
        }
        return $this->json($battles);
    }
}
