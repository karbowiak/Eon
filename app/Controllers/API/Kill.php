<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Kill extends ControllerInterface
{
    /** @var \Eon\Models\Killmails */
    protected $killmail;
    /** @var \Eon\Helpers\Redis */
    protected $redis;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->killmail = $this->container->get('model/killmails');
        $this->redis = $this->container->get('redis');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        return $this->json(['killCount' => $this->killmail->collection->count()]);
    }

    /**
     * @route GET | /add | addKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function addKill()
    {
        return $this->json(['error' => 'not implemented yet']);
    }

    /**
     * @route GET | /killID/{killID:[0-9]+} | getKillByID
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getKillByID()
    {
        $killID = (int) $this->arguments->get('killID');
        $md5 = md5('api/kill/killID/' . $killID);
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5));
        }

        $data = $this->killmail->findOne(['killID' => $killID], ['projection' => ['_id' => 0]]);
        $data['killTime'] = date(\DateTime::ATOM, (string) $data['killTime'] / 1000);
        $this->redis->set($md5, $data, 3600);

        return $this->json($data->toArray());
    }

    /**
     * @route GET | /hash/{hash:[a-zA-Z0-9]+} | getKillByHash
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getKillByHash()
    {
        $killHash = $this->arguments->get('killHash');
        $md5 = md5($killHash);
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5));
        }

        $data = $this->killmail->findOne(['hash' => $killHash], ['projection' => ['_id' => 0]]);
        $data['killTime'] = date(\DateTime::ATOM, strtotime($data['killTime']) / 1000);
        $this->redis->set($md5, $data, 3600);

        return $this->json($data->toArray());
    }

    /**
     * @route GET | /dump/{date:[0-9]+} | getKillsByDate
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getKillsByDate()
    {
        $timestamp = $this->arguments->get('timestamp');
        $md5 = md5($timestamp);
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5));
        }

        $startDate = $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime($timestamp)));
        $endDate = $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime($timestamp) + 86400));

        $data = $this->killmail->aggregate([
            ['$match' => ['killTime' => ['$gte' => $startDate, '$lt' => $endDate]]],
            ['$project' => ['_id' => 0, 'killID' => 1, 'crestHash' => 1]],
            ['$sort' => ['killID' => -1]],
        ]);

        $this->redis->set($md5, $data, 3600);
        return $this->json($data->toArray());
    }
}
