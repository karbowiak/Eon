<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Stats extends ControllerInterface
{
    /** @var \Eon\Helpers\Redis */
    protected $redis;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->redis = $this->container->get('redis');
    }

    /**
     * @route GET | /top10characters[/{allTime:[0-1]}] | top10Characters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function top10Characters(): \Psr\Http\Message\ResponseInterface
    {
        if ($this->redis->has(md5('top10CharactersStatsAPI'))) {
            return $this->json($this->redis->get(md5('top10CharactersStatsAPI')));
        }
        return $this->json([]);
    }

    /**
     * @route GET | /top10corporations[/{allTime:[0-1]}] | top10Corporations
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function top10Corporations(): \Psr\Http\Message\ResponseInterface
    {
        if ($this->redis->has(md5('top10CorporationsStatsAPI'))) {
            return $this->json($this->redis->get(md5('top10CorporationsStatsAPI')));
        }
        return $this->json([]);
    }

    /**
     * @route GET | /top10alliances[/{allTime:[0-1]}] | top10Alliances
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function top10Alliances(): \Psr\Http\Message\ResponseInterface
    {
        if ($this->redis->has(md5('top10AlliancesStatsAPI'))) {
            return $this->json($this->redis->get(md5('top10AlliancesStatsAPI')));
        }
        return $this->json([]);
    }

    /**
     * @route GET | /top10solarsystems[/{allTime:[0-1]}] | top10SolarSystems
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function top10SolarSystems(): \Psr\Http\Message\ResponseInterface
    {
        if ($this->redis->has(md5('top10SolarSystemsStatsAPI'))) {
            return $this->json($this->redis->get(md5('top10SolarSystemsStatsAPI')));
        }
        return $this->json([]);
    }

    /**
     * @route GET | /top10regions[/{allTime:[0-1]}] | top10Regions
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function top10Regions(): \Psr\Http\Message\ResponseInterface
    {
        if ($this->redis->has(md5('top10RegionsStatsAPI'))) {
            return $this->json($this->redis->get(md5('top10RegionsStatsAPI')));
        }
        return $this->json([]);
    }

    /**
     * @route GET | /mostvaluablekillslast7days[/{limit:[0-9]+}] | mostValuableKillsOverTheLast7Days
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function mostValuableKillsOverTheLast7Days(): \Psr\Http\Message\ResponseInterface
    {
        $limit = $this->arguments->get('limit', 10);
        $killmails = $this->container->get('model/killmails');
        $md5 = md5('mostValuableKillsOverTheLast7Days' . $limit);
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $data = $killmails->find(
            ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))]],
            ['sort' => ['totalValue' => -1], 'limit' => (int) $limit, 'projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]]
        )->toArray();
        $this->redis->set($md5, $data, 3600);
        return $this->json($data);
    }

    /**
     * @route GET | /sevendaykillcount | sevenDayKillCount
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function sevenDayKillCount(): \Psr\Http\Message\ResponseInterface
    {
        $killmails = $this->container->get('model/killmails');
        $md5 = md5('sevenDayKillCount');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $data = $killmails->find(['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))]])->toArray();
        $this->redis->set($md5, $data, 3600);
        return $this->json($data);
    }

    /**
     * @route GET | /activeentities[/{allTime:[0-1]}] | activeEntities
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeEntities(): \Psr\Http\Message\ResponseInterface
    {
        $data = [
            'activeCharacters' => json_decode($this->activeCharacters(), false),
            'activeCorporations' => json_decode($this->activeCorporations(), false),
            'activeAlliances' => json_decode($this->activeAlliances(), false),
            'activeShipTypes' => json_decode($this->activeShipTypes(), false),
            'activeSolarSystems' => json_decode($this->activeSolarSystems(), false),
            'activeRegions' => json_decode($this->activeRegions(), false),
        ];

        return $this->json($data);
    }

    /**
     * @route GET | /activecharacters[/{allTime:[0-1]}] | activeCharacters
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeCharacters(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeCharacters');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.characterID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$attackers.characterID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeCharacters'] = count($data);

        $this->redis->set($md5, count($data), 60);
        return $this->json($returnData);
    }

    /**
     * @route GET | /activecorporations[/{allTime:[0-1]}] | activeCorporations
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeCorporations(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeCorporations');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.corporationID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$attackers.corporationID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeCorporations'] = count($data);
        $this->redis->set($md5, count($data), 60);

        return $this->json($returnData);
    }

    /**
     * @route GET | /activealliances[/{allTime:[0-1]}] | activeAlliances
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeAlliances(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeAlliances');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.allianceID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$attackers.allianceID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeAlliances'] = count($data);
        $this->redis->set($md5, count($data), 60);

        return $this->json($returnData);
    }

    /**
     * @route GET | /activeshiptypes[/{allTime:[0-1]}] | activeShipTypes
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeShipTypes(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeShipTypes');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.shipTypeID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$attackers.shipTypeID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeShipTypes'] = count($data);
        $this->redis->set($md5, count($data), 60);

        return $this->json($returnData);
    }

    /**
     * @route GET | /activesolarsystems[/{allTime:[0-1]}] | activeSolarSystems
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeSolarSystems(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeSolarSystems');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'solarSystemID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$solarSystemID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeSolarSystems'] = count($data);
        $this->redis->set($md5, count($data), 60);

        return $this->json($returnData);
    }

    /**
     * @route GET | /activeregions[/{allTime:[0-1]}] | activeRegions
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function activeRegions(): \Psr\Http\Message\ResponseInterface
    {
        $md5 = md5('activeRegions');
        if ($this->redis->has($md5)) {
            return $this->json($this->redis->get($md5, []));
        }

        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'regionID' => ['$ne' => 0]]];
        $killmails = $this->container->get('model/killmails');
        $data = $killmails->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$regionID']],
            ['$group' => ['_id' => 1, 'count' => ['$sum' => 1]]],
        ],
            ['allowDiskUse' => true, 'maxTimeMS' => 30000]
        )->toArray();

        $returnData['activeRegions'] = count($data);
        $this->redis->set($md5, count($data), 60);

        return $this->json($returnData);
    }
}
