<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;
use Eon\Models\Comments as CommentsModel;

class Comments extends ControllerInterface
{
    public CommentsModel $comments;

    public function __construct(
        Bootstrap $bootstrap
    ) {
        parent::__construct($bootstrap);
        $this->comments = $bootstrap->get('model/comments');
    }

    /**
     * @route POST | /post | post
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post(): ResponseInterface
    {
        $data = [
            'killID' => $this->getPostParam('killID'),
            'name' => $this->getPostParam('name'),
            'comment' => $this->getPostParam('comment'),
            'createdAt' => new UTCDateTime(time() * 1000),
        ];
        $this->comments->setData($data);
        try {
            $this->comments->collection->insertOne($data);
        } catch (\Exception $e) {}

        return $this->json($data);
    }

    /**
     * @route GET | /get/{killID:[0-9]+} | get
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(): ResponseInterface
    {
        $killID = $this->arguments->get('killID');
        return $this->json($this->comments->find(['killID' => $killID]));
    }

    /**
     * @route GET | /all | all
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function all(): ResponseInterface
    {
        return $this->json($this->comments->find());
    }
}
