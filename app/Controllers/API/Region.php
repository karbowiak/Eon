<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Region extends ControllerInterface
{
    /** @var \Eon\Models\Regions */
    protected $region;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->region = $this->container->get('model/regions');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->region->collection->count();
        return $this->json(['regionCount' => $count], 360);
    }

    /**
     * @route GET | /information/{regionID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->region->find(['regionID' => $this->arguments->get('regionID')], ['projection' => ['_id' => 0]]);
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['region']);
    }
}
