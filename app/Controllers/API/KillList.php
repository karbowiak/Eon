<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class KillList extends ControllerInterface
{
    /** @var \Eon\Models\KillList */
    protected $killlist;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->killlist = $this->container->get('model/killlist');
    }

    /**
     * @route GET | /latest[/page/{page:[0-9]+}] | getLatest
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLatest()
    {
        return $this->json($this->killlist->getLatest($this->arguments->get('page')));
    }

    /**
     * @route GET | /bigkills[/page/{page:[0-9]+}] | getBigKills
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBigKills()
    {
        return $this->json($this->killlist->getBigKills($this->arguments->get('page')));
    }

    /**
     * @route GET | /wspace[/page/{page:[0-9]+}] | getWSpace
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getWSpace()
    {
        return $this->json($this->killlist->getWSpace($this->arguments->get('page')));
    }

    /**
     * @route GET | /abyssal[/page/{page:[0-9]+}] | getAbyssal
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAbyssal()
    {
        return $this->json($this->killlist->getAbyssal($this->arguments->get('page')));
    }

    /**
     * @route GET | /highsec[/page/{page:[0-9]+}] | getHighSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getHighSec()
    {
        return $this->json($this->killlist->getHighSec($this->arguments->get('page')));
    }

    /**
     * @route GET | /lowsec[/page/{page:[0-9]+}] | getLowSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getLowSec()
    {
        return $this->json($this->killlist->getLowSec($this->arguments->get('page')));
    }

    /**
     * @route GET | /nullsec[/page/{page:[0-9]+}] | getNullSec
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getNullSec()
    {
        return $this->json($this->killlist->getNullSec($this->arguments->get('page')));
    }

    /**
     * @route GET | /solo[/page/{page:[0-9]+}] | getSolo
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSolo()
    {
        return $this->json($this->killlist->getSolo($this->arguments->get('page')));
    }

    /**
     * @route GET | /npc[/page/{page:[0-9]+}] | getNPC
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getNPC()
    {
        return $this->json($this->killlist->getNPC($this->arguments->get('page')));
    }

    /**
     * @route GET | /5b[/page/{page:[0-9]+}] | get5b
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get5b()
    {
        return $this->json($this->killlist->get5b($this->arguments->get('page')));
    }

    /**
     * @route GET | /10b[/page/{page:[0-9]+}] | get10b
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get10b()
    {
        return $this->json($this->killlist->get10b($this->arguments->get('page')));
    }

    /**
     * @route GET | /citadels[/page/{page:[0-9]+}] | getCitadels
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCitadels()
    {
        return $this->json($this->killlist->getCitadels($this->arguments->get('page')));
    }

    /**
     * @route GET | /t1[/page/{page:[0-9]+}] | getT1
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getT1()
    {
        return $this->json($this->killlist->getT1($this->arguments->get('page')));
    }

    /**
     * @route GET | /t2[/page/{page:[0-9]+}] | getT2
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getT2()
    {
        return $this->json($this->killlist->getT2($this->arguments->get('page')));
    }

    /**
     * @route GET | /t3[/page/{page:[0-9]+}] | getT3
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getT3()
    {
        return $this->json($this->killlist->getT3($this->arguments->get('page')));
    }

    /**
     * @route GET | /frigates[/page/{page:[0-9]+}] | getFrigates
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFrigateS()
    {
        return $this->json($this->killlist->getFrigates($this->arguments->get('page')));
    }

    /**
     * @route GET | /destroyers[/page/{page:[0-9]+}] | getDestroyers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDestroyers()
    {
        return $this->json($this->killlist->getDestroyers($this->arguments->get('page')));
    }

    /**
     * @route GET | /cruisers[/page/{page:[0-9]+}] | getCruisers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCruisers()
    {
        return $this->json($this->killlist->getCruisers($this->arguments->get('page')));
    }

    /**
     * @route GET | /battlecruisers[/page/{page:[0-9]+}] | getBattleCruisers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBattleCruisers()
    {
        return $this->json($this->killlist->getBattleCruisers($this->arguments->get('page')));
    }

    /**
     * @route GET | /battleships[/page/{page:[0-9]+}] | getBattleShips
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBattleShips()
    {
        return $this->json($this->killlist->getBattleShips($this->arguments->get('page')));
    }

    /**
     * @route GET | /capitals[/page/{page:[0-9]+}] | getCapitals
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCapitals()
    {
        return $this->json($this->killlist->getCapitals($this->arguments->get('page')));
    }

    /**
     * @route GET | /freighters[/page/{page:[0-9]+}] | getFreighters
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFreighters()
    {
        return $this->json($this->killlist->getFreighters($this->arguments->get('page')));
    }

    /**
     * @route GET | /supercarriers[/page/{page:[0-9]+}] | getSuperCarriers
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSuperCarriers()
    {
        return $this->json($this->killlist->getSuperCarriers($this->arguments->get('page')));
    }

    /**
     * @route GET | /titans[/page/{page:[0-9]+}] | getTitans
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getTitans()
    {
        return $this->json($this->killlist->getTitans($this->arguments->get('page')));
    }
}
