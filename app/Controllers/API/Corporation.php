<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Corporation extends ControllerInterface
{
    /** @var \Eon\Models\Corporations */
    protected $corporation;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->corporation = $this->container->get('model/corporations');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->corporation->count();
        return $this->json(['corporationCount' => $count], 360);
    }

    /**
     * @route GET | /information/{corporationID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->corporation->getInformation($this->arguments->get('corporationID'));
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['corporation'], 360);
    }

    /**
     * @route GET | /members/{corporationID:[0-9]+} | members
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function members()
    {
        $members = $this->corporation->getMembers($this->arguments->get('corporationID'));
        return $this->json($members->toArray(), 360);
    }

    /**
     * @route GET | /top/characters/{corporationID:[0-9]+}[/{limit:[0-9]+}] | topCharacters
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCharacters()
    {
        $data = $this->top->topCharacters('corporationID', $this->arguments->get('corporationID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/alliances/{corporationID:[0-9]+}[/{limit:[0-9]+}] | topAlliances
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topAlliances()
    {
        $data = $this->top->topAlliances('corporationID', $this->arguments->get('corporationID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/ships/{corporationID:[0-9]+}[/{limit:[0-9]+}] | topShips
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topShips()
    {
        $data = $this->top->topShips('corporationID', $this->arguments->get('corporationID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/systems/{corporationID:[0-9]+}[/{limit:[0-9]+}] | topSystems
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topSystems()
    {
        $data = $this->top->topSystems('corporationID', $this->arguments->get('corporationID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/regions/{corporationID:[0-9]+}[/{limit:[0-9]+}] | topRegions
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topRegions()
    {
        $data = $this->top->topRegions('corporationID', $this->arguments->get('corporationID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }
}
