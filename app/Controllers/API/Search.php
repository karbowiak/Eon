<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Search extends ControllerInterface
{
    /** @var \Eon\Models\Search */
    protected $search;

    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->search = $this->container->get('model/search');
    }

    /**
     * @route GET | /all/{searchTerm:[^\\/]+}[/] | findAny
     * @param string $searchTerm
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findAny(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['alliance', 'corporation', 'character', 'region', 'system', 'item']));
    }

    /**
     * @route GET | /character/{searchTerm:[^\\/]+}[/] | findCharacter
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findCharacter(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['character'])['character']);
    }

    /**
     * @route GET | /corporation/{searchTerm:[^\\/]+}[/] | findCorporation
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findCorporation(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['corporation'])['corporation']);
    }

    /**
     * @route GET | /alliance/{searchTerm:[^\\/]+}[/] | findAlliance
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findAlliance(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['alliance'])['alliance']);
    }

    /**
     * @route GET | /faction/{searchTerm:[^\\/]+}[/] | findFaction
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findFaction(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['faction'])['faction']);
    }

    /**
     * @route GET | /system/{searchTerm:[^\\/]+}[/] | findSolarSystem
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findSolarSystem(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['system'])['system']);
    }

    /**
     * @route GET | /region/{searchTerm:[^\\/]+}[/] | findRegion
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findRegion(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['region'])['region']);
    }

    /**
     * @route GET | /celestial/{searchTerm:[^\\/]+}[/] | findCelestial
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findCelestial(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['celestial'])['celestial']);
    }

    /**
     * @route GET | /item/{searchTerm:[^\\/]+}[/] | findItem
     * @param string $searchTerm
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function findItem(string $searchTerm)
    {
        return $this->json($this->search->search($searchTerm, ['item'])['item']);
    }
}
