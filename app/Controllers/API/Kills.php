<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class Kills extends ControllerInterface
{
    /** @var \Eon\Models\Participants */
    protected $participants;
    public $validArguments = ['killTime' => 'datetime', 'solarSystemID' => 'int', 'regionID' => 'int',
        'shipValue' => 'float', 'fittingValue' => 'float', 'totalValue' => 'float', 'isNPC' => 'bool',
        'isSolo' => 'bool', 'victim.shipTypeID' => 'int', 'victim.characterID' => 'int',
        'victim.corporationID' => 'int', 'victim.allianceID' => 'int', 'victim.factionID' => 'int',
        'attackers.shipTypeID' => 'int', 'attackers.weaponTypeID' => 'int', 'attackers.characterID' => 'int',
        'attackers.corporationID' => 'int', 'attackers.allianceID' => 'int', 'attackers.factionID' => 'int',
        'attackers.finalBlow' => 'int', 'items.typeID' => 'int', 'items.groupID' => 'int', 'items.categoryID' => 'int',
        'page' => 'int', 'limit' => 'int', 'order' => 'string', ];

    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->participants = $this->container->get('model/participants');
    }

    protected function verifyParams(): array
    {
        $validArguments = $this->validArguments;
        $extraParameters = $this->arguments->get('extraParams');
        $arguments = explode('/', rtrim($extraParameters, '/'));

        $count = 0;
        $tempArray = [];
        $returnArray = [];

        if (count($arguments) >= 2) {
            foreach ($arguments as $param) {
                if (empty($param)) {
                    continue;
                }

                if ($count % 2 == false) {
                    $tempArray[$param] = $arguments[$count + 1];
                }

                $count++;
            }

            foreach ($tempArray as $key => $value) {
                foreach ($validArguments as $arg => $type) {
                    if ($key == $arg) {
                        switch ($type) {
                            case 'int':
                                $returnArray[$key] = (int) $value;
                                break;

                            case 'string':
                                $returnArray[$key] = (string) $value;
                                break;

                            case 'float':
                                $returnArray[$key] = (float) $value;
                                break;

                            case 'bool':
                                $returnArray[$key] = (bool) $value;
                                break;
                            case 'datetime':
                                if (is_numeric($value)) {
                                    $returnArray[$key] = (int) $value * 1000;
                                } else {
                                    $returnArray[$key] = (int) strtotime($value * 1000);
                                }

                                break;
                        }
                    }
                }
            }
        }

        // Do validation (This is about as ugly as it gets ...
        $returnArray['page'] = $returnArray['page'] ?? 1;
        $returnArray['limit'] = $returnArray['limit'] ?? 100;
        $returnArray['offset'] = $returnArray['offset'] ?? 0;
        $returnArray['order'] = $returnArray['order'] ?? 'DESC';

        if ($returnArray['page'] > 1) {
            $returnArray['offset'] = $returnArray['limit'] * $returnArray['page'];
        }

        if ($returnArray['limit'] > 100) {
            $returnArray['limit'] = 100;
        }

        if ($returnArray['limit'] < 1) {
            $returnArray['limit'] = 1;
        }

        $validOrder = ['ASC', 'DESC'];
        if (!in_array($returnArray['order'], $validOrder, false)) {
            $returnArray['order'] = 'DESC';
        }

        return $returnArray;
    }

    /**
     * @route GET | /solarSystem/{solarSystemID:[0-9]+}[/{extraParams:.*}] | getSolarSystem
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getSolarSystem(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getBySolarSystemID($this->arguments->get('solarSystemID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /region/{regionID:[0-9]+}[/{extraParams:.*}] | getRegion
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getRegion(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByRegionID($this->arguments->get('regionID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /character/{characterID:[0-9]+}[/{extraParams:.*}] | getCharacter
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCharacter(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerCharacterID($this->arguments->get('characterID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /corporation/{corporationID:[0-9]+}[/{extraParams:.*}] | getCorporation
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getCorporation(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerCorporationID($this->arguments->get('corporationID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /alliance/{allianceID:[0-9]+}[/{extraParams:.*}] | getAlliance
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAlliance(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerAllianceID($this->arguments->get('allianceID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /faction/{factionID:[0-9]+}[/{extraParams:.*}] | getFaction
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFaction(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerFactionID($this->arguments->get('factionID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /shipType/{shipTypeID:[0-9]+}[/{extraParams:.*}] | getShipType
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getShipType(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerShipTypeID($this->arguments->get('shipTypeID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /weaponType/{weaponTypeID:[0-9]+}[/{extraParams:.*}] | getWeaponType
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getWeaponType(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getByAttackerWeaponTypeID($this->arguments->get('weaponTypeID'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /afterDate/{afterDate:[^\\/]+}[/{extraParams:.*}] | getAfterDate
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAfterDate(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getAllKillsAfterDate($this->arguments->get('afterDate'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /beforeDate/{beforeDate:[^\\/]+}[/{extraParams:.*}] | getBeforeDate
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBeforeDate(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getAllKillsBeforeDate($this->arguments->get('beforeDate'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }

    /**
     * @route GET | /betweenDates/{afterDate:[^\\/]+}/{beforeDate:[^\\/]+}[/{extraParams:.*}] | getBetweenDates
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getBetweenDates(): ResponseInterface
    {
        $params = $this->verifyParams();
        return $this->json($this->participants->getAllKillsBetweenDates($this->arguments->get('afterDate'), $this->arguments->get('beforeDate'), $params, $params['limit'], 360, $params['order'], $params['offset']));
    }
}
