<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Item extends ControllerInterface
{
    /** @var \Eon\Models\TypeIDs */
    protected $typeid;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->typeid = $this->container->get('model/typeids');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->typeid->collection->count();
        return $this->json(['typeidCount' => $count], 360);
    }

    /**
     * @route GET | /information/{typeID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->typeid->find(['typeID' => $this->arguments->get('typeID')], ['projection' => ['_id' => 0]]);
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['typeid']);
    }

    /**
     * @route GET | /top/characters/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topCharacters
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCharacters()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topCharacters($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }

    /**
     * @route GET | /top/corporations/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topCorporations
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCorporations()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topCorporations($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }

    /**
     * @route GET | /top/alliances/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topAlliances
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topAlliances()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topAlliances($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }

    /**
     * @route GET | /top/ships/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topShips
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topShips()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topShips($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }

    /**
     * @route GET | /top/systems/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topSystems
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topSystems()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topSystems($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }

    /**
     * @route GET | /top/regions/{attackerType:[a-zA-Z0-9]+}/{typeID:[0-9]+}[/{limit:[0-9]+}] | topRegions
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topRegions()
    {
        $attackerType = $this->arguments->get('attackerType');
        if($attackerType !== null)
            $data = $this->top->topRegions($attackerType, $this->arguments->get('typeID'), $this->arguments->get('limit', 10));
        return $this->json($data ?? [], 360);
    }
}
