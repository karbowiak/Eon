<?php

namespace Eon\Controllers\API;

use Eon\Bootstrap;
use Eon\Interfaces\ControllerInterface;

class Character extends ControllerInterface
{
    /** @var \Eon\Models\Characters */
    protected $character;
    /** @var \Eon\Models\Search */
    protected $search;
    /** @var \Eon\Models\TopList */
    protected $top;
    public function __construct(Bootstrap $bootstrap)
    {
        parent::__construct($bootstrap);
        $this->character = $this->container->get('model/characters');
        $this->search = $this->container->get('model/search');
        $this->top = $this->container->get('model/toplist');
    }

    /**
     * @route GET | /count | count
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function count()
    {
        $count = $this->character->count();
        return $this->json(['characterCount' => $count], 360);
    }

    /**
     * @route GET | /information/{characterID:[0-9]+} | information
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function information()
    {
        $info = $this->character->getInformation($this->arguments->get('characterID'));
        return $this->json($info->toArray(), 360);
    }

    /**
     * @route GET | /find/{searchTerm:[^\\/]+} | find
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function find()
    {
        return $this->json($this->search->search($this->arguments->get('searchTerm'), 50)['character'], 360);
    }

    /**
     * @route GET | /top/alliances/{characterID:[0-9]+}[/{limit:[0-9]+}] | topAlliances
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topAlliances()
    {
        $data = $this->top->topAlliances('characterID', $this->arguments->get('characterID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/corporations/{characterID:[0-9]+}[/{limit:[0-9]+}] | topCorporations
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topCorporations()
    {
        $data = $this->top->topCorporations('characterID', $this->arguments->get('characterID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/ships/{characterID:[0-9]+}[/{limit:[0-9]+}] | topShips
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topShips()
    {
        $data = $this->top->topShips('characterID', $this->arguments->get('characterID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/systems/{characterID:[0-9]+}[/{limit:[0-9]+}] | topSystems
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topSystems()
    {
        $data = $this->top->topSystems('characterID', $this->arguments->get('characterID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }

    /**
     * @route GET | /top/regions/{characterID:[0-9]+}[/{limit:[0-9]+}] | topRegions
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function topRegions()
    {
        $data = $this->top->topRegions('characterID', $this->arguments->get('characterID'), $this->arguments->get('limit', 10));
        return $this->json($data, 360);
    }
}
