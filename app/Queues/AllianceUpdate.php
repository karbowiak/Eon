<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;

class AllianceUpdate extends QueueInterface
{
    public $queueName = 'allianceUpdate';
    public function handle(): void
    {
        /** @var \Eon\Models\Alliances $alliances */
        $alliances = $this->getContainer()->get('model/alliances');
        /** @var \Eon\Helper\ESIHelper $esi */
        $esi = $this->getContainer()->get('esi');

        $allianceID = $this->data['allianceID'];

        // Check if the alliance already exists and when it was updated last
        try {
            /** @var \MongoDB\BSON\UTCDateTime $lastUpdated */
            $lastUpdated = $alliances->findOne(['allianceID' => $allianceID], ['projection' => ['lastUpdated' => 1]])->get('lastUpdated');
            if ($lastUpdated !== null) {
                $currentTime = time();
                // If the currentTime minus the time when it was last updated, hasn't been at least an hour, skip it
                if ($currentTime - $lastUpdated->toDateTime()->getTimestamp() < 86400) {
                    return;
                }
            }
        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        if ($allianceID > 0) {
            // Save the alliance information
            $this->log('Updating alliance: ' . $allianceID);
            $data = $alliances->esi($allianceID, 1);
            $alliances->setData($data->toArray());
            $alliances->save();
            $corporations = $esi->handle('get', '/alliances/' . $allianceID . '/corporations');
            foreach ($corporations->getArrayCopy() as $corporation) {
                $this->getContainer()->queue->enqueue('corporationUpdate', ['corporationID' => $corporation]);
            }
        }
    }
}
