<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;

class CharacterUpdate extends QueueInterface
{
    public $queueName = 'characterUpdate';
    public function handle(): void
    {
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->getContainer()->get('model/characters');
        $characterID = $this->data['characterID'];

        // Check if the character already exists and when it was updated last
        try {
            /** @var \MongoDB\BSON\UTCDateTime $lastUpdated */
            $lastUpdated = $characters->findOne(['characterID' => $characterID], ['projection' => ['lastUpdated' => 1]])->get('lastUpdated');
            if ($lastUpdated !== null) {
                $currentTime = time();
                // If the currentTime minus the time when it was last updated, hasn't been at least an hour, skip it
                if ($currentTime - $lastUpdated->toDateTime()->getTimestamp() < 172800) {
                    return;
                }
            }
        } catch (\Exception $e) {
            if ($characterID > 0) {
                $characters->update(['characterID' => $characterID], ['dontFetch' => true]);
            }
            dump($e->getMessage());
        }

        if ($characterID > 0) {
            // Save the character information
            $data = $characters->esi($characterID, 1);
            if ($data !== null) {
                $this->log('Updating character: ' . $characterID . ' / ' . $data->get('characterName'));
                $characters->setData($data->toArray());
                $characters->save();
                // Queue updates
                $this->getContainer()->queue->enqueue('allianceUpdate', ['allianceID' => $data->get('allianceID')]);
                $this->getContainer()->queue->enqueue(
                    'corporationUpdate',
                    ['corporationID' => $data->get('corporationID')]
                );
            }
        }
    }
}
