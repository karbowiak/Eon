<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;

class PopulateWarKillmails extends QueueInterface
{
    public $queueName = 'populateWar';
    public function handle(): void
    {
        $warID = $this->data['warID'];
        /** @var \Eon\Helper\ESIHelper $esi */
        $esi = $this->getContainer()->get('esi');
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->getContainer()->get('queue');
        $killmails = $esi->handle('get', '/wars/' . $warID . '/killmails/');

        foreach ($killmails as $killmail) {
            $queue->enqueue('parseKillmail', ['id' => $killmail->killmail_id, 'hash' => $killmail->killmail_hash]);
        }
    }
}
