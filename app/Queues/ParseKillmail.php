<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;
use Exception;

class ParseKillmail extends QueueInterface
{
    public $queueName = 'parseKillmail';
    public function handle(): void
    {
        $id = $this->data['id'];
        $hash = $this->data['hash'];
        $warID = $this->data['warID'] ?? 0;
        $container = $this->getContainer();

        /** @var \Eon\Models\ESIKillmails $esiKillmailData */
        $esiKillmailData = $container->get('model/esikillmails');
        $esiData = $esiKillmailData->getEsiData($id);

        try {
            /** @var \Eon\Models\Killmails $killModel */
            $killModel = $container->get('model/killmails');
            if ($killModel->exists($id)) {
                $this->log("Killmail {$id} already exists, continuing");
                $esiKillmailData->update(['killID' => $id], ['parsed' => true]);
                return;
            }

            /** @var \Eon\Helpers\KillmailHelper $parser */
            $parser = $container->get('killmail');
            $killmail = $parser->parse($id, $hash, $warID, $esiData);

            // Websocket push
            /** @var \Eon\Helpers\Queue $queue */
            $queue = $container->get('queue');
            $queue->enqueue('pushKillmailWebsocket', ['killmail' => $killmail], true);

            $killModel->setData($killmail);
            $killModel->save();
            $esiKillmailData->update(['killID' => $id], ['parsed' => true]);
            $this->log("Saved killmail with id: {$id}");
        } catch (Exception $e) {
            $this->log("Error occurred: {$e->getMessage()}");
            $this->log($e->getTraceAsString());
        }
    }
}
