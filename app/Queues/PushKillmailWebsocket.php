<?php

namespace Eon\Queues;

use Bloatless\WebSocket\Client;
use Eon\Interfaces\QueueInterface;
use Exception;

class PushKillmailWebsocket extends QueueInterface
{
    public $queueName = 'pushKillmailWebsocket';
    public function handle(): void
    {
        $killmail = $this->data['killmail'];

        // Websocket push
        try {
            $client = new \WebSocket\Client('wss://evedata.xyz/kills');
            $client->send(json_encode(
                ['type' => 'broadcast', 'endpoint' => '/kills', 'data' => $killmail],
                JSON_THROW_ON_ERROR,
                512
            ));
            $client->close();
        } catch (Exception $e) {
            $this->log("Error occurred: {$e->getMessage()}");
            $this->log($e->getTraceAsString());
        }
    }
}
