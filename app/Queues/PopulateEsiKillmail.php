<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;

class PopulateEsiKillmail extends QueueInterface
{
    public $queueName = 'populateEsiKillmail';
    public function handle(): void
    {
        $killID = $this->data['killID'];
        $hash = $this->data['hash'];

        /** @var \Eon\Helpers\ESIHelper $esi */
        $esi = $this->getContainer()->get('esi');
        /** @var \Eon\Helpers\Queue $queue */
        $killmailData = json_decode(json_encode($esi->getKillmailData($killID, $hash)), true);

        /** @var \Eon\Models\Killmails $killmails */
        $killmails = $this->container->get('model/killmails');
        /** @var \Eon\Models\ESIKillmails $esiKillmails */
        $esiKillmails = $this->container->get('model/esikillmails');

	$esiExists = $esiKillmails->exists($killID);
        if(!$esiExists) {
            $killExists = $killmails->exists($killID);

            $esiKillmails->setData([
                'killID' => $killID,
                'hash' => $hash,
                'esidata' => $killmailData,
                'parsed' => $killExists,
            ]);
            $esiKillmails->save();
        }
    }
}
