<?php

namespace Eon\Queues;

use Eon\Interfaces\QueueInterface;

class CorporationUpdate extends QueueInterface
{
    public $queueName = 'corporationUpdate';
    public function handle(): void
    {
        /** @var \Eon\Models\Corporations $corporations */
        $corporations = $this->getContainer()->get('model/corporations');
        $corporationID = $this->data['corporationID'];

        // Check if the corporation already exists and when it was updated last
        try {
            /** @var \MongoDB\BSON\UTCDateTime $lastUpdated */
            $lastUpdated = $corporations->findOne(['corporationID' => $corporationID], ['projection' => ['lastUpdated' => 1]])->get('lastUpdated');
            if ($lastUpdated !== null) {
                $currentTime = time();
                // If the currentTime minus the time when it was last updated, hasn't been at least an hour, skip it
                if ($currentTime - $lastUpdated->toDateTime()->getTimestamp() < 345600) {
                    return;
                }
            }
        } catch (\Exception $e) {
            if($corporationID > 0) {
                $corporations->update(['characterID' => $corporationID], ['dontFetch' => true]);
            }
            dump($e->getMessage());
        }

        if ($corporationID > 0) {
            // Save the corporation information
            $this->log('Updating corporation: ' . $corporationID);
            $data = $corporations->esi($corporationID, 1);
            $corporations->setData($data->toArray());
            $corporations->save();

            // If there is an alliance ID we queue it for update
            $this->getContainer()->queue->enqueue('allianceUpdate', ['allianceID' => $data->get('allianceID')]);
        }
    }
}
