<?php

namespace Eon\Cron;

use Eon\Interfaces\QueueInterface;

class UpdatePrices extends QueueInterface
{
    public $queueName = 'updatePrices';
    public $cronTime = '15 12 */1 * *';

    public function handle(): void
    {
        /** @var \Eon\Models\TypeIDs $typeModel */
        $typeModel = $this->getContainer()->get('model/typeids');
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->getContainer()->get('queue');

        /** @var \Tightenco\Collect\Support\Collection $types */
        $types = $typeModel->find(['published' => true]);
        foreach($types as $type) {
            if($type->typeID === 0) {
                continue;
            }

            $queue->enqueue('updatePrice', ['typeID' => $type->typeID], true);
        }
    }
}
