<?php

namespace Eon\Cron;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\QueueInterface;

class UpdateTop10 extends QueueInterface
{
    public $queueName = 'updateTop10';
    public $cronTime = '0 */1 * * *';

    public function handle(): void
    {
        $this->log('Updating top 10');
        $this->updateTop10Characters();
        $this->updateTop10Corporation();
        $this->updateTop10Alliances();
        $this->updateTop10SolarSystems();
        $this->updateTop10Regions();
    }

    private function updateTop10Characters(): void
    {
        $redis = $this->getContainer()->get('redis');
        /** @var \Eon\Models\Killmails $collection */
        $collection = $this->getContainer()->get('model/killmails');
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->getContainer()->get('model/characters');
        $md5 = md5('top10CharactersStatsAPI');
        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.characterID' => ['$ne' => 0]]];

        $data = $collection->aggregate([
            $match,
            ['$unwind' => '$attackers'],
            ['$group' => ['_id' => '$attackers.characterID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'characterID' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => 10],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 1800000, 'hint' => 'attackers.characterID_-1_killTime_-1'] // 600s / 10m
        )->toArray();

        foreach ($data as $key => $character) {
            $count = $data[$key]->count;
            $data[$key] = $characters->findOne(['characterID' => $character->characterID]);
            $data[$key]['count'] = $count;
        }

        $redis->set($md5, $data, 86400);
    }

    private function updateTop10Corporation(): void
    {
        $redis = $this->getContainer()->get('redis');
        /** @var \Eon\Models\Killmails $collection */
        $collection = $this->getContainer()->get('model/killmails');
        /** @var \Eon\Models\Corporations $corporations */
        $corporations = $this->getContainer()->get('model/corporations');
        $md5 = md5('top10CorporationsStatsAPI');
        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.corporationID' => ['$ne' => 0]]];

        $data = $collection->aggregate([
            $match,
            ['$unwind' => '$attackers'],
            ['$group' => ['_id' => '$attackers.corporationID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'corporationID' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => 10],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 600000] // 600s / 10m
        )->toArray();

        foreach ($data as $key => $corporation) {
            $count = $data[$key]->count;
            $data[$key] = $corporations->findOne(['corporationID' => $corporation->corporationID]);
            $data[$key]['count'] = $count;
        }

        $redis->set($md5, $data, 86400);
    }

    private function updateTop10Alliances(): void
    {
        $redis = $this->getContainer()->get('redis');
        /** @var \Eon\Models\Killmails $collection */
        $collection = $this->getContainer()->get('model/killmails');
        /** @var \Eon\Models\Alliances $alliances */
        $alliances = $this->getContainer()->get('model/alliances');
        $md5 = md5('top10AlliancesStatsAPI');
        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'attackers.allianceID' => ['$ne' => 0]]];

        $data = $collection->aggregate([
            $match,
            ['$unwind' => '$attackers'],
            ['$group' => ['_id' => '$attackers.allianceID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'allianceID' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => 10],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 600000] // 600s / 10m
        )->toArray();

        foreach ($data as $key => $alliance) {
            $count = $data[$key]->count;
            $data[$key] = $alliances->findOne(['allianceID' => $alliance->allianceID], ['projection' => ['_id' => 0, 'corporations' => 0, 'description' => 0]]);
            $data[$key]['count'] = $count;
        }

        $redis->set($md5, $data, 86400);
    }

    private function updateTop10SolarSystems(): void
    {
        $redis = $this->getContainer()->get('redis');
        /** @var \Eon\Models\Killmails $collection */
        $collection = $this->getContainer()->get('model/killmails');
        /** @var \Eon\Models\Characters $solarSystems */
        $solarSystems = $this->getContainer()->get('model/solarsystems');
        $md5 = md5('top10SolarSystemsStatsAPI');
        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'solarSystemID' => ['$ne' => 0]]];

        $data = $collection->aggregate([
            $match,
            ['$group' => ['_id' => '$solarSystemID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'solarSystemID' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => 10],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 600000] // 600s / 10m
        )->toArray();

        foreach ($data as $key => $solarSystem) {
            $data[$key]->solarSystemName = $solarSystems->findOne(['solarSystemID' => $solarSystem->solarSystemID])->get('solarSystemName');
        }

        $redis->set($md5, $data, 86400);
    }

    private function updateTop10Regions(): void
    {
        $redis = $this->getContainer()->get('redis');
        /** @var \Eon\Models\Killmails $collection */
        $collection = $this->getContainer()->get('model/killmails');
        /** @var \Eon\Models\Characters $regions */
        $regions = $this->getContainer()->get('model/regions');
        $md5 = md5('top10RegionsStatsAPI');
        $match = ['$match' => ['killTime' => ['$gte' => $this->makeTimeFromDateTime(date('Y-m-d H:i:s', strtotime('-7 days')))], 'regionID' => ['$ne' => 0]]];

        $data = $collection->aggregate(
            [
            $match,
            ['$group' => ['_id' => '$regionID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'regionID' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => 10],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 600000] // 600s / 10m // 600s / 10m
        )->toArray();

        foreach ($data as $key => $region) {
            $data[$key]->regionName = $regions->findOne(['regionID' => $region->regionID])->get('regionName');
        }

        $redis->set($md5, $data, 86400);
    }

    /**
     * @param $dateTime
     * @return UTCDatetime
     */
    private function makeTimeFromDateTime($dateTime): UTCDatetime
    {
        $unixTime = strtotime($dateTime);
        $milliseconds = $unixTime * 1000;

        return new UTCDatetime($milliseconds);
    }

    /**
     * @param $unixTime
     * @return UTCDatetime
     */
    private function makeTimeFromUnixTime($unixTime): UTCDatetime
    {
        $milliseconds = $unixTime * 1000;
        return new UTCDatetime($milliseconds);
    }
}
