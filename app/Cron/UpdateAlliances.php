<?php

namespace Eon\Cron;

use Eon\Interfaces\QueueInterface;

class UpdateAlliances extends QueueInterface
{
    public $queueName = 'allianceUpdate';
    public $cronTime = '0 0 * * *';

    public function handle(): void
    {
        $esi = $this->getContainer()->get('esi');
        $alliances = $esi->handle('get', '/alliances');

        $this->log('Processing ' . count($alliances) . ' alliances..');
        foreach ($alliances as $alliance) {
            $this->getContainer()->get('queue')->enqueue('allianceUpdate', ['allianceID' => $alliance]);
        }
    }
}
