<?php

namespace Eon\Cron;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\QueueInterface;

class UpdateCharacters extends QueueInterface
{
    public $queueName = 'characterUpdate';
    public $cronTime = '*/5 * * * *';

    public function handle(): void
    {
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->getContainer()->get('model/characters');
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->getContainer()->get('queue');

        $date = new UTCDateTime(strtotime('-9 days') * 1000);
        $characters->find(['lastUpdated' => ['$lt' => $date], 'dontFetch' => false], ['limit' => 1000])->each(function ($character) use ($queue) {
            $this->log("Updating {$character->characterName}");
            $queue->enqueue('characterUpdate', ['characterID' => $character->characterID]);
        });
    }
}
