<?php

namespace Eon\Cron;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\QueueInterface;

class UpdateCorporations extends QueueInterface
{
    public $queueName = 'corporationUpdate';
    public $cronTime = '*/5 * * * *';

    public function handle(): void
    {
        /** @var \Eon\Models\Characters $corporations */
        $corporations = $this->getContainer()->get('model/corporations');
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->getContainer()->get('queue');

        $date = new UTCDateTime(strtotime('-5 days') * 1000);
        $corporations->find(
            ['lastUpdated' => ['$lt' => $date], 'corporationID' => ['$gt' => 1999999], 'dontFetch' => false],
            ['limit' => 1000]
        )->each(function ($corporation) use ($queue) {
            $this->log("Updating {$corporation->corporationName}");
            $queue->enqueue('corporationUpdate', ['corporationID' => $corporation->corporationID]);
        });
    }
}
