<?php

namespace Eon\Cron;

use Eon\Interfaces\QueueInterface;

class ParseKillmails extends QueueInterface
{
    public $queueName = 'parseKillmails';
    public $cronTime = '* * * * *';
    public function handle(): void
    {
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->getContainer()->get('queue');
        /** @var \Eon\Models\ESIKillmails $esiKillmails */
        $esiKillmails = $this->getContainer()->get('model/esikillmails');
        $unparsedMails = $esiKillmails->getUnparsed();

        foreach ($unparsedMails as $mail) {
            $killID = $mail->killID;
            $hash = $mail->hash;
            $this->log("Sending {$killID} to parser", true);
            $data = ['id' => $killID, 'hash' => $hash];
            $queue->enqueue('parseKillmail', $data, true);
        }
    }
}
