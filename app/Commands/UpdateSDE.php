<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;

class UpdateSDE extends CommandsInterface
{
    protected string $signature = 'update:sde';

    protected string $description = 'Updates the CCP SDE';

    protected string $sdeUrl = 'https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/sde.zip';

    protected string $sdeSql = 'https://www.fuzzwork.co.uk/dump/sqlite-latest.sqlite.bz2';

    public function handle(): void
    {
        $cachePath = __DIR__ . '/../../resources/cache';

        // Create indexes, and thus - the collections
        $this->out('Creating indexes');
        $this->container->ensureIndexes();

        // Download and unzip SDE
        $this->out('<info>Downloading SDE</info>');
        exec("curl --progress-bar -o {$cachePath}/sde.zip {$this->sdeUrl}");
        exec("unzip -oq {$cachePath}/sde.zip -d {$cachePath}");

        // Download and unpack SQL
        $this->out('<info>Downloading SQL</info>');
        exec("curl --progress-bar -o {$cachePath}/sqlite-latest.sqlite.bz2 {$this->sdeSql}");
        exec("rm {$cachePath}/sqlite-latest.sqlite");
        exec("bzip2 -d {$cachePath}/sqlite-latest.sqlite.bz2");

        $seeds = glob(__DIR__ . '/../../resources/database/seeds/*.php');
        $seedCount = count($seeds);

        $this->out("Processing {$seedCount} seeds");
        foreach ($seeds as $seedFile) {
            require_once($seedFile);
            $name = str_replace('.php', '', basename($seedFile));
            $class = "\\Eon\\Resources\\Database\\Seeds\\{$name}";

            $this->out("Running {$name}");

            /** @var \Eon\Interfaces\SeedInterface $seed */
            $seed = new $class($this->container);
            $seed->execute();
        }
    }
}
