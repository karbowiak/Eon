<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;

class FindCharacters extends CommandsInterface
{
    protected string $signature = 'find:characters';

    protected string $description = 'Finds characters based on holes in the data';

    public function handle(): void
    {
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');
        $queue = $this->container->queue;
        $all = $characters->find([], ['$sort' => ['characterID' => -1]]);

        /** @var \Tightenco\Collect\Support\Collection $character */
        foreach ($all as $key => $character) {
            $nextCharacter = $all[$key + 1];

            $currentCharacterID = $character->characterID;
            $nextCharacterID = $nextCharacter->characterID;

            if ($nextCharacterID - $currentCharacterID >= 2) {
                $this->out('Next character ID is further than 2 away');
                $current = $currentCharacterID + 1;
                do {
                    $this->out('Enqueueing characterID: ' . $current);
                    $queue->enqueue('characterUpdate', ['characterID' => $current]);
                    $current++;
                } while ($current <= $nextCharacterID);
            }
        }
    }
}
