<?php

namespace Eon\Commands;

use Cron\CronExpression;
use Eon\Interfaces\CommandsInterface;
use Tightenco\Collect\Support\Collection;
use Symfony\Component\Console\Helper\Table;

class Cron extends CommandsInterface
{
    protected string $signature = 'cron
                                {--debug : Enable debug mode}
                                {exec? : Run a cronjob right now}';

    protected string $description = 'Run once a minute with crontab';

    protected array $crons = [];

    private function loadCrons(): array
    {
        $files = glob(__DIR__ . '/../Cron/*.php');
        $crons = [];
        foreach($files as $cron) {
            require_once($cron);
            $className = '\\Eon\\Cron\\' . str_replace('.php', '', basename($cron));
            $loaded = new $className([]);
            $cronHandle = $loaded->queueName;
            $cronTime = $loaded->cronTime;
            $crons[$cronHandle]['class'] = $className;
            $crons[$cronHandle]['runtime'] = $cronTime;
        }

        return $crons;
    }

    public function handle(): void
    {
        $queue = $this->container->queue;
        $crons = new Collection($this->loadCrons());

        if ($this->exec !== null) {
            $cron = $crons->get($this->exec);
            if ($cron === null) {
                $this->out('Error, ' . $this->exec . ' is not a valid cron. Valid crons: <info>' . implode(', ', array_keys($crons->toArray())) . '</info>');
                return;
            }

            /** @var \Eon\Interfaces\QueueInterface $class */
            $class = new $cron['class']([]);
            $this->out('Now running <info>' . $this->exec);
            if ($this->debug === true) {
                $class->output(true);
            }
            $class->handle();
            return;
        }

        $run = true;
        $show = true;

        do {
            try {
                $closest = 0;
                $crons = $crons->transform(static function ($value, $key) use (&$closest) {
                    $cron = CronExpression::factory($value['runtime']);
                    $closest = $closest === 0 ? $cron->getNextRunDate()->getTimestamp() : min($closest, $cron->getNextRunDate()->getTimestamp());

                    return [
                        'queueName' => $key,
                        'class' => $value['class'],
                        'runtime' => $value['runtime'],
                        'nextRun' => $cron->getNextRunDate(),
                        'nextRunUnix' => $cron->getNextRunDate()->getTimestamp(),
                        'isDue' => $cron->isDue(),
                    ];
                });

                // Gotta wrap the old collection in a new one, to separate them - otherwise calling transform on it again, would just make it transform the original
                $cronInfo = (new Collection($crons))->transform(static function ($cron) {
                    return $cron['nextRun']->format('Y-m-d H:i:s');
                })->toArray();

                $crons->each(function ($cron) use ($queue, $show) {
                    if ($cron['isDue'] === true) {
                        $this->out('Scheduling <info>' . $cron['queueName'] . '</info>');
                        $show = true;
                        $queue->cron($cron['queueName'], true);
                    }
                });

                // Render an options table
                if ($show) {
                    $this->out('Current server time: <info>' . date('Y-m-d H:i:s') . '</info>');
                    $table = new Table($this->output);
                    $rows = array_map(static function ($a, $b) {
                        return ["<info>{$a}</info>", $b];
                    }, array_keys($cronInfo), $cronInfo);
                    $table->addRows($rows);
                    $table->render();
                    $show = false;
                }

                $secondsTillNext = $closest - time();
                $this->out('<info>' . date('Y-m-d H:i:s') . '</info> | Sleeping for ' . $secondsTillNext . ' seconds, till next is scheduled.');
                sleep($secondsTillNext);
            } catch (\Exception $e) {
                $run = false;
            }
        } while ($run);
    }
}
