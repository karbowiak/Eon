<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;
use Tightenco\Collect\Support\Collection;

class ListenRedisQ extends CommandsInterface
{
    protected string $signature = 'listen:redisq';

    protected string $description = 'Listens to RedisQ and sends killmails to the fetcher as they come in from RedisQ';

    public function handle(): void
    {
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->container->get('queue');
        /** @var \Eon\Helpers\ESIHelper $esi */
        $esi = $this->container->get('esi');
        $redisq = 'https://redisq.zkillboard.com/listen.php';

        $run = true;
        do {
            try {
                $data = file_get_contents($redisq);
                if ($this->isJson($data)) {
                    $json = json_decode($data, false, 512, JSON_THROW_ON_ERROR);
                    $killmail = new Collection($json->package);
                    if ($killmail->has('zkb')) {
                        $killID = $killmail->get('killID');
                        $hash = $killmail->get('zkb')->hash;
                        /** @var \Eon\Models\Killmails $killmails */
                        $killmails = $this->container->get('model/killmails');
                        $killExists = $killmails->exists($killID);

			if (!$killExists) {
                            $this->out("Adding {$killID} to queue..");
                            $data = ['id' => $killID, 'hash' => $hash];
                            $killmailData = json_decode(json_encode($esi->getKillmailData($killID, $hash)), true);

                            /** @var \Eon\Models\ESIKillmails $esiKillmails */
                            $esiKillmails = $this->container->get('model/esikillmails');
                            $esiKillmails->setData([
                                'killID' => $killID,
                                'hash' => $hash,
                                'esidata' => $killmailData,
                                'parsed' => $killExists,
                            ]);
                            $esiKillmails->save();
                            $queue->enqueue('parseKillmail', $data, true);
                        }
                    }
                }
            } catch (\Exception $e) {
                dump($e);
                $run = false;
            }

            usleep(500000);
        } while ($run === true);
    }

    /**
     * @param string $data
     *
     * @return bool
     */
    protected function isJson(string $data): bool
    {
        json_decode($data);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
