<?php

namespace Eon\Commands;

use League\Csv\Reader;
use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\CommandsInterface;

class BackfillPrices extends CommandsInterface
{
    protected string $signature = 'backfill:prices';

    protected string $description = 'Backfills prices from 2016-08-01 and forward';

    public function handle(): void
    {
        /** @var \Eon\Models\Prices $priceModel */
        $priceModel = $this->container->get('model/prices');

        /** @var \Eon\Models\TypeIDs $typeModel */
        $typeModel = $this->container->get('model/typeids');

        $this->out('Truncating price table..');
        $priceModel->truncate();
        $this->container->ensureIndexes();

        $this->out('<info>Starting backfill of prices</info>');
        $startDate = date_create('2016-08-01');
        $nowDate = date_create(date('Y-m-d'));
        $diff = date_diff($startDate, $nowDate);
        $progressBar = $this->progressBar($diff->days);
        $startDate = strtotime('2016-08-01');
        $currentTime = $startDate;
        $typeData = [];
        do {
            $year = date('Y', $currentTime);
            $fileName = 'market-history-' . date('Y-m-d', $currentTime) . '.csv.gz';
            try {
                try {
                    $file = __DIR__ . "/../../resources/marketdata/{$fileName}";
                    if (!file_exists($file)) {
                        $ctx = stream_context_create(['http' => ['timeout' => 5]]);
                        $data = file_get_contents('https://data.everef.net/market-history/' . $year . '/' . $fileName, false, $ctx);
                        if (!empty($kills)) {
                            file_put_contents($file, $data);
                        } else {
                            throw new \Exception('Market endpoint sends bad data :( ' . $file);
                        }
                    } else {
                        $data = file_get_contents($file);
                    }
                } catch (\Exception $e) {
                    $data = file_get_contents('https://data.everef.net/market-history/' . $year . '/' . $fileName);
                }
                $csvData = gzdecode($data);
                $csvElem = Reader::createFromString($csvData);
                $csvElem->setHeaderOffset(0);
    
                $insertData = [];
                foreach ($csvElem as $elem) {
                    if ((int) $elem['region_id'] !== 10000002) {
                        continue;
                    }
    
                    $typeId = (int) $elem['type_id'];
                    $extra = !empty($typeData[$typeId]) ? $typeData[$typeId] : $typeModel->findOne(['typeID' => (int) $elem['type_id']]);
                    $typeData[$typeId] = $extra;
                    $price = $this->getPrice($elem['type_id'], 0) === 0 ? (float) $elem['average'] : $this->getPrice($elem['type_id'], 0);
                    $insertData[] = [
                        'typeID' => (int) $elem['type_id'],
                        'typeNames' => $extra->get('name'),
                        'marketGroupID' => (int) $extra->get('marketGroupID'),
                        'groupID' => (int) $extra->get('groupID'),
                        'price' => $price,
                        'fromDate' => new UTCDateTime($currentTime * 1000),
                        'fromDate_str' => (string) date('Y-m-d', $currentTime),
                        'lastUpdated' => new UTCDateTime(time() * 1000),
                    ];
                }
    
                $priceModel->setData($insertData, true);
                $priceModel->saveMany();
            } catch (\Exception $e) {
                dump($e->getMessage());
            }

            $progressBar->advance();
            $currentTime = strtotime('+1 days', $currentTime);
        } while (time() >= $currentTime);
        $progressBar->finish();
    }

    private function getPrice(int $typeID, int $groupID)
    {
        switch ($typeID) {
            case 12478: // Khumaak
            case 34559: // Conflux Element
                return 0.01; // Items that get market manipulated and abused will go here
            case 44265: // Victory Firework
                return 0.01; // Items that drop from sites will go here

            // Items that have been determined to be obnoxiously market
            // manipulated will go here
            case 34558:
            case 34556:
            case 34560:
            case 36902:
            case 34559:
            case 34557:
                return 0.01;
            case 2834: // Utu
            case 3516: // Malice
            case 11375: // Freki
                return 80000000000; // 80b
            case 3518: // Vangel
            case 3514: // Revenant
            case 32788: // Cambion
            case 32790: // Etana
            case 32209: // Mimir
            case 11942: // Silver Magnate
            case 33673: // Whiptail
                return 100000000000; // 100b
            case 35779: // Imp
            case 42125: // Vendetta
            case 42246: // Caedes
                return 120000000000; // 120b
            case 48636: // Hydra
                return 140000000000; // 150b
            case 2836: // Adrestia
            case 33675: // Chameleon
            case 35781: // Fiend
            case 45530: // Virtuoso
                return 150000000000; // 150b
            case 33397: // Chremoas
            case 42245: // Rabisu
            case 45649: // Komodo
                return 200000000000; // 200b
            case 45531: // Victor
                return 230000000000;
            case 48635: // Tiamat
                return 230000000000;
            case 9860: // Polaris
            case 11019: // Cockroach
                return 1000000000000; // 1 trillion, rare dev ships
            case 42241: // Molok
                return 650000000000;

            // Rare cruisers
            case 11940: // Gold Magnate
            case 635: // Opux Luxury Yacht
            case 11011: // Guardian-Vexor
            case 25560: // Opux Dragoon Yacht
            case 33395: // Moracha
                return 500000000000; // 500b

            // Rare battleships
            case 13202: // Megathron Federate Issue
            case 26840: // Raven State Issue
            case 11936: // Apocalypse Imperial Issue
            case 11938: // Armageddon Imperial Issue
            case 26842: // Tempest Tribal Issue
                return 750000000000; // 750b
        }

        return 0;
    }
}
