<?php

namespace Eon\Commands;

use Exception;
use Eon\Bootstrap;
use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\CommandsInterface;

class UpdateBattles extends CommandsInterface
{
    protected string $signature = 'update:battles';

    protected string $description = 'Populates the battles data';
    /** @var \Eon\Models\Killmails */
    protected $killmails;
    /** @var \Eon\Models\Battles */
    protected $battles;
    /** @var \Eon\Models\Storage */
    protected $storage;
    /** @var \Eon\Models\SolarSystems */
    protected $solarSystem;

    public function __construct(
        Bootstrap $container,
        string $name = null
    ) {
        ini_set('memory_limit', -1);
        parent::__construct($container, $name);
        $this->killmails = $container->get('model/killmails');
        $this->battles = $container->get('model/battles');
        $this->storage = $container->get('model/storage');
        $this->solarSystem = $container->get('model/solarsystems');
    }

    public function handle(): void
    {
        /*
         * look over the killmail table in 2 hour chunks, each time advancing one hour, till there is a hit.
         * Then go over a 4 hour chunk of time, in 10 minute intervals to check for hits of 3 kills or more.
         * example: startTime = 2009-01-01 22:00:00, endTime = 2009-01-01 00:00:00
         * A hit has been made in that two hour chunk, of 500 people.
         * Expand it by a 30 minutes in each direction (3 hours total) so start is 21:30 and end is 00:30 the next day.
         * Then look for killmails in a 10 minute interval
         * First hit of 3 kills or more, is registered as the start.
         * Once 6 chunks have registered false (meaning less than 3 kills), call the battle finished, and move forward an hour
         * It can go beyond 00:30 if it keeps getting hits of 3 kills or more.
         */

        $startTime = time() - 43200;
        $endTime = time();

        do {
            $timeAfter = new UTCDateTime(($startTime - 3600) * 1000);
            $timeBefore = new UTCDateTime(($startTime + 3600) * 1000);

            $pipeline = [
                ['$match' => ['killTime' => ['$gte' => $timeAfter, '$lte' => $timeBefore]]],
                ['$group' => ['_id' => '$solarSystemID', 'count' => ['$sum' => 1]]],
                ['$project' => ['_id' => 0, 'count' => '$count', 'solarSystemID' => '$_id']],
                ['$match' => ['count' => ['$gte' => 50]]],
                ['$sort' => ['count' => -1]],
            ];

            $results = $this->killmails->aggregate($pipeline)->toArray();
            if (count($results) >= 1) {
                foreach ($results as $result) {
                    $run = true;
                    $fails = 0;
                    $minTime = $startTime - 3600;
                    $solarSystemID = $result->solarSystemID;
                    $battleStartTime = 0;

                    $battleCount = $this->battles->find(['solarSystemInfo.solarSystemID' => $solarSystemID, 'startTime' => ['$gte' => new UTCDateTime(($minTime - 14400) * 1000)]]);
                    if ($battleCount->isNotEmpty()) {
                        continue;
                    }

                    do {
                        $timeAfter = new UTCDateTime($minTime * 1000);
                        $timeBefore = new UTCDateTime(($minTime + 600) * 1000);
                        $pipeline = [
                            ['$match' => ['solarSystemID' => $solarSystemID, 'killTime' => ['$gte' => $timeAfter, '$lte' => $timeBefore]]],
                            ['$group' => ['_id' => '$solarSystemID', 'count' => ['$sum' => 1]]],
                            ['$project' => ['_id' => 0, 'count' => '$count', 'solarSystemID' => '$_id']],
                            ['$match' => ['count' => ['$gte' => 3]]],
                            ['$sort' => ['count' => -1]],
                        ];
                        $result = $this->killmails->aggregate($pipeline)->toArray();

                        if ($battleStartTime === 0 && count($result) >= 1) {
                            $fails = 0;
                            $battleStartTime = $minTime;
                        }

                        if ($fails < 20 && $battleStartTime !== 0) {
                            $solarSystemData = $this->solarSystem->getAllBySolarSystemID($solarSystemID);
                            $solarSystemName = $solarSystemData->get('solarSystemName');
                            $regionName = $solarSystemData->get('regionName');
                            $this->out('A battle happened in ' . $solarSystemName . ' / ' . $regionName . ' and started at ' .
                                date('Y-m-d H:i:s', $minTime - 12000) . ' and ended at ' .
                                date('Y-m-d H:i:s', $battleStartTime));

                            $this->processBattle($minTime - 12000, $battleStartTime, $solarSystemID);
                            $run = false;
                        } elseif ($fails >= 20) {
                            $run = false;
                        }

                        $minTime += 600;
                        if (count($result) === 0) {
                            $fails++;
                            continue;
                        }
                    } while ($run === true);
                }
            }

            $startTime += 3600;
            $this->storage->set('battleImporterStartTime', $startTime);
        } while ($startTime < $endTime);
    }

    protected function processBattle($startTime, $endTime, $solarSystemID): void
    {
        $killData = $this->killmails->aggregate([
            [
                '$match' => ['solarSystemID' => $solarSystemID, 'killTime' => [
                    '$gte' => new UTCDateTime($startTime * 1000),
                    '$lte' => new UTCDateTime($endTime * 1000),
                ]],
            ],
            ['$unwind' => '$attackers'],
        ])->toArray();

        $redTeam = [];
        $redTeamCharacters = [];
        $redTeamShips = [];
        $redTeamCorporations = [];
        $redTeamKills = [];
        $blueTeam = [];
        $blueTeamCharacters = [];
        $blueTeamShips = [];
        $blueTeamCorporations = [];
        $blueTeamKills = [];
        $success = $this->findTeams($redTeam, $blueTeam, $killData);
        if ($success === false) {
            return;
        }

        foreach ($redTeam as $member) {
            foreach ($killData as $mail) {
                if ($mail->attackers->allianceName === $member) {
                    if ($mail->attackers->corporationName !== '' && !in_array($mail->attackers->corporationName, $redTeamCorporations, false)) {
                        $redTeamCorporations[] = $mail->attackers->corporationName;
                    }

                    if (!in_array($mail->attackers->characterName, $redTeamCharacters, false)) {
                        if (!isset($redTeamShips[$mail->attackers->shipTypeName])) {
                            $redTeamShips[$mail->attackers->shipTypeName] = [
                                'shipTypeName' => $mail->attackers->shipTypeName,
                                'count' => 1,
                            ];
                        } else {
                            $redTeamShips[$mail->attackers->shipTypeName]['count']++;
                        }
                    }

                    if (!in_array($mail->attackers->characterName, $redTeamCharacters, false)) {
                        $redTeamCharacters[] = $mail->attackers->characterName;
                    }

                    if (!in_array($mail->killID, $redTeamKills, false)) {
                        $redTeamKills[] = $mail->killID;
                    }
                }
            }
        }
        foreach ($blueTeam as $member) {
            foreach ($killData as $mail) {
                if ($mail->attackers->allianceName === $member) {
                    if ($mail->attackers->corporationName !== '' && !in_array($mail->attackers->corporationName, $blueTeamCorporations, false)) {
                        $blueTeamCorporations[] = $mail->attackers->corporationName;
                    }

                    if (!in_array($mail->attackers->characterName, $blueTeamCharacters, false)) {
                        if (!isset($blueTeamShips[$mail->attackers->shipTypeName])) {
                            $blueTeamShips[$mail->attackers->shipTypeName] = [
                                'shipTypeName' => $mail->attackers->shipTypeName,
                                'count' => 1,
                            ];
                        } else {
                            $blueTeamShips[$mail->attackers->shipTypeName]['count']++;
                        }
                    }

                    if (!in_array($mail->attackers->characterName, $blueTeamCharacters, false)) {
                        $blueTeamCharacters[] = $mail->attackers->characterName;
                    }

                    if (!in_array($mail->killID, $blueTeamKills, false)) {
                        $blueTeamKills[] = $mail->killID;
                    }
                }
            }
        }

        // Remove the overlap
        if (count($blueTeamKills) > count($redTeamKills)) {
            foreach ($blueTeamKills as $key => $id) {
                if (in_array($id, $redTeamKills, false)) {
                    unset($blueTeamKills[$key]);
                }
            }
        } else {
            foreach ($redTeamKills as $key => $id) {
                if (in_array($id, $blueTeamKills, false)) {
                    unset($redTeamKills[$key]);
                }
            }
        }

        if (!empty($redTeam) && !empty($blueTeam) && !empty($redTeamCorporations) && !empty($blueTeamCorporations)) {
            // Quite possibly hilariously incorrect...
            $dataArray = [
                'startTime' => new UTCDateTime($startTime * 1000),
                'endTime' => new UTCDateTime($endTime * 1000),
                'solarSystemInfo' => $this->solarSystem->findOne(['solarSystemID' => $solarSystemID]),
                'teamRed' => [
                    'characters' => array_values($redTeamCharacters),
                    'corporations' => array_values($redTeamCorporations),
                    'alliances' => array_values($redTeam),
                    'ships' => array_values($redTeamShips),
                    'kills' => array_values($redTeamKills),
                ],
                'teamBlue' => [
                    'characters' => array_values($blueTeamCharacters),
                    'corporations' => array_values($blueTeamCorporations),
                    'alliances' => array_values($blueTeam),
                    'ships' => array_values($blueTeamShips),
                    'kills' => array_values($blueTeamKills),
                ],
            ];
            $battleID = md5(json_encode($dataArray, JSON_THROW_ON_ERROR, 512));
            $dataArray['battleID'] = $battleID;

            // Insert the data to the battles table
            try {
                $this->battles->setData($dataArray);
                $this->battles->save();
            } catch (Exception $e) {
                $this->out('An error happened.. ' . $e->getMessage());
            }
        }
    }

    protected function findTeams(&$redTeam, &$blueTeam, $killData): bool
    {
        $allianceTemp = [];
        $corporationTemp = [];
        $this->allianceSides($allianceTemp, $blueTeam, $redTeam, $killData);

        if (empty($blueTeam) || empty($redTeam)) {
            $this->corporationSides($corporationTemp, $blueTeam, $redTeam, $killData);
        }
        if (empty($blueTeam) || empty($redTeam)) {
            return false;
        }

        $redTeamSize = count($redTeam);
        $blueTeamSize = count($blueTeam);

        if ($redTeamSize > $blueTeamSize) {
            foreach ($redTeam as $key => $player) {
                if (in_array($player, $blueTeam, false)) {
                    unset($redTeam[$key]);
                }

                if ($player === '') {
                    unset($redTeam[$key]);
                }
            }
        } else {
            foreach ($blueTeam as $key => $player) {
                if (in_array($player, $redTeam, false)) {
                    unset($blueTeam[$key]);
                }

                if ($player === '') {
                    unset($blueTeam[$key]);
                }
            }
        }

        return true;
    }

    protected function allianceSides(&$allianceTemp, &$blueTeam, &$redTeam, $killData): void
    {
        foreach ($killData as $data) {
            $attacker = $data->attackers;
            $victim = $data->victim;

            if (isset($attacker->allianceName, $victim->allianceName) && $victim->allianceName !== '') {
                if (@!in_array($attacker->allianceName, $allianceTemp[$victim->allianceName], true)) {
                    $allianceTemp[$victim->allianceName][] = $attacker->allianceName;
                } else {
                    continue;
                }
            }
        }

        // Clean up any strings that are empty
        foreach ($allianceTemp as $key => $innerArray) {
            foreach ($innerArray as $ikey => $welp) {
                if ($welp === '') {
                    unset($allianceTemp[$key][$ikey]);
                }
            }
        }

        // Find the red team
        $size = 0;
        $redTeamVictim = '';
        foreach ($allianceTemp as $victim => $attacker) {
            $attackers = count($attacker);
            if ($attackers > $size) {
                $size = $attackers;
                $redTeam = $attacker;
                $redTeamVictim = $victim;
            }
        }
        // Unset the redTeam from the tempArray
        unset($allianceTemp[$redTeamVictim]);

        // Find the blue team
        foreach ($allianceTemp as $blue) {
            foreach ($blue as $member) {
                if (!in_array($member, $blueTeam, false)) {
                    $blueTeam[] = $member;
                }
            }
        }
    }

    protected function corporationSides(&$corporationTemp, &$blueTeam, &$redTeam, $killData): void
    {
        foreach ($killData as $data) {
            $attacker = $data->attackers;
            $victim = $data->victim;

            if (isset($attacker->corporationName, $victim->corporationName) && $victim->corporationName !== '') {
                if (@!in_array($attacker->corporationName, $corporationTemp[$victim->corporationName], true)) {
                    $corporationTemp[$victim->corporationName][] = $attacker->corporationName;
                } else {
                    continue;
                }
            }
        }

        // Clean up any strings that are empty
        foreach ($corporationTemp as $key => $innerArray) {
            foreach ($innerArray as $ikey => $welp) {
                if ($welp === '') {
                    unset($corporationTemp[$key][$ikey]);
                }
            }
        }

        // Find the red team
        $size = 0;
        $redTeamVictim = '';
        foreach ($corporationTemp as $victim => $attacker) {
            $attackers = count($attacker);
            if ($attackers > $size) {
                $size = $attackers;
                $redTeam = $attacker;
                $redTeamVictim = $victim;
            }
        }
        // Unset the redTeam from the tempArray
        unset($corporationTemp[$redTeamVictim]);

        // Find the blue team
        foreach ($corporationTemp as $blue) {
            foreach ($blue as $member) {
                if (!in_array($member, $blueTeam, false)) {
                    $blueTeam[] = $member;
                }
            }
        }
    }
}
