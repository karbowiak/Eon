<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;
use Tightenco\Collect\Support\Collection;

class BackfillKillmails extends CommandsInterface
{
    protected string $signature = 'backfill:killmails';

    protected string $description = 'Backfill killmails from zKillboard';

    public function handle(): void
    {
        $totals = array_reverse(
            @json_decode(
                file_get_contents('https://zkillboard.com/api/history/totals.json'),
                true,
                512,
                JSON_THROW_ON_ERROR
            ) ?? [],
            true
        );

        $processed = 1;
        $killmails = 0;
        (new Collection($totals))->transform(static function ($row) use (&$killmails) {
            $killmails += $row;
        });

        $this->out('Iterating over ' . count($totals) . ' individual days');
        foreach ($totals as $date => $total) {
            $this->out("Day: {$date} | {$total} kills available");
            try {
                $file = __DIR__ . "/../../resources/cache/{$date}.json";
                if (!file_exists($file)) {
                    $kills = file_get_contents("https://zkillboard.com/api/history/{$date}.json");
                    if (!empty($kills)) {
                        file_put_contents($file, $kills);
                        $kills = json_decode($kills, true, 512, JSON_THROW_ON_ERROR);
                    } else {
                        throw new \RuntimeException('Kills was empty');
                    }
                } else {
                    $kills = json_decode(
                        file_get_contents($file),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    );
                }
            } catch (\Exception $e) {
                sleep(10);
                $kills = json_decode(
                    file_get_contents("https://zkillboard.com/api/history/{$date}.json"),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            }
            foreach ($kills as $id => $hash) {
                if ($id === 'day') {
                    continue;
                }

                /** @var \Eon\Helpers\Queue $queue */
                $queue = $this->container->get('queue');
                $queue->enqueue('populateEsiKillmail', ['killID' => $id, 'hash' => $hash]);

                $this->out("{$date} / {$processed} / {$killmails} | Queued killID: {$id} ({$hash}) for ESI fetching..");
                $processed++;
            }
        }
    }
}
