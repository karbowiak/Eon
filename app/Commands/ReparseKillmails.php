<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;
use Tightenco\Collect\Support\Collection;

class ReparseKillmails extends CommandsInterface
{
    protected string $signature = 'reparse:killmails';

    protected string $description = 'Reparse all killmails';

    public function handle(): void
    {
        /** @var \Eon\Models\Killmails $killmails */
        $killmails = $this->container->get('model/killmails');

        /** @var \Eon\Helper\KillmailHelper $killmailParser */
        $killmailParser = $this->container->get('killmail');

        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->container->get('queue');

        $this->out('Wiping character / corporation / alliance kills/losses/points');
        $this->out('!!WARNING!! this could take a while..');
        // Wipe character, corporation and alliance point values
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');
        /** @var \Eon\Models\Corporations $corporations */
        $corporations = $this->container->get('model/corporations');
        /** @var \Eon\Models\Alliances $alliances */
        $alliances = $this->container->get('model/alliances');

	try {
	        $characters->collection->updateMany([], ['$set' => ['kills' => 0, 'losses' => 0, 'points' => 0]]);
        	$corporations->collection->updateMany([], ['$set' => ['kills' => 0, 'losses' => 0, 'points' => 0]]);
	        $alliances->collection->updateMany([], ['$set' => ['kills' => 0, 'losses' => 0, 'points' => 0]]);
	} catch (\Exception $e) {
		dd($e->getMessage());
	}

        /** @var Collection $killmail */
        $skip = 0;
        $killmailCount = $killmails->count();
        $progressBar = $this->progressBar($killmailCount);
        $this->out('Processing ' . $killmails->count() . ' killmails');

        while ($skip < $killmailCount) {
            $mails = $killmails->find(
                [],
                [
                    'projection' => ['killID' => -1, 'hash' => -1],
                    'sort' => ['killTime' => -1],
                    'skip' => $skip,
                    'limit' => 1000,
                ]
            );
            foreach ($mails as $killmail) {
                $queue->enqueue(
                    'parseKillmail',
                    ['id' => $killmail->killID, 'hash' => $killmail->hash, 'reparse' => true]
                );
                $progressBar->advance();
                $skip++;
            }
        }
        $progressBar->finish();
    }
}
