<?php

namespace Eon\Commands;

use Exception;
use Eon\Bootstrap;
use Swoole\WebSocket\Frame;
use Eon\Interfaces\CommandsInterface;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Tightenco\Collect\Support\Collection;
use Swoole\Websocket\Server as SwooleServer;
use Symfony\Component\Console\Helper\Table;
use Ilex\SwoolePsr7\SwooleResponseConverter;
use Ilex\SwoolePsr7\SwooleServerRequestConverter;

/**
 * Class Server
 * @package Eon\Commands
 * @property string host
 * @property int port
 * @property int production
 * @property int workers
 * @property int max_requests
 * @property int dispatch_mode
 * @property int tcp_nodelay
 * @property int reuse_port
 * @property int log
 */
class Server extends CommandsInterface
{
    /** @var string */
    protected string $signature = 'server
                            {--host=0.0.0.0 : Host for the server to listen on}
                            {--port=8080 : Port for the server to listen on}
                            {--workers=5 : Number of worker threads (Should match CPU Threads)}
                            {--max_requests=5000 : Number of requests for a worker to process, before it\'s reaped}
                            {--dispatch_mode=2 : Incoming request distribution policy (1: Round Robin, 2: Fixed Mode (Every request is served by the same worker), 3: Least Busy (Requests are assigned to the least busy worker)}
                            {--reuse_port=1 : Reuse a port for data, instead of closing it and opening a new one}
                            {--tcp_nodelay=1 : Speeds up TCP connections by not waiting for a response from the client}
                            {--production : Runs the server in production mode, which disables reloading on file changes}
                            {--log : Runs the server in quiet mode, meaning no request logging}';
    /** @var string */
    protected string $description = 'Swoole based web server';

    /**
     * @var array|\Swoole\Table
     */
    protected $wsClients = [];

    /**
     * @var array
     */
    protected $wsEndpoints = [];

    /**
     * @var SwooleServerRequestConverter
     */
    protected $serverRequestFactory;

    public function __construct(
        Bootstrap $container,
        string $name = null
    ) {
        parent::__construct($container, $name);
        $this->wsClients = new \Swoole\Table(1024);
        $this->wsClients->column('fd', \Swoole\Table::TYPE_INT);
        $this->wsClients->column('data', \Swoole\Table::TYPE_STRING, 512);
        $this->wsClients->column('endpoint', \Swoole\Table::TYPE_STRING, 2048);
        $this->wsClients->create();
    }

    /**
     *
     */
    public function handle(): void
    {
        $this->out('<info>Starting Swoole web server</info>');

        $options = [
            'worker_num' => $this->workers,
            'daemonize' => '0',
            'max_request' => $this->max_requests,
            'dispatch_mode' => $this->dispatch_mode,
            'task_ipc_mode' => '1',
            'reactor_num' => round($this->workers) / 2,
            'open_tcp_nodelay' => $this->tcp_nodelay === '1' ? '1' : '0',

            // Coroutines should be enabled
            'enable_coroutine' => true,

            // Directory where uploaded data is saved to
            'upload_tmp_dir' => dirname(__DIR__, 2) . '/resources/cache/uploads',

            // Set the output buffer size
            'buffer_output_size' => 4 * 1024 * 1024, // 4MB

            // Handle static files internally
            'enable_static_handler' => true,
            'document_root' => dirname(__DIR__, 2) . '/public',
        ];

        // Render an options table
        $table = new Table($this->output);
        $rows = array_map(static function ($a, $b) {
            return ["<info>{$a}</info>", $b];
        }, array_keys($options), $options);
        $table->addRows($rows);
        $table->render();

        // Start Swoole
        $server = new SwooleServer($this->host, $this->port, SWOOLE_PROCESS, SWOOLE_SOCK_TCP);

        // Set Swoole options
        $server->set($options);

        // On Request
        $server->on('workerStart', function () {
            $this->container->initContainer();
            $this->container->initSlim($this->production);

            /* @var \Ilex\SwoolePsr7\SwooleServerRequestConverter $serverRequestFactory */
            $this->serverRequestFactory = new SwooleServerRequestConverter(
                $this->container->psr17Factory,
                $this->container->psr17Factory,
                $this->container->psr17Factory,
                $this->container->psr17Factory
            );
        });

        $server->on('start', function ($server) {
            $this->out('Server is now up at: http://' . $server->host . ':' . $server->port);
            $this->out('Websocket Server is now up at: ws://' . $server->host . ':' . $server->port);
        });

        $server->on('request', function (Request $request, Response $response) {
            if ($this->log) {
                $time = date('Y-m-d H:i:s');
                $postParams = $this->logPostParams($request->post);
                $this->out("<info>{$time}</info> :: Request from {$request->server['remote_addr']} | {$request->server['request_method']} | {$request->server['request_uri']} {$postParams}");
            }

            $psr7Request = $this->serverRequestFactory->createFromSwoole($request);
            $psr7Response = $this->container->app->handle($psr7Request);
            $converter = new SwooleResponseConverter($response);
            $converter->send($psr7Response);
        });

        // Load the websockets
        $sockets = glob(__DIR__ . '/../../app/Websockets/*.php');
        foreach ($sockets as $socket) {
            $class = '\\Eon\\Websockets\\' . str_replace('.php', '', basename($socket));
            $class = new $class($this->container, $server);
            $this->wsEndpoints[$class->endpoint] = $class;
        }

        $server->on('open', function (\Swoole\Server $server, Request $request) {
            $this->wsClients[$request->fd] = [
                'id' => $request->fd,
                'data' => json_encode($server->getClientInfo($request->fd), JSON_THROW_ON_ERROR, 512),
                'endpoint' => $request->server['request_uri'],
            ];
        });

        $server->on('message', function (\Swoole\Server $server, Frame $frame) {
            $frameData = $this->isJson($frame->data) ? new Collection(json_decode($frame->data, false)) : new Collection([]);
            $type = $frameData->get('type');
            $data = $frameData->get('data') ?? [];
            /** @var \Swoole\Table\Row $fdData */
            $fdData = $this->wsClients[$frame->fd]->value ?? [];

            switch ($type) {
                case 'subscribe':
                    if (!empty($this->wsEndpoints[$fdData['endpoint']])) {
                        $this->wsEndpoints[$fdData['endpoint']]->subscribe($frame->fd, $data);
                    }
                    break;

                case 'unsubscribe':
                    if (!empty($this->wsEndpoints[$fdData['endpoint']])) {
                        $this->wsEndpoints[$fdData['endpoint']]->unsubscribe($frame->fd);
                    }
                    break;

                case 'broadcast':
                    $endpoint = $frameData->get('endpoint');
                    try {
                        if (empty($endpoint)) {
                            $server->push(
                                $frame->fd,
                                json_encode(
                                    ['error' => 'You did not specify an endpoint for posting data'],
                                    JSON_THROW_ON_ERROR,
                                    512
                                )
                            );
                        }

                        // Look into adding redis queue support, so stuff that's broadcast, is also stored in redis, and sent out upon reconnect
                        $this->wsEndpoints[$endpoint]->handle($data);
                    } catch (Exception $e) {
                        $server->push(
                            $frame->fd,
                            json_encode(['error' => 'Endpoint doesn\'t exist'], JSON_THROW_ON_ERROR, 512)
                        );
                    }
                    break;
            }
        });

        $server->on('close', function (\Swoole\Server $server, $fd) {
            foreach ($this->wsEndpoints as $endpoint) {
                $endpoint->unsubscribe($fd);
            }
            $this->wsClients->del($fd);
        });

        // Start swoole
        $server->start();
    }

    /**
     * @param array|null $postParams
     *
     * @return string
     */
    private function logPostParams(?array $postParams = []): string
    {
        $params = '';
        if (!empty($params)) {
            $params = '| ';
            foreach ($postParams as $key => $value) {
                $params .= "{$key}:{$value} ";
            }
        }

        return $params;
    }

    /**
     * @param string $data
     *
     * @return bool
     */
    protected function isJson(string $data): bool
    {
        json_decode($data, false);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
