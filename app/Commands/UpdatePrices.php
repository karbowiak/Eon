<?php

namespace Eon\Commands;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\CommandsInterface;
use Tightenco\Collect\Support\Collection;

class UpdatePrices extends CommandsInterface
{
    protected string $signature = 'update:prices';

    protected string $description = 'Updates all the individual item values';

    public function handle(): void
    {
        /** @var \Eon\Models\Prices $priceModel */
        $priceModel = $this->container->get('model/prices');
        /** @var \Eon\Models\TypeIDs $typeModel */
        $typeModel = $this->container->get('model/typeids');
        $types = $typeModel->find(['published' => true]);
        $typeCount = $types->count();
        $progressBar = $this->progressBar($typeCount);
        $insertData = new Collection([]);
        foreach ($types as $type) {
            try {
                $price = $this->getPrice($type->typeID, $type->groupID);

                $insertData[] = [
                    'typeID' => (int) $type->typeID,
                    'typeNames' => $type->name,
                    'marketGroupID' => (int) $type->marketGroupID ?? 0,
                    'groupID' => (int) $type->groupID,
                    'price' => (float) $price,
                    'fromDate' => new UTCDateTime(time() * 1000),
                    'fromDate_str' => (string) date('Y-m-d'),
                    'lastUpdated' => new UTCDateTime(time() * 1000),
                ];
                $progressBar->advance();
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }

        $priceModel->setData($insertData, true);
        $priceModel->saveMany();
        $progressBar->finish();
    }

    private function getPrice(int $typeID, int $groupID)
    {
        switch ($typeID) {
            case 12478: // Khumaak
            case 34559: // Conflux Element
                return 0.01; // Items that get market manipulated and abused will go here
            case 44265: // Victory Firework
                return 0.01; // Items that drop from sites will go here

            // Items that have been determined to be obnoxiously market
            // manipulated will go here
            case 34558:
            case 34556:
            case 34560:
            case 36902:
            case 34559:
            case 34557:
                return 0.01;
            case 2834: // Utu
            case 3516: // Malice
            case 11375: // Freki
                return 80000000000; // 80b
            case 3518: // Vangel
            case 3514: // Revenant
            case 32788: // Cambion
            case 32790: // Etana
            case 32209: // Mimir
            case 11942: // Silver Magnate
            case 33673: // Whiptail
                return 100000000000; // 100b
            case 35779: // Imp
            case 42125: // Vendetta
            case 42246: // Caedes
                return 120000000000; // 120b
            case 48636: // Hydra
                return 140000000000; // 150b
            case 2836: // Adrestia
            case 33675: // Chameleon
            case 35781: // Fiend
            case 45530: // Virtuoso
                return 150000000000; // 150b
            case 33397: // Chremoas
            case 42245: // Rabisu
            case 45649: // Komodo
                return 200000000000; // 200b
            case 45531: // Victor
                return 230000000000;
            case 48635: // Tiamat
                return 230000000000;
            case 9860: // Polaris
            case 11019: // Cockroach
                return 1000000000000; // 1 trillion, rare dev ships
            case 42241: // Molok
                return 650000000000;

            // Rare cruisers
            case 11940: // Gold Magnate
            case 635: // Opux Luxury Yacht
            case 11011: // Guardian-Vexor
            case 25560: // Opux Dragoon Yacht
            case 33395: // Moracha
                return 500000000000; // 500b

            // Rare battleships
            case 13202: // Megathron Federate Issue
            case 26840: // Raven State Issue
            case 11936: // Apocalypse Imperial Issue
            case 11938: // Armageddon Imperial Issue
            case 26842: // Tempest Tribal Issue
                return 750000000000; // 750b
        }

        /** @var \Eon\Helpers\ESIHelper $esi */
        $esi = $this->container->get('esi');
        $data = $esi->handle('get', '/markets/10000002/history/', ['type_id' => $typeID]);
        return end($data)->lowest ?? 0;
    }
}
