<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;

/**
 * Class Generator
 * @package Eon\Commands
 * @property string type
 * @property string name
 */
class Generator extends CommandsInterface
{
    protected string $signature = 'generator
        {type : Valid generator types: Command, Controller, Middleware, Model, Websockets, Queue}
        {name : Name of the file and class}';

    protected string $description = 'Generates files for building an application.';

    public function handle(): void
    {
        $validTypes = ['command', 'cron', 'controller', 'middleware', 'model', 'websocket', 'queue'];
        $validTypesString = implode(' / ', $validTypes);
        $type = strtolower($this->type);
        $name = $this->name;

        if (in_array($type, $validTypes, true)) {
            $templateData = $this->getTemplate($type);
            $className = ucfirst($name);
            switch ($type) {
                case 'command':
                    $signature = $name;
                    $templateData = str_replace(['<<CLASSNAME>>', '<<SIGNATURE>>'], [$className, $signature], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Commands/' . $className . '.php', $templateData);

                    $this->out('Remember to add command to list of commands available in settings/commands.yml');
                    break;
                case 'controller':
                    $className .= 'Controller';
                    $templateData = str_replace(['<<CLASSNAME>>'], [$className], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Controllers/' . $className . '.php', $templateData);

                    $this->out('Remember to add route to your controller in settings/routes.yml');
                    break;
                case 'middleware':
                    $className .= 'Handler';
                    $templateData = str_replace(['<<CLASSNAME>>'], [$className], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Middleware/' . $className . '.php', $templateData);

                    $this->out('Remember to add middleware to your middleware settings in settings/middleware.yml');
                    break;
                case 'model':
                    $collectionName = strtolower($name);
                    $templateData = str_replace(['<<CLASSNAME>>', '<<COLLECTIONNAME>>'], [$className, $collectionName], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Models/' . $className . '.php', $templateData);
                    break;
                case 'websocket':
                    $templateData = str_replace(['<<CLASSNAME>>'], [$className], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/WebSockets/' . $className . '.php', $templateData);

                    // @TODO fix this.. maybe handle it sort of like controller routes, but for websockets ?
                    $this->out('YOU GOTTA DO SOMETHING TO LOAD THE WEBSOCKET, BUT WHAT?');
                    break;
                case 'queue':
                    $className .= 'Queue';
                    $templateData = str_replace(['<<CLASSNAME>>'], [$className], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Queues/' . $className . '.php', $templateData);

                    $this->out('Remember to add queue to your queue configuration in settings/queues.yml');
                    break;
                case 'cron':
                    $className .= 'Cron';
                    $templateData = str_replace(['<<CLASSNAME>>'], [$className], $templateData);
                    $this->writeTemplate(__DIR__ . '/../../app/Cron/' . $className . '.php', $templateData);

                    $this->out('Remember to add your cron to your cron configuration in settings/cron.yml');
                    break;
            }

            $this->out("{$type} has now been generated..");
        } else {
            $this->out("<fg=red>Error,</> <info>{$type}</info> is not one of <fg=red>{$validTypesString}</>");
        }
    }

    private function writeTemplate(string $path, string $templateData): void
    {
        file_put_contents($path, $templateData);
    }

    private function getTemplate(string $type): string
    {
        $type = ucfirst($type);
        return file_get_contents(__DIR__ . "/../generator/{$type}Generator.php");
    }
}
