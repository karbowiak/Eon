<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;

class Queue extends CommandsInterface
{
    protected string $signature = 'queue {--workers=16 : Amount of workers to use}';

    protected string $description = 'Swoole based task queue';

    public function handle(): void
    {
        // Ensure indexes (Could take a moment if they don't exist..)
        $this->out('Ensuring indexes..');
        $this->container->ensureIndexes();

        $queue = $this->container->queue;
        $this->out("Starting queue with {$this->workers} workers");
        $queue->setAmountOfWorkers($this->workers);
        $queue
            ->setRedisConfig(!empty($_ENV['EON_REDIS_DB']) ? $_ENV['EON_REDIS_DB'] : 'redis', '', 'queue', !empty($_ENV['EON_REDIS_PORT']) ? (int) $_ENV['EON_REDIS_PORT'] : 6379)
            ->setPidPath(dirname(__DIR__, 2) . '/resources/cache/queue.pid');
        $queue->run();
    }
}
