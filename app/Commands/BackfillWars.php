<?php

namespace Eon\Commands;

use Eon\Interfaces\CommandsInterface;
use Eon\Helper\ESIHelper;
use Eon\Interfaces\QueueInterface;
use Tightenco\Collect\Support\Collection;

class BackfillWars extends CommandsInterface
{
    protected string $signature = 'backfill:wars {--populateKillmails : Populate killmails}';

    protected string $description = 'Backfills the wars';

    public function handle(): void
    {
        /** @var ESIHelper $esi */
        $esi = $this->container->get('esi');
        /** @var \Eon\Models\Corporations $corporation */
        $corporation = $this->container->get('model/corporations');
        /** @var \Eon\Models\Alliances $alliance */
        $alliance = $this->container->get('model/alliances');
        /** @var \Eon\Models\Wars $warsModel */
        $warsModel = $this->container->get('model/wars');
        /** @var \Eon\Helpers\Queue $queue */
        $queue = $this->container->get('queue');
        $minId = 999999999;
        $maxId = 0;
        $run = true;
        $warCount = 0;
        $processed = 0;
        $page = 1;
        $pages = 999;
        $this->out('Processing Wars');
        do {
            try {
                $wars = $esi->getWars($minId);
                $warCount += count($wars);
                if (count($wars) < 2000) {
                    $run = false;
                }
                foreach ($wars as $warID) {
                    $maxId = max($warID, $maxId);
                    $minId = min($warID, $minId);
                    $pages = (int) round($maxId / 2000);

                    $this->out("{$processed} / {$warCount} | {$warID} | {$page} / {$pages}");
                    if($warsModel->exists($warID) && !$warsModel->needsUpdate($warID)) {
                        if ($this->populateKillmails) {
                            $queue->enqueue('populateWar', ['warID' => $warID]);
                        }

                        $processed++;
                        continue;
                    }

                    try {
                        $warInfo = $esi->handle('get', '/wars/' . $warID);
                    } catch (\Exception $e) {
                        continue;
                    }
                    $aggressorAllianceID = $warInfo->aggressor->alliance_id ?? 0;
                    $aggressorCorporationID = $warInfo->aggressor->corporation_id ?? 0;
                    $defenderAllianceID = $warInfo->defender->alliance_id ?? 0;
                    $defenderCorporationID = $warInfo->defender->corporation_id ?? 0;
                    $allies = $warInfo->allies ?? [];
                    $allyCount = count($allies);
                    $data = [
                        'warID' => $warInfo->id,
                        'timeDeclared' => $warInfo->declared ?? null,
                        'timeStarted' => $warInfo->started ?? null,
                        'timeFinished' => $warInfo->finished ?? null,
                        'openForAllies' => $warInfo->open_for_allies,
                        'allyCount' => $allyCount,
                        'allies' => (new Collection($allies))->transform(static function ($ally) use (
                            $corporation,
                            $alliance
                        ) {
                            $corporationID = $ally->corporation_id ?? 0;
                            $allianceID = $ally->alliance_id ?? 0;

                            return [
                                'corporationID' => $corporationID,
                                'corporationName' => $corporationID > 0 ? $corporation->getById($corporationID)->get('corporationName') : '',
                                'allianceID' => $allianceID,
                                'allianceName' => $allianceID > 0 ? $alliance->getById($allianceID)->get('allianceName') : '',
                            ];
                        })->toArray(),
                        'mutual' => $warInfo->mutual,
                        'aggressor' => [
                            'shipsKilled' => $warInfo->aggressor->ships_killed,
                            'iskKilled' => $warInfo->aggressor->isk_destroyed,
                            'corporationID' => $aggressorCorporationID,
                            'corporationName' => $aggressorCorporationID > 0 ? $corporation->getById($aggressorCorporationID)->get('corporationName') : '',
                            'allianceID' => $aggressorAllianceID,
                            'allianceName' => $aggressorAllianceID > 0 ? $alliance->getById($aggressorAllianceID)->get('allianceName') : '',
                        ],
                        'defender' => [
                            'shipsKilled' => $warInfo->aggressor->ships_killed,
                            'iskKilled' => $warInfo->aggressor->isk_destroyed,
                            'corporationID' => $defenderCorporationID,
                            'corporationName' => $defenderCorporationID > 0 ? $corporation->getById($defenderCorporationID)->get('corporationName') : '',
                            'allianceID' => $defenderAllianceID,
                            'allianceName' => $defenderAllianceID > 0 ? $alliance->getById($defenderAllianceID)->get('allianceName') : '',
                        ],
                    ];

                    $warsModel->setData($data);
                    $warsModel->save();

                    if ($data['aggressor']['shipsKilled'] > 0 || $data['defender']['shipsKilled'] > 0) {
                        $queue->enqueue('populateWar', ['warID' => $data['warID']]);
                    }

                    $processed++;
                }

                if($page === $pages)
                    $run = false;

                $page++;
            } catch (\Exception $e) {
                $this->out($e->getMessage());
            }
        } while ($run);
    }
}
