<?php

namespace Eon\Helpers;

use Exception;
use Know\Swoole\Queue\Job;
use Know\Swoole\Queue\JobInterface;
use Xin\Cli\Color;
use Tightenco\Collect\Support\Collection;

class Queue extends Job
{
    /** @var \Eon\Helpers\Config */
    public $config;
    /** @var bool */
    public $now = false;
    /** @var \Tightenco\Collect\Support\Collection */
    public $queues = [];
    /** @var \Tightenco\Collect\Support\Collection */
    public $crons = [];

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->redisHost = !empty($_ENV['EON_REDIS_DB']) ? $_ENV['EON_REDIS_DB'] : 'redis';
        $this->redisPort = !empty($_ENV['EON_REDIS_PORT']) ? (int) $_ENV['EON_REDIS_PORT'] : 6379;
        $this->queues = new Collection();
        $this->crons = new Collection();
        $this->loadQueues();
        $this->loadCrons();
    }

    public function loadQueues(): void
    {
        $queues = glob(__DIR__ . '/../Queues/*.php');
        foreach($queues as $queue) {
            require_once($queue);
            $className = '\\Eon\\Queues\\' . str_replace('.php', '', basename($queue));
            $loaded = new $className(null);
            $queueHandle = $loaded->queueName;
            $this->queues[$queueHandle] = $className;
        }
    }

    public function loadCrons(): void
    {
        $crons = glob(__DIR__ . '/../Cron/*.php');
        foreach($crons as $cron) {
            require_once($cron);
            $className = '\\Eon\\Cron\\' . str_replace('.php', '', basename($cron));
            $loaded = new $className(null);
            $cronHandle = $loaded->queueName;
            $this->crons[$cronHandle] = $className;
        }
    }

    public function setAmountOfWorkers(int $workers): void
    {
        $this->maxProcesses = $workers;
    }

    public function cron(string $queueName, $data, $now = true): bool
    {
        $queue = $this->crons->get($queueName);
        if ($queue !== null) {
            $job = ['class' => $queue, 'data' => $data, 'log' => 'cron.log'];
            $this->submit($job, $now);

            return true;
        }

        return false;
    }

    public function enqueue(string $queueName, $data, $now = false): bool
    {
        $queue = $this->queues->get($queueName, null);
        if ($queue !== null) {
            $job = ['class' => $queue, 'data' => $data, 'log' => null];
            $this->submit($job, $now);

            return true;
        }

        return false;
    }

    public function submit(array $job, bool $now = false)
    {
        $redis = $this->getRedisChildClient();
        if ($now) {
            return $redis->rPush($this->queueKey, gzcompress(json_encode($job, JSON_THROW_ON_ERROR, 512), 6));
        }
        return $redis->lPush($this->queueKey, gzcompress(json_encode($job, JSON_THROW_ON_ERROR, 512), 6));
    }

    public function handle($recv)
    {
        $data = json_decode(gzuncompress($recv), true, 512, JSON_THROW_ON_ERROR);
        $class = $data['class'];
        $logFile = $data['log'] ?? 'queue.log';
        $data = $data['data'] ?? [];
        $obj = new $class($data);
        if (!empty($logFile)) {
            $obj->logFile = $logFile;
        }
        try {
            if ($obj instanceof JobInterface) {
                $startTime = microtime(true);
                $name = get_class($obj);
                $date = date('Y-m-d H:i:s');
                echo Color::colorize("[{$date}] Processing: {$name}", Color::FG_GREEN) . PHP_EOL;
                $obj->handle();
                $date = date('Y-m-d H:i:s');
                $endTime = microtime(true) - $startTime;
                echo Color::colorize("[{$date}] Processed: {$name} | {$endTime}ms", Color::FG_GREEN) . PHP_EOL;
            }
        } catch (Exception $ex) {
            $date = date('Y-m-d H:i:s');
            $name = get_class($obj);
            echo Color::colorize("[{$date}] Failed: {$name} | {$ex->getMessage()}", Color::FG_RED) . PHP_EOL;
            $this->logError($ex);
            $redis = $this->getRedisChildClient();
            $redis->lPush($this->errorKey, $recv);
        }
    }
}
