<?php

namespace Eon\Helpers;

use Exception;
use League\Container\Container;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Client;
use MongoDB\Collection as MCollection;
use MongoDB\DeleteResult;
use MongoDB\Driver\Cursor;
use MongoDB\UpdateResult;
use RuntimeException;
use Tightenco\Collect\Support\Collection;

class Mongo
{
    /** @var string $collectionName Name of the collection */
    public $collectionName = '';
    /** @var string $databaseName Name of the database */
    public $databaseName = 'app';
    /** @var string Index field */
    protected $indexField = '';
    /** @var MCollection */
    public $collection;
    /** @var \MongoDB\GridFS\Bucket */
    public $bucket;
    /** @var array $hidden Fields to hide from output (ie. Password hash, email etc.) */
    protected $hidden = [];
    /** @var array $required Fields required to insert data to model (ie. email, password hash, etc.) */
    protected $required = [];
    /** @var \Eon\Helpers\Config */
    protected $config;
    /** @var \MongoDB\Client */
    protected $mongodb;
    /** @var \Eon\Helpers\Redis */
    protected $redis;
    /** @var \Tightenco\Collect\Support\Collection */
    protected $data;
    /** @var Container */
    protected $container;

    public function __construct(Config $config, Redis $redis, Client $mongoConnection, Container $container)
    {
        $this->config = $config;
        $this->redis = $redis;
        $this->data = new Collection();
        $this->mongodb = $mongoConnection;
        $this->container = $container;

        $db = !empty($this->databaseName) ? $this->databaseName : $config->get('mongodb/db', 'eon');
        $collectionName = !empty($this->collectionName) ? $this->collectionName : 'defaultCollection';
        $this->collection = $mongoConnection->selectDatabase($db)->selectCollection($collectionName);
        $this->bucket = $mongoConnection->selectDatabase($db)->selectGridFSBucket();
    }

    /**
     * @param $dateTime
     * @return UTCDateTime
     */
    public function makeTimeFromDateTime(string $dateTime): UTCDateTime
    {
        $unixTime = strtotime($dateTime);
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    /**
     * @param $unixTime
     * @return UTCDateTime
     */
    public function makeTimeFromUnixTime(int $unixTime): UTCDateTime
    {
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    /**
     * Make time from whatever
     *
     * @param $time
     * @return UTCDateTime
     */
    public function makeTime($time): UTCDateTime
    {
        if (is_int($time)) {
            return $this->makeTimeFromUnixTime($time);
        }
        return $this->makeTimeFromDateTime($time);
    }

    /**
     * Set a lot of data at once
     *
     * @param array|Collection $data
     * @param bool             $clear
     */
    public function setData($data = [], bool $clear = false): void
    {
        if ($clear === true) {
            $this->data = new Collection();
        }

        foreach ($data as $key => $value) {
            $this->data->put($key, $value);
        }
    }

    /**
     * Get everything in the data array
     *
     * @return Collection
     */
    public function getData(): Collection
    {
        return $this->data;
    }

    /**
     * Magic get method, returns a variable from the data collection, if it exists
     *
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->data->get($key);
    }

    /**
     * Magic set method, sets a variable and key in the data collection
     *
     * @param string $key
     * @param $value
     */
    public function __set(string $key, $value)
    {
        $this->data->put($key, $value);
    }

    /**
     * Magic isset method, checks if a variable is in the data collection
     *
     * @param $key
     * @return bool
     */
    public function __isset($key): bool
    {
        return $this->data->has($key);
    }

    /**
     * Mongo find
     *
     * @param array|Collection $filter
     * @param array|Collection $options
     * @param bool             $showHidden
     * @param int              $cacheTime
     *
     * @return \Tightenco\Collect\Support\Collection
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \JsonException
     */
    public function find(array $filter = [], array $options = [], bool $showHidden = false, int $cacheTime = 0): Collection
    {
        $md5 = md5(json_encode($filter, JSON_THROW_ON_ERROR));
        if ($this->redis->has($md5)) {
            return new Collection(json_decode($this->redis->get($md5), true, 512, JSON_THROW_ON_ERROR));
        }

        $data = $this->collection->find($filter, $options)->toArray();
        $this->redis->set($md5, json_encode($data, JSON_THROW_ON_ERROR), $cacheTime);

        if ($showHidden) {
            return new Collection($data);
        }

        return (new Collection($data))->forget($this->hidden);
    }

    /**
     * Return the first element found
     *
     * @param array|Collection $filter
     * @param array|Collection $options
     * @param bool             $showHidden
     * @param int              $cacheTime
     *
     * @return \Tightenco\Collect\Support\Collection
     * @throws \Psr\SimpleCache\InvalidArgumentException|\JsonException
     */
    public function findOne(array $filter = [], array $options = [], bool $showHidden = false, int $cacheTime = 0): Collection
    {
        $md5 = md5(json_encode($filter, JSON_THROW_ON_ERROR));
        if ($this->redis->has($md5)) {
            return new Collection(json_decode($this->redis->get($md5), true, 512, JSON_THROW_ON_ERROR));
        }

        $data = $this->find($filter, $options, $showHidden)->first();
        $this->redis->set($md5, json_encode($data, JSON_THROW_ON_ERROR), $cacheTime);
        return new Collection($data);
    }

    /**
     * Runs an aggregate pipeline - more information can be found here: https://docs.mongodb.com/manual/core/aggregation-pipeline/
     *
     * @param array|Collection $pipeline
     * @param array|Collection $options
     *
     * @return Cursor|\Traversable
     */
    public function aggregate(array $pipeline = [], array $options = [])
    {
        return $this->collection->aggregate($pipeline, $options);
    }

    /**
     * Mongo delete
     *
     * @param array|Collection $filter
     * @return \MongoDB\DeleteResult
     * @throws \Exception
     */
    public function delete(array $filter = []): DeleteResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when deleting');
        }

        return $this->collection->deleteOne($filter);
    }

    /**
     * Truncate a collection
     *
     * @return void
     */
    public function truncate(): void
    {
        try {
            $this->collection->drop();
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Mongo update
     *
     * @param array|Collection $filter
     * @param array|Collection $update
     * @return \MongoDB\UpdateResult
     */
    public function update(array $filter, array $update): UpdateResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when updating');
        }
        return $this->collection->updateOne($filter, $update);
    }

    /**
     * Create a new data array
     *
     * @param array|\stdClass|null $input
     * @return \Eon\Helpers\Mongo
     */
    public function clear($input = null): Mongo
    {
        $that = $this;
        $that->data = !empty($input) ? new Collection($input) : new Collection();
        return $that;
    }

    /**
     * Save many things from the local data object
     */
    public function saveMany(): void
    {
//        foreach ($this->data as $data) {
//            $this->hasRequired(is_array($data) ? new Collection($data) : $data);
//        }

        $this->collection->insertMany($this->data->all());
    }

    /**
     * Save the local data object
     *
     * @return \MongoDB\InsertOneResult|\MongoDB\UpdateResult
     */
    public function save()
    {
        $this->hasRequired();
        try {
            return $this->collection->insertOne($this->data->toArray());
        } catch (Exception $e) {
            try {
                return $this->collection->replaceOne([$this->indexField => $this->data->get($this->indexField)], $this->data->toArray(), ['upsert' => true]);
            } catch (Exception $e) {
                throw new RuntimeException('Error: ' . $e->getMessage());
            }
        }
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->collection->count();
    }

    /**
     * @param \Tightenco\Collect\Support\Collection|null $data
     */
    protected function hasRequired(Collection $data = null): void
    {
        if (!empty($this->required)) {
            foreach ($this->required as $key) {
                if ($data !== null && !$data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
                if (!$this->data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
            }
        }
    }
}
