<?php

namespace Eon\Helpers;

use Symfony\Component\Yaml\Yaml;
use Tightenco\Collect\Support\Collection;

class Config
{
    /**
     * @var string
     */
    protected $configLocation;
    /**
     * @var \Tightenco\Collect\Support\Collection
     */
    protected $config;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->configLocation = __DIR__ . '/../../resources/settings';
        $this->config = new Collection();
        $this->loadConfig();
    }

    /**
     * @param string|null $path
     * @param string|null $parentName
     */
    private function loadConfig(?string $path = null, ?string $parentName = null): void
    {
        $location = $path ?? $this->configLocation;
        $locations = [$location . '/*.yml', $location . '/*/*.yml', $location . '/*/*/*.yml'];
        foreach ($locations as $location) {
            $files = glob($location);
            foreach ($files as $file) {
                $extra = explode(basename(explode('settings/', $file)[1]), explode('settings/', $file)[1])[0];
                $configName = $extra . basename($file, '.yml');
                if (is_dir($file)) {
                    $this->loadConfig($file, $configName);
                    continue;
                }
                $this->parseConfig($configName, $file, $parentName);
            }
        }

        $this->config;
    }

    /**
     * @param string $configName
     * @param string $filePath
     * @param string|null $parentName
     */
    private function parseConfig(string $configName, string $filePath, string $parentName = null): void
    {
        $keyName = $parentName ?? $configName;
        $data = $parentName ? [$configName => Yaml::parseFile($filePath)] : Yaml::parseFile($filePath);
        if ($this->config->has($keyName)) {
            $items = $this->config->all();
            foreach ($data as $key => $value) {
                if ($parentName && ($parentName !== $keyName)) {
                    $items[$parentName][$keyName][$key] = $value;
                } else {
                    $items[$keyName][$key] = $value;
                }
            }
            $this->config = new Collection($items);
        } else {
            $this->config->put($keyName, $data);
        }
    }

    /**
     * @param string $key
     * @param $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        if (strpos($key, '/') !== false) {
            $cnt = 1;
            $keys = explode('/', $key);
            $keyCount = count($keys) - 1; // We already used the first key, also why $cnt is 1
            $selectedItem = $this->config->get($keys[0]);
            if ($selectedItem === null) {
                return $default;
            }

            do {
                $selectedItem = $selectedItem[$keys[$cnt]];
                $cnt++;
            } while ($cnt <= $keyCount);

            if (@$selectedItem !== null && @$default === null) {
                return $selectedItem;
            }
        }

        if (!$this->config->has($key)) {
            return $default;
        }
        return $this->config->get($key);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->config->all();
    }
}
