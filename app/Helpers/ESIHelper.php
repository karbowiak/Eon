<?php

namespace Eon\Helpers;

use Seat\Eseye\Configuration;
use Seat\Eseye\Log\NullLogger;
use Seat\Eseye\Cache\NullCache;
use Seat\Eseye\Exceptions\RequestFailedException;
use Seat\Eseye\Exceptions\UriDataMissingException;
use bandwidthThrottle\tokenBucket\storage\FileStorage;
use Seat\Eseye\Exceptions\EsiScopeAccessDeniedException;
use bandwidthThrottle\tokenBucket\Rate;
use bandwidthThrottle\tokenBucket\BlockingConsumer;
use bandwidthThrottle\tokenBucket\TokenBucket;
use League\Container\Container;
use Seat\Eseye\Containers\EsiResponse;
use Seat\Eseye\Eseye;
use Seat\Eseye\Exceptions\InvalidContainerDataException;
use Seat\Eseye\Exceptions\InvalidAuthenticationException;

class ESIHelper
{
    /** @var \Seat\Eseye\Eseye */
    protected $esi;
    protected $tokenBucket;
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $configuration = Configuration::getInstance();
        $configuration->http_user_agent = 'Eon @ michael@karbowiak.dk';
        $configuration->logger = NullLogger::class; // No logs
        $configuration->cache = NullCache::class; // No cache

        // Load ESI and set the version to latest
        $this->esi = new Eseye();
        $this->esi->setVersion('latest');

        // Token bucket
        $storage = new FileStorage(__DIR__ . '/../../resources/cache/bucket.token');
        $reqs = 100;
        $rate = new Rate($reqs, Rate::SECOND);
        $bucket = new TokenBucket($reqs, $rate, $storage);
        $bucket->bootstrap($reqs);
        $this->tokenBucket = new BlockingConsumer($bucket);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $data
     *
     * @return \Seat\Eseye\Containers\EsiResponse
     */
    public function handle(string $method, string $uri, array $data = []): ?EsiResponse
    {
        try {
            // Do error and stats tracking here..
            $this->esi->setQueryString($data);
            $this->tokenBucket->consume(1);
            $result = null;
            try {
                $result = $this->esi->invoke($method, $uri, $data);
            } catch (EsiScopeAccessDeniedException $e) {
                $this->tokenBucket->consume(50);
            } catch (InvalidAuthenticationException $e) {
                $this->tokenBucket->consume(50);
            } catch (InvalidContainerDataException $e) {
                $this->tokenBucket->consume(50);
            } catch (RequestFailedException $e) {
                $this->tokenBucket->consume(50);
            } catch (UriDataMissingException $e) {
                $this->tokenBucket->consume(50);
            }

            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getKillmailData(int $id, string $hash): ?EsiResponse
    {
        return $this->handle('get', '/killmails/' . $id . '/' . $hash);
    }

    public function getWars(int $minId): ?EsiResponse
    {
        return $this->handle('get', '/wars/', ['max_war_id' => $minId]);
    }

    public function getWarById(int $warId): ?EsiResponse
    {
        return $this->handle('get', '/wars/' . $warId);
    }

    public function getWarKills(int $warId): ?EsiResponse
    {
        return $this->handle('get', '/wars/' . $warId . '/killmails');
    }

    public function getFactionName(int $factionId): string
    {
        /** @var \Eon\Models\Factions $factions */
        $factions = $this->container->get('model/factions');
        return (string) $factions->findOne(['corporationID' => $factionId])->get('factionName');
    }

    public function getStationName(int $stationId): string
    {
        return $this->handle('get', '/universe/stations/' . $stationId)->name;
    }

    public function getStationInfo(int $stationId): ?EsiResponse
    {
        return $this->handle('get', '/universe/stations/' . $stationId);
    }

    public function getCharacterData(int $characterId): ?EsiResponse
    {
        return $this->handle('get', '/characters/' . $characterId);
    }

    public function getCorporationData(int $corporationID): ?EsiResponse
    {
        return $this->handle('get', '/corporations/' . $corporationID);
    }

    public function getAllianceData(int $allianceID): ?EsiResponse
    {
        return $this->handle('get', '/alliances/' . $allianceID);
    }
}
