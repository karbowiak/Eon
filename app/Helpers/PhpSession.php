<?php

namespace Eon\Helpers;

use SessionHandlerInterface;

class PhpSession implements SessionHandlerInterface
{
    /**
     * @var float|int
     */
    protected $ttl = 60 * 60 * 24 * 30;
    /**
     * @var \Eon\Helpers\Redis
     */
    protected $redis;

    /**
     * PhpSession constructor.
     *
     * @param \Eon\Helpers\Redis $redis
     */
    public function __construct(Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @param string $session_id
     *
     * @return string
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function read($session_id): string
    {
        $data = json_decode($this->redis->get($session_id), false);
        if ($data !== null) {
            return $data;
        }
        return '';
    }

    /**
     * @param string $session_id
     * @param string $session_data
     *
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function write($session_id, $session_data): bool
    {
        return $this->redis->set($session_id, json_encode($session_data), $this->ttl);
    }

    /**
     * @param string $session_id
     *
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function destroy($session_id): bool
    {
        return $this->redis->delete($session_id);
    }

    /**
     * @return bool
     */
    public function close(): bool
    {
        return true;
    }

    /**
     * @param int $maxLifeTime
     *
     * @return bool
     */
    public function gc($maxLifeTime): bool
    {
        return true;
    }

    /**
     * @param string $save_path
     * @param string $name
     *
     * @return bool
     */
    public function open($save_path, $name): bool
    {
        return true;
    }
}
