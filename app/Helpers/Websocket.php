<?php

namespace Eon\Helpers;

use Swoole\Table;
use Swoole\Http\Request;
use Swoole\WebSocket\Server;
use Tightenco\Collect\Support\Collection;

class Websocket
{
    /** @var \Swoole\WebSocket\Server */
    protected $server;
    /**
     * @var \Swoole\Table
     */
    protected $clients;
    /**
     * @var
     */
    protected $container;

    /**
     * Websockets constructor.
     *
     * @param \Swoole\Server $server
     * @param $container
     */
    public function __construct(\Swoole\Server $server, $container)
    {
        $this->container = $container;
        // WS
        $this->clients = new Table(1024);
        $this->clients->column('fd', Table::TYPE_INT);
        $this->clients->column('data', Table::TYPE_STRING, 64);
        $this->clients->column('subscriptions', Table::TYPE_STRING, 2048);
        $this->clients->create();

        $server->on('open', function (Server $server, Request $request) {
            $this->clients[$request->fd] = [
                'id' => $request->fd,
                'data' => json_encode($server->getClientInfo($request->fd)),
                'subscriptions' => '',
            ];
        });

        $server->on('message', function (Server $server, $frame) use (&$clients) {
            $data = $this->isJson($frame->data) ? new Collection(json_decode($frame->data, false)) : new Collection([]);

            if ($data->has('token')) {
                $validTokens = $this->container->get('config')->get('websocket/tokens');
                if (in_array($data->get('token'), $validTokens, true)) {
                    $message = $data->get('message');
                    $this->sendAll(json_encode($message), $server, $clients);
                }
            }

            $this->sendAll(json_encode([$frame->data]), $server, $clients);
            $server->push($frame->fd, json_encode(['success']));
        });

        $server->on('close', static function (Server $server, $fd) use (&$clients) {
            $clients->del($fd);
        });
    }

    /**
     * @param string $message
     * @param \Swoole\WebSocket\Server $server
     * @param \Swoole\Table $clients
     */
    public function sendAll(string $message, Server $server, Table $clients): void
    {
        /**
         * @var  $fd
         * @var \Swoole\Http\Request $client
         */
        foreach ($clients as $fd => $client) {
            if ($server->exist($fd)) {
                $server->push($fd, $message);
            } else {
                $clients->del($fd);
            }
        }
    }

    /**
     * @param string $data
     *
     * @return bool
     */
    protected function isJson(string $data): bool
    {
        json_decode($data, false);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
