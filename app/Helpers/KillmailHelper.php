<?php

namespace Eon\Helpers;

use RuntimeException;
use Eon\Models\TypeIDs;
use Eon\Models\Factions;
use Eon\Models\GroupIDs;
use Eon\Models\Alliances;
use Eon\Models\Killmails;
use Eon\Models\Characters;
use Eon\Models\Celestials;
use Eon\Models\Corporations;
use Eon\Models\SolarSystems;
use MongoDB\BSON\UTCDateTime;
use League\Container\Container;
use Tightenco\Collect\Support\Collection;

class KillmailHelper
{
    protected ESIHelper $esiHelper;
    protected Container $container;
    protected string $imageServerUrl = 'https://images.evetech.net';
    protected Characters $characters;
    protected Corporations $corporations;
    protected Alliances $alliances;
    protected Factions $factions;
    protected SolarSystems $solarSystems;
    protected TypeIDs $typeIDs;
    protected GroupIDs $groupIDs;
    protected Celestials $celestials;
    protected Killmails $killmails;

    public function __construct(Container $container)
    {
        $this->esiHelper = $container->get('esi');
        $this->container = $container;

        $this->characters = $container->get('model/characters');
        $this->corporations = $container->get('model/corporations');
        $this->alliances = $container->get('model/alliances');
        $this->factions = $container->get('model/factions');
        $this->solarSystems = $container->get('model/solarsystems');
        $this->typeIDs = $container->get('model/typeids');
        $this->groupIDs = $container->get('model/groupids');
        $this->celestials = $container->get('model/celestials');
        $this->killmails = $container->get('model/killmails');
    }

    /**
     * @param int    $killID
     * @param string $hash
     *
     * @return array
     */
    public function generateEsiDataFromParsedKillmail(int $killID, string $hash): array
    {
        $killmailData = $this->killmails->getKill($killID, $hash);
        return [
            'attackers' => array_map(static function ($attacker) {
                return [
                    'alliance_id' => $attacker['allianceID'],
                    'character_id' => $attacker['characterID'],
                    'corporation_id' => $attacker['corporationID'],
                    'damage_done' => $attacker['damageDone'],
                    'final_blow' => $attacker['finalBlow'],
                    'security_status' => $attacker['securityStatus'],
                    'ship_type_id' => $attacker['shipTypeID'],
                    'weapon_type_id' => $attacker['weaponTypeID'],
                ];
            }, (array) $killmailData['attackers']),
            'killmail_id' => $killmailData['killID'],
            'killmail_time' => $killmailData['killTime_str'],
            'solar_system_id' => $killmailData['solarSystemID'],
            'victim' => [
                'alliance_id' => $killmailData['victim']['allianceID'],
                'character_id' => $killmailData['victim']['characterID'],
                'corporation_id' => $killmailData['victim']['corporationID'],
                'damage_taken' => $killmailData['victim']['damageTaken'],
                'items' => array_map(static function ($item) {
                    $data = [
                        'flag' => $item['flag'],
                        'item_type_id' => $item['typeID'],
                        'quantity_dropped' => $item['qtyDropped'],
                        'quantity_destroyed' => $item['qtyDestroyed'],
                        'singleton' => $item['singleton'],
                    ];

                    if (isset($item->items)) {
                        $data['items'] = array_map(static function ($i) {
                            return [
                                'flag' => $i['flag'],
                                'item_type_id' => $i['typeID'],
                                'quantity_dropped' => $i['qtyDropped'],
                                'quantity_destroyed' => $i['qtyDestroyed'],
                                'singleton' => $i['singleton'],
                            ];
                        }, (array) $item->items);
                    }

                    return $data;
                }, (array) $killmailData['items']),
                'position' => [
                    'x' => $killmailData['x'],
                    'y' => $killmailData['y'],
                    'z' => $killmailData['z'],
                ],
                'ship_type_id' => $killmailData['victim']['shipTypeID'],
            ],
        ];
    }

    /**
     * @param int    $killId
     * @param string $hash
     * @param int    $warId
     * @param array  $esiData
     *
     * @return array
     */
    public function parse(int $killId, string $hash, int $warId = 0, array $esiData): array
    {
        try {
            $killmail = $this->generateTop($esiData, $killId, $hash, $warId);
            $killmail['victim'] = $this->generateVictim($esiData['victim']);
            $pointValue = $killmail['pointValue'];
            $totalDamage = $killmail['victim']['damageTaken'];
            $killmail['attackers'] = $this->generateAttackers($esiData['attackers'], $pointValue, $totalDamage);
            $killmail['items'] = $this->generateItems($esiData);

            return $killmail;
        } catch (\Exception $e) {
            dd($e->getMessage(), $esiData);
        }
    }

    /**
     * @param array  $data
     * @param int    $killId
     * @param string $hash
     * @param int    $warId
     *
     * @return array
     */
    private function generateTop(array $data, int $killId, string $hash, int $warId): array
    {
        $killTime = strtotime($data['killmail_time']) * 1000;
        $solarSystemData = $this->solarSystems->getAllBySolarSystemID($data['solar_system_id']);
        $killValues = $this->calculateKillValue($data);
        $pointValue = $killValues['totalValue'] === 0 ? 0 : ($killValues['totalValue'] / 10000) / count($data['attackers']);
        $x = $data['victim']['position']['x'] ?? 0;
        $y = $data['victim']['position']['y'] ?? 0;
        $z = $data['victim']['position']['z'] ?? 0;
        $shipTypeID = $data['victim->ship_type_id'] ?? 0;

        $top = [
            'killID' => (int) $killId,
            'hash' => (string) $hash,
            'killTime' => new UTCDateTime($killTime),
            'killTime_str' => $data['killmail_time'],
            'solarSystemID' => $solarSystemData->get('solarSystemID'),
            'solarSystemName' => $solarSystemData->get('solarSystemName'),
            'solarSystemSecurity' => $solarSystemData->get('security'),
            'regionID' => $solarSystemData->get('regionID'),
            'regionName' => $solarSystemData->get('regionName'),
            'near' => $this->getNear($x, $y, $z, $solarSystemData->get('solarSystemID')),
            'x' => $x,
            'y' => $y,
            'z' => $z,
            'shipValue' => (float) $killValues['shipValue'],
            'fittingValue' => (float) $killValues['itemValue'],
            'totalValue' => (float) $killValues['totalValue'],
            'pointValue' => $pointValue,
            'dna' => $this->getDNA($data['victim']['items'], $shipTypeID),
            'isNPC' => $this->isNPC($data),
            'isSolo' => $this->isSolo($data),
            'warID' => $warId,
        ];

        return $top;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function generateVictim(array $data): array
    {
        $characterID = $data['character_id'] ?? 0;
        $corporationID = $data['corporation_id'] ?? 0;
        $allianceID = $data['alliance_id'] ?? 0;
        $factionID = $data['faction_id'] ?? 0;
        $shipTypeID = $data['ship_type_id'] ?? 0;

        $characterInfo = $characterID > 0 ? $this->characters->getById($characterID) : new Collection();
        $corporationInfo = $corporationID > 0 ? $this->corporations->getById($corporationID) : new Collection();
        $allianceInfo = $allianceID > 0 ? $this->alliances->getById($allianceID) : new Collection();
        $factionInfo = $factionID > 0 ? $this->factions->findOne(['corporationID' => $factionID]) : new Collection();

        $shipInfo = $this->typeIDs->getAllByTypeID($shipTypeID);
        $groupData = $this->groupIDs->getAllByGroupID($shipInfo->get('groupID'));

        $shipTypeName = $shipTypeID > 0 ? (array) $shipInfo->get('name') : ['en' => ''];
        $shipGroupName = $shipInfo->get('groupID') > 0 ? (array) $groupData->get('name') : ['en' => ''];
        $victim = [
            'shipTypeID' => $shipTypeID > 0 ? $shipInfo->get('typeID') : 0,
            'shipTypeName' => $shipTypeName['en'],
            'shipImageURL' => $this->imageServerUrl . "/types/{$shipTypeID}/render",
            'shipGroupID' => $shipInfo->get('groupID'),
            'shipGroupName' => $shipGroupName['en'],
            'damageTaken' => $data['damage_taken'],
            'characterID' => $characterID,
            'characterName' => $characterID > 0 ? $characterInfo->get('characterName') : '',
            'characterImageURL' => $this->imageServerUrl . '/characters/' . $characterID . '/portrait',
            'corporationID' => $corporationID,
            'corporationName' => $corporationID > 0 ? $corporationInfo->get('corporationName') : '',
            'corporationImageURL' => $this->imageServerUrl . '/corporations/' . $corporationID . '/logo',
            'allianceID' => $allianceID,
            'allianceName' => $allianceID > 0 ? $allianceInfo->get('allianceName') : '',
            'allianceImageURL' => $this->imageServerUrl . '/alliances/' . $allianceID . '/logo',
            'factionID' => $factionID,
            'factionName' => $factionID > 0 ? $factionInfo->get('factionName') : '',
            'factionImageURL' => $this->imageServerUrl . '/alliances/' . $factionID . '/logo',
        ];

        $this->characters->update(['characterID' => $characterID], ['$inc' => ['losses' => 1]]);
        $this->corporations->update(['corporationID' => $corporationID], ['$inc' => ['losses' => 1]]);
        if ($allianceID > 0) {
            $this->alliances->update(['allianceID' => $allianceID], ['$inc' => ['losses' => 1]]);
        }

        return $victim;
    }

    /**
     * @param $attackers
     * @param $pointValue
     * @param $totalDamage
     *
     * @return array
     */
    private function generateAttackers(array $attackers, $pointValue, $totalDamage): array
    {
        $return = [];

        foreach ($attackers as $data) {
            try {
                $characterID = $data['character_id'] ?? 0;
                $corporationID = $data['corporation_id'] ?? 0;
                $allianceID = $data['alliance_id'] ?? 0;
                $factionID = $data['faction_id'] ?? 0;
                $weaponTypeID = $data['weapon_type_id'] ?? 0;
                $shipTypeID = $data['ship_type_id'] ?? 0;
                $characterInfo = $characterID > 0 ? $this->characters->getById($characterID) : new Collection();
                $corporationInfo = $corporationID > 0 ? $this->corporations->getById($corporationID) : new Collection();
                $allianceInfo = $allianceID > 0 ? $this->alliances->getById($allianceID) : new Collection();
                $factionInfo = $factionID > 0 ? $this->factions->findOne(['corporationID' => $factionID]) : new Collection();

                $weaponTypeData = $weaponTypeID > 0 ? $this->typeIDs->getAllByTypeID($weaponTypeID) : '';
                $shipData = $this->typeIDs->getAllByTypeID($shipTypeID) ?? collect(['groupID' => 0]);
                $groupData = $shipData->get('groupID') > 0 ? $this->groupIDs->getAllByGroupID($shipData->get('groupID')) : collect([]);

                $shipTypeName = $shipTypeID > 0 ? (array) $shipData->get('name') : ['en' => ''];
                $shipGroupName = $shipData->get('groupID') > 0 ? (array) $groupData->get('name') : ['en' => ''];
                $weaponTypeName = $weaponTypeID > 0 ? (array) $weaponTypeData->get('name') : ['en' => ''];
                $inner = [
                    'shipTypeID' => $shipTypeID > 0 ? $shipData->get('typeID') : 0,
                    'shipTypeName' => $shipTypeName['en'] ?? '',
                    'shipImageURL' => $this->imageServerUrl . "/types/{$shipTypeID}/render",
                    'shipGroupID' => $shipData->get('groupID'),
                    'shipGroupName' => $shipGroupName['en'],
                    'characterID' => $characterID,
                    'characterName' => $characterID > 0 ? $characterInfo->get('characterName') : '',
                    'characterImageURL' => $this->imageServerUrl . '/characters/' . $characterID . '/portrait',
                    'corporationID' => $corporationID,
                    'corporationName' => $corporationID > 0 ? $corporationInfo->get('corporationName') : '',
                    'corporationImageURL' => $this->imageServerUrl . '/corporations/' . $corporationID . '/logo',
                    'allianceID' => $allianceID,
                    'allianceName' => $allianceID > 0 ? $allianceInfo->get('allianceName') : '',
                    'allianceImageURL' => $this->imageServerUrl . '/alliances/' . $allianceID . '/logo',
                    'factionID' => $factionID,
                    'factionName' => $factionID > 0 ? $factionInfo->get('factionName') : '',
                    'factionImageURL' => $this->imageServerUrl . '/alliances/' . $factionID . '/logo',
                    'securityStatus' => $data['security_status'],
                    'damageDone' => $data['damage_done'],
                    'finalBlow' => $data['final_blow'],
                    'weaponTypeID' => $weaponTypeID,
                    'weaponTypeName' => $weaponTypeName['en'],
                ];
                if ($data['damage_done'] === 0 || $totalDamage === 0) {
                    $inner['points'] = 0;
                } else {
                    $percentDamage = (int) $data['damage_done'] / $totalDamage;
                    $points = $pointValue * $percentDamage;
                    if ($points > 0) {
                        $inner['points'] = $points;
                        if ($characterID > 0) {
                            $this->characters->update(
                                ['characterID' => $characterID],
                                ['$inc' => ['points' => $inner['points']]]
                            );
                        }
                        if ($corporationID > 0) {
                            $this->corporations->update(
                                ['corporationID' => $corporationID],
                                ['$inc' => ['points' => $inner['points']]]
                            );
                        }
                        if ($allianceID > 0) {
                            $this->alliances->update(
                                ['allianceID' => $allianceID],
                                ['$inc' => ['points' => $inner['points']]]
                            );
                        }
                    }
                }
                if ($characterID > 0) {
                    $this->characters->update(['characterID' => $characterID], ['$inc' => ['kills' => 1]]);
                }
                if ($corporationID > 0) {
                    $this->corporations->update(['corporationID' => $corporationID], ['$inc' => ['kills' => 1]]);
                }
                if ($allianceID > 0) {
                    $this->alliances->update(['allianceID' => $allianceID], ['$inc' => ['kills' => 1]]);
                }
                $return[] = $inner;
            } catch (\Exception $e) {
                throw new RuntimeException($e->getMessage());
            }
        }

        return $return;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function generateItems(array $data): array
    {
        $items = [];

        foreach ($data['victim']['items'] as $item) {
            try {
                $itemData = $this->typeIDs->getAllByTypeID($item['item_type_id']);
                $groupData = new Collection();
                if ($itemData->has('groupID')) {
                    $groupData = $this->groupIDs->getAllByGroupID($itemData->get('groupID'));
                }
                $qtyDropped = $item['quantity_dropped'] ?? 0;
                $qtyDestroyed = $item['quantity_destroyed'] ?? 0;
                $typeName = $itemData->has('name') ? (array) $itemData->get('name') : ['en' => ''];
                $groupName = $groupData->has('name') ? (array) $groupData->get('name') : ['en' => ''];
                $items[] = [
                    'typeID' => $item['item_type_id'],
                    'typeName' => $typeName['en'],
                    'typeImageURL' => $this->imageServerUrl . '/types/' . $item['item_type_id'] . '/icon',
                    'groupID' => $itemData->get('groupID'),
                    'groupName' => $groupName['en'],
                    'categoryID' => $groupData->get('categoryID'),
                    'flag' => $item['flag'],
                    'qtyDropped' => $qtyDropped,
                    'qtyDestroyed' => $qtyDestroyed,
                    'singleton' => $item['singleton'],
                    'value' => $this->getPriceForTypeID($item['item_type_id'], $data['killmail_time']),
                ];
            } catch (\Exception $e) {
                throw new \RuntimeException($e->getMessage());
            }
        }

        return $items;
    }

    /**
     * @param     $x
     * @param     $y
     * @param     $z
     * @param int $solarSystemId
     *
     * @return string
     */
    private function getNear($x, $y, $z, int $solarSystemId): string
    {
        if ($x === 0 && $y === 0 && $z === 0) {
            return '';
        }

        /** @var \Tightenco\Collect\Support\Collection $celestials */
        $celestials = $this->celestials->find(['solarSystemID' => $solarSystemId]);
        $minimumDistance = null;
        $celestialName = '';

        foreach ($celestials as $celestial) {
            $distance = sqrt((($celestial->x - $x) ** 2) + (($celestial->y - $y) ** 2) + (($celestial->z - $z) ** 2));

            if ($minimumDistance === null || $distance >= $minimumDistance) {
                $minimumDistance = $distance;
                $celestialName = $this->fillInCelestialName($celestial);
            }
        }

        return $celestialName;
    }

    /**
     * @param $celestial
     *
     * @return string
     */
    private function fillInCelestialName($celestial): string
    {
        $celestialName = '';
        $types = ['Stargate', 'Moon', 'Planet', 'Asteroid Belt', 'Sun'];
        foreach ($types as $type) {
            if (isset($celestial->typeName) && strpos($celestial->typeName, $type) !== false) {
                $string = $type;
                $string .= ' (';
                $string .= $celestial->itemName ?? $celestial->solarSystemName;
                $string .= ')';
                $celestialName = $string;
            }
        }

        return $celestialName;
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function calculateKillValue(array $data): array
    {
        if (empty($data)) {
            return ['itemValue' => 0, 'shipValue' => 0, 'totalValue' => 0];
        }

        $shipTypeID = $data['victim']['ship_type_id'] ?? 0;
        $victimShipValue = $this->getPriceForTypeID($shipTypeID, $data['killmail_time']);
        $killValue = 0;

        foreach ($data['victim']['items'] as $item) {
            $isCargo = isset($item['items']) ? true : false;
            if ($isCargo) {
                foreach ($item['items'] as $innerItem) {
                    $killValue += $this->getItemValue($innerItem, $isCargo, $data['killmail_time']);
                }
            }

            $killValue += $this->getItemValue($item, $isCargo, $data['killmail_time']);
        }

        return ['itemValue' => $killValue, 'shipValue' => $victimShipValue, 'totalValue' => $killValue + $victimShipValue];
    }

    /**
     * @param int         $typeID
     * @param string|null $date
     *
     * @return float
     */
    private function getPriceForTypeID(int $typeID, string $date = null): float
    {
        /** @var \Eon\Models\Prices $data */
        $data = $this->container->get('model/prices');
        $value = $data->getPriceByTypeId($typeID, $date);

        return $value->get('price');
    }

    /**
     * @param      $item
     * @param bool $isCargo
     *
     * @return float
     */
    private function getItemValue(array $item, $isCargo = false, string $date = null): float
    {
        $typeID = $item['item_type_id'];
        $flag = $item['flag'];
        $id = $this->typeIDs->getAllByTypeID($typeID);

        $itemName = null;
        if ($id->has('name')) {
            $itemName = (array) $id->get('name');
            $itemName = $itemName['en'];
        }

        if (!$itemName) {
            $itemName = 'TypeID ' . $typeID;
        }

        // Golden Pod
        if ($typeID === 33329 && $flag === 89) {
            $price = 0.01;
        } else {
            $price = $this->getPriceForTypeID($typeID, $date);
        }

        if ($isCargo && strpos($itemName, 'Blueprint') !== false) {
            $item['singleton'] = 2;
        }

        if ($item['singleton'] === 2) {
            $price /= 100;
        }

        $dropped = $item['quantity_dropped'] ?? 0;
        $destroyed = $item['quantity_destroyed'] ?? 0;
        return ($price * ($dropped + $destroyed));
    }

    /**
     * @param array $data
     * @param       $shipTypeID
     *
     * @return string
     */
    private function getDNA(array $data, $shipTypeID): string
    {
        /** @var \Eon\Models\InvFlags $invFlags */
        $invFlags = $this->container->get('model/invflags');

        $slots = [
            'LoSlot0','LoSlot1','LoSlot2','LoSlot3','LoSlot4','LoSlot5','LoSlot6','LoSlot7','MedSlot0',
            'MedSlot1','MedSlot2','MedSlot3','MedSlot4','MedSlot5','MedSlot6','MedSlot7','HiSlot0','HiSlot1','HiSlot2',
            'HiSlot3','HiSlot4','HiSlot5','HiSlot6','HiSlot7','DroneBay','RigSlot0','RigSlot1','RigSlot2','RigSlot3',
            'RigSlot4','RigSlot5','RigSlot6','RigSlot7','SubSystem0','SubSystem1','SubSystem2','SubSystem3',
            'SubSystem4','SubSystem5','SubSystem6','SubSystem7','SpecializedFuelBay',
        ];

        $fittingArray = [];
        $fittingString = $shipTypeID . ':';

        foreach ($data as $item) {
            $flagName = $invFlags->findOne(['flagID' => $item['flag']])->get('flagName');
            $categoryID = $item['category_id'] ?? 0;
            if ($categoryID === 8 || in_array($flagName, $slots, false)) {
                $typeID = $item['item_type_id'] ?? 0;
                $dropped = $item['quantity_dropped'] ?? 0;
                $destroyed = $item['quantity_destroyed'] ?? 0;
                if (isset($fittingArray[$typeID])) {
                    $fittingArray[$typeID]['count'] += ($dropped + $destroyed);
                } else {
                    $fittingArray[$typeID] = ['count' => $dropped + $destroyed];
                }
            }
        }

        foreach ($fittingArray as $key => $item) {
            $fittingString .= "{$key};{$item['count']}:";
        }
        $fittingString .= ':';
        return $fittingString;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function isNPC(array $data): bool
    {
        $npc = 0;
        $calc = 0;
        $kdCount = count($data['attackers']);

        foreach ($data['attackers'] as $attacker) {
            $characterID = $attacker['character_id'] ?? 0;
            $corporationID = $attacker['corporation_id'] ?? 0;
            $npc += $characterID === 0 && ($corporationID < 1999999 && $corporationID !== 1000125) ? 1 : 0;
        }

        if ($kdCount > 0 && $npc > 0) {
            $calc = count($data['attackers']) / $npc;
        }

        return $calc === 1;
    }

    /**
     * @param $data
     *
     * @return bool
     */
    private function isSolo(array $data): bool
    {
        $npc = 0;
        $calc = 0;
        $kdCount = count($data['attackers']);

        if ($kdCount > 2) {
            return false;
        } elseif ($kdCount === 1) {
            return true;
        }

        foreach ($data['attackers'] as $attacker) {
            $characterID = $attacker['character_id'] ?? 0;
            $corporationID = $attacker['corporation_id'] ?? 0;
            $npc += $characterID === 0 && ($corporationID < 1999999 && $corporationID !== 1000125) ? 1 : 0;
        }
        if ($npc > 0) {
            $calc = 2 / $npc;
        }

        return $calc === 2;
    }
}
