<?php

namespace Eon\Helpers;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Twig
{
    /**
     * @var \Eon\Helpers\Config
     */
    public $config;
    /**
     * @var \Twig\Environment
     */
    public $twig;

    public function __construct(Config $config)
    {
        $this->config = $config;

        // Load twig
        $loader = new FilesystemLoader($this->config->get('twig/templatePath', __DIR__ . '/../../app/Templates'));
        $twig = new Environment($loader, [
            'cache' => $this->config->get('twig/cachePath', __DIR__ . '/../../resources/cache'),
            'debug' => $this->config->get('twig/debug', false),
            'auto_reload' => $this->config->get('twig/auto_reload', true),
            'strict_variables' => $this->config->get('twig/strict_variables', true),
            'optimizations' => $this->config->get('twig/optimizations', true),
        ]);

        $this->twig = $twig;
    }

    public function render(string $template, array $data = [])
    {
        if (pathinfo($template, PATHINFO_EXTENSION) !== 'twig') {
            $template .= '.twig';
        }

        return $this->twig->render($template, $data);
    }
}
