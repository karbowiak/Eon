<?php

namespace Eon\Helpers;

use Whoops\Handler\HandlerInterface;
use Whoops\Handler\PrettyPageHandler;

class WhoopsHandlerContainer extends \Middlewares\WhoopsHandlerContainer
{
    /**
     * {@inheritdoc}
     *
     * @return HandlerInterface
     */
    public function get($id)
    {
        $format = self::getPreferredFormat($id);

        return $this->$format();
    }

    protected function html(): HandlerInterface
    {
        $handler = new PrettyPageHandler();
        $handler->handleUnconditionally(true);

        return $handler;
    }

    /**
     * Returns the preferred format used by whoops.
     *
     * @param string $accept
     *
     * @return string
     */
    protected static function getPreferredFormat(string $accept): string
    {
        $formats = [
            'json' => ['application/json'],
            'html' => ['text/html'],
            'xml' => ['text/xml'],
            'plain' => ['text/plain', 'text/css', 'text/javascript'],
        ];

        foreach ($formats as $format => $mimes) {
            foreach ($mimes as $mime) {
                if (stripos($accept, $mime) !== false) {
                    return $format;
                }
            }
        }

        return 'unknown';
    }
}
