<?php

namespace Eon\Helpers;

use Eon\Bootstrap;
use http\Message\Body;
use Nyholm\Psr7\Stream;
use XMLParser\XMLParser;
use Nyholm\Psr7\Response;
use MongoDB\BSON\UTCDateTime;
use Rakit\Validation\ErrorBag;
use Bloatless\WebSocket\Client;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validator;
use Tightenco\Collect\Support\Collection;

class Controller
{
    public bool $protected = false;
    protected ServerRequestInterface $request;
    protected ResponseInterface $response;
    protected Bootstrap $container;
    private array $preload = [];
    protected Validator $validator;
    protected ErrorBag $validatorErrors;
    protected Collection $arguments;

    public function __construct(Bootstrap $bootstrap)
    {
        $this->container = $bootstrap;
        $this->validator = new Validator();
    }

    public function __invoke($actionName = 'handle')
    {
        $controller = $this;

        return function (ServerRequestInterface $request, Response $response, $args) use ($controller, $actionName) {
            $controller->arguments = new Collection($args);
            $controller->setRequest($request);
            $controller->setResponse($response);

            return call_user_func_array([$controller, $actionName], $args);
        };
    }

    /**
     * @param $request
     */
    protected function setRequest(ServerRequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * @param $response
     */
    protected function setResponse(Response $response): void
    {
        $this->response = $response;
    }

    /**
     * Get a single route argument
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getArg(string $key)
    {
        if ($this->getArgs()->has($key)) {
            return $this->getArgs()->get($key);
        }
        return null;
    }

    /**
     * Get route arguments
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    protected function getArgs(): Collection
    {
        return $this->arguments;
    }

    /**
     * Return a single POST/GET Param
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getParam(string $key)
    {
        return $this->getParams()->get($key);
    }

    /**
     * Return all POST/GET Params
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    protected function getParams(): Collection
    {
        return new Collection($this->request->getQueryParams());
    }

    /**
     * Return a single POST Param
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getPostParam(string $key)
    {
        return $this->getPostParams()->get($key);
    }

    /**
     * Return all POST Params
     *
     * @return Collection
     */
    protected function getPostParams(): Collection
    {
        $post = array_diff_key($this->request->getParsedBody(), array_flip([
            '_METHOD',
        ]));

        return new Collection($post);
    }

    /**
     * Get the files posted
     *
     * @return Collection
     */
    protected function getFiles(): Collection
    {
        $files = array_diff_key($this->request->getUploadedFiles(), array_flip([
            '_METHOD',
        ]));

        return new Collection($files);
    }

    /**
     * Get a single request header
     *
     * @param string $key
     *
     * @return string
     */
    protected function getHeader(string $key): ?string
    {
        return $this->getHeaders()->get($key);
    }

    /**
     * Get all request headers
     *
     * @return Collection
     */
    protected function getHeaders(): Collection
    {
        return new Collection($this->request->getHeaders());
    }

    /**
     * Tells the web client to preload a resource, can be image, css, media, etc.
     * Refer to https://www.w3.org/TR/preload/#server-push-(http/2) for more info
     *
     * @param string $urlPath local path or remote http/https
     */
    protected function preload(string $urlPath): void
    {
        $this->preload[] = "<{$urlPath}>; rel=preload;";
    }

    /**
     * @param $dateTime
     *
     * @return UTCDateTime
     */
    protected function makeTimeFromDateTime(string $dateTime): UTCDateTime
    {
        $unixTime = strtotime($dateTime);
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    /**
     * @param $unixTime
     *
     * @return UTCDateTime
     */
    protected function makeTimeFromUnixTime(int $unixTime): UTCDateTime
    {
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    /**
     * Render the data as xml output
     *
     * @param array|Collection $data
     * @param int $status
     * @param String $contentType
     * @param int $cacheTime
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function xml($data = [], int $cacheTime = 30, int $status = 200, String $contentType = 'application/xml; charset=UTF-8'): ResponseInterface
    {
        $response = $this->generateResponse($status, $contentType, $cacheTime);
        $body = $this->body(XMLParser::encode($data, 'response'));

        return $response->withBody($body);
    }

    /**
     * Output html data
     *
     * @param string $htmlData
     * @param int $cacheTime
     * @param int $status
     * @param string $contentType
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function html(string $htmlData, int $cacheTime = 0, int $status = 200, string $contentType = 'text/html'): ResponseInterface
    {
        $response = $this->generateResponse($status, $contentType, $cacheTime);
        $body = $this->body($htmlData);

        return $response->withBody($body);
    }

    /**
     * Render the data as json output
     *
     * @param array|Collection $data
     * @param int $status
     * @param String $contentType
     * @param int $cacheTime
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function json($data = [], int $cacheTime = 30, int $status = 200, String $contentType = 'application/json; charset=UTF-8'): ResponseInterface
    {
        $response = $this->generateResponse($status, $contentType, $cacheTime);
        $body = $this->body(json_encode($data, JSON_NUMERIC_CHECK));

        return $response->withBody($body);
    }

    /**
     * Renders a twig template
     *
     * @param string $template
     * @param array|Collection $data
     * @param int $cacheTime
     * @param int $status
     * @param string $contentType
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function render(string $template, $data = [], int $cacheTime = 0, int $status = 200, string $contentType = 'text/html'): ResponseInterface
    {
        $twig = $this->container->twig;
        $render = $twig->render($template, $data);

        $response = $this->generateResponse($status, $contentType, $cacheTime);
        $body = $this->body($render);
        return $response->withBody($body);
    }

    /**
     * Generates the response for the output types, render, json, xml and html
     *
     * @param int $status
     * @param string $contentType
     * @param int $cacheTime
     *
     * @return ResponseInterface
     */
    protected function generateResponse(int $status, string $contentType, int $cacheTime): ResponseInterface
    {
        $response = $this->response->withStatus($status)
            ->withHeader('Content-Type', $contentType)
            ->withAddedHeader('Access-Control-Allow-Origin', '*')
            ->withAddedHeader('Access-Control-Allow-Methods', '*');

        if ($cacheTime > 0) {
            $response = $response
                ->withAddedHeader('Expires', gmdate('D, d M Y H:i:s', time() + $cacheTime))
                ->withAddedHeader('Cache-Control', "public, max-age={$cacheTime}, proxy-revalidate");
        }

        if (!empty($this->preload)) {
            foreach ($this->preload as $preload) {
                $response = $response->withAddedHeader('Link', $preload);
            }
        }

        return $response;
    }

    /**
     * Generate a new body with input data
     *
     * @param $data
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    private function body($data): StreamInterface
    {
        $body = Stream::create();
        $body->write($data);

        return $body;
    }

    /**
     * Get the full path of the request (http://mydomain.tld/request/requestData)
     * @return string
     */
    protected function getFullPath(): string
    {
        $port = $this->request->getServerParams()['SERVER_PORT'];

        return "{$this->request->getUri()->getScheme()}://{$this->request->getUri()->getHost()}:{$port}/{$this->request->getUri()->getPath()}";
    }

    /**
     * Get the full host of the request (http://mydomain.tld/)
     * @return string
     */
    protected function getFullHost(): string
    {
        $port = $this->request->getServerParams()['SERVER_PORT'];

        return "{$this->request->getUri()->getScheme()}://{$this->request->getUri()->getHost()}:{$port}";
    }

    /**
     * is the incoming request from a cellphone?
     *
     * @return bool
     */
    protected function isMobile(): bool
    {
        return preg_match(
            "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",
            $this->getHeader('user-agent')
        );
    }

    /**
     * Redirect.
     *
     * Note: This method is not part of the PSR-7 standard.
     *
     * This method prepares the response object to return an HTTP Redirect
     * response to the client.
     *
     * @param string $url The redirect destination.
     *
     * @return ResponseInterface
     */
    protected function redirect($url): ResponseInterface
    {
        return $this->response->withAddedHeader('Location', $url);
    }

    /**
     * @param array $rules
     * @param array $input
     *
     * @return bool
     */
    protected function validate(array $rules = [], array $input = []): bool
    {
        $input = !empty($input) ? $input : $this->request->getQueryParams();
        $validation = $this->validator->make($input, $rules);
        $validation->validate();

        if ($validation->fails()) {
            $this->validatorErrors = $validation->errors();
            return false;
        }
        return true;
    }
}
