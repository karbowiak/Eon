<?php

namespace Eon\Helpers;

use Exception;
use Psr\SimpleCache\CacheInterface;
use Traversable;

class Redis implements CacheInterface
{
    /**
     * @var \Redis
     */
    protected $redis;
    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    private function isValidKey(string $key): void
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException("{$key} is not a string");
        }
    }

    /**
     * Fetches a value from the cache.
     *
     * @param string $key The unique key of this item in the cache.
     * @param mixed $default Default value to return if the key does not exist.
     *
     * @return mixed The value of the item from the cache, or $default in case of cache miss.
     * @throws \Psr\SimpleCache\InvalidArgumentException|\JsonException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function get($key, $default = null)
    {
        $this->isValidKey($key);
        return $this->has($key) ?
            json_decode($this->redis->get($key), true, 512, JSON_THROW_ON_ERROR) :
            $default;
    }

    /**
     * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
     *
     * @param string $key The key of the item to store.
     * @param mixed $value The value of the item to store, must be serializable.
     * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
     *                                      the driver supports TTL then the library may set a default value
     *                                      for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function set($key, $value, $ttl = null)
    {
        if ($ttl > 0) {
            $this->isValidKey($key);

            return $this->redis->set($key, json_encode($value), $ttl);
        }

        return false;
    }

    /**
     * Delete an item from the cache by its unique key.
     *
     * @param string $key The unique cache key of the item to delete.
     *
     * @return bool True if the item was successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function delete($key)
    {
        $this->isValidKey($key);
        return $this->redis->delete($key);
    }

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear(): bool
    {
        return $this->redis->flushDB();
    }

    /**
     * Obtains multiple cache items by their unique keys.
     *
     * @param iterable $keys A list of keys that can obtained in a single operation.
     * @param mixed $default Default value to return for keys that do not exist.
     *
     * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function getMultiple($keys, $default = null)
    {
        $keys = $this->getAsArray($keys);
        $return = $this->redis->mget($keys);

        if (empty($return) || $return === null) {
            return $default;
        }
        return $return;
    }

    /**
     * Persists a set of key => value pairs in the cache, with an optional TTL.
     *
     * @param iterable $values A list of key => value pairs for a multiple-set operation.
     * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
     *                                       the driver supports TTL then the library may set a default value
     *                                       for it or let the driver take care of that.
     *
     * @return bool True on success and false on failure.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $values is neither an array nor a Traversable,
     *   or if any of the $values are not a legal value.
     */
    public function setMultiple($values, $ttl = null): bool
    {
        if ($ttl > 0) {
            $values = $this->getAsArray($values);
            $multi = $this->redis->multi();
            foreach ($values as $key => $value) {
                $multi->set($key, $value, $ttl);
            }
            $multi->exec();

            return true;
        }

        return false;
    }

    /**
     * Deletes multiple cache items in a single operation.
     *
     * @param iterable $keys A list of string-based keys to be deleted.
     *
     * @return bool True if the items were successfully removed. False if there was an error.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if $keys is neither an array nor a Traversable,
     *   or if any of the $keys are not a legal value.
     */
    public function deleteMultiple($keys): bool
    {
        $keys = $this->getAsArray($keys);
        $multi = $this->redis->multi();
        foreach ($keys as $key) {
            $multi->delete($key);
        }
        $multi->exec();

        return true;
    }

    /**
     * Determines whether an item is present in the cache.
     *
     * NOTE: It is recommended that has() is only to be used for cache warming type purposes
     * and not to be used within your live applications operations for get/set, as this method
     * is subject to a race condition where your has() will return true and immediately after,
     * another script can remove it making the state of your app out of date.
     *
     * @param string $key The cache item key.
     *
     * @return bool
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     *   MUST be thrown if the $key string is not a legal value.
     */
    public function has($key): bool
    {
        $this->isValidKey($key);
        return $this->redis->exists($key);
    }

    private function getAsArray($keys)
    {
        if ($keys instanceof Traversable) {
            return iterator_to_array($keys);
        }

        if (is_array($keys)) {
            return $keys;
        }

        throw new InvalidArgumentException('Value must be an array');
    }
}

class InvalidArgumentException extends Exception implements \Psr\SimpleCache\InvalidArgumentException
{
}
