<?php

namespace Eon;

use Eon\Helpers\Twig;
use Nyholm\Psr7\Stream;
use Nyholm\Psr7\Response;
use Eon\Helpers\ESIHelper;
use Slim\Factory\AppFactory;
use Eon\Helpers\PhpSession;
use Eon\Helpers\Queue;
use Eon\Helpers\KillmailHelper;
use Slim\Middleware\ErrorMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use Eon\Helpers\WhoopsHandlerContainer;
use League\Container\Container;
use Middlewares\ResponseTime;
use Middlewares\Whoops;
use MongoDB\Client;
use Redis;
use Slim\App;
use Symfony\Component\Yaml\Yaml;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpMethodNotAllowedException;

/**
 * Class Bootstrap
 * @package Eon
 *
 * Defined below, are the various classes available through the container
 * This allows for simpler fetching of them when only bootstrapper is available
 *
 * @property \Eon\Helpers\Mongo mongo
 * @property Client mongoConnection
 * @property \Eon\Helpers\Redis redis
 * @property Redis redisConnection
 * @property \Eon\Helpers\Config config
 * @property \Eon\Helpers\Twig twig
 * @property \Eon\Helpers\PhpSession session
 * @property \Eon\Helpers\Queue queue
 */
class Bootstrap
{
    /**
     * @var \Eon\Helpers\Config
     */
    public Helpers\Config $config;
    /**
     * @var Container
     */
    public Container $container;
    /**
     * @var App
     */
    public App $app;
    /**
     * @var Psr17Factory
     */
    public Psr17Factory $psr17Factory;

    /**
     * Bootstrap constructor.
     */
    public function __construct()
    {
        $this->initConfig();
    }

    /**
     * Initializes configuration
     */
    protected function initConfig(): void
    {
        $this->config = new Helpers\Config();
    }

    /**
     * Initializes the container
     */
    public function initContainer(): void
    {
        $container = new Container();

        // Default container items
        $container->add('container', $container);
        $container->add('config', $this->config);
        $container->add('mongoConnection', $this->initMongoDb());
        $container->add('redisConnection', $this->initRedis());
        $container->add('redis', Helpers\Redis::class)->addArgument('redisConnection');
        $container->add('session', PhpSession::class)->addArgument('redis');
        $container->add('queue', Queue::class)->addArgument('config');
        $container->add('esi', ESIHelper::class)->addArgument('container');
        $container->add('killmail', KillmailHelper::class)->addArgument('container');

        // Load user models
        foreach ([__DIR__ . '/../app/Models/*.php', __DIR__ . '/../app/Models/*/*.php'] as $path) {
            $models = glob($path);
            foreach ($models as $model) {
                require_once realpath($model);
                $className = str_replace('.php', '', basename($model));
                $class = '\\Eon\\Models\\' . $className;
                $container->add('model/' . strtolower($className), $class)->addArguments([
                    'config',
                    'redis',
                    'mongoConnection',
                    'container',
                ]);
            }
        }

        $this->container = $container;
    }

    /**
     * @return \Redis
     */
    protected function initRedis(): Redis
    {
        $redis = new Redis();
        $dbServer = !empty($_ENV['EON_REDIS_DB']) ? $_ENV['EON_REDIS_DB'] : 'redis';
        $dbPort = !empty($_ENV['EON_REDIS_PORT']) ? (int) $_ENV['EON_REDIS_PORT'] : 6379;
        $auth = !empty($_ENV['EON_REDIS_AUTH']) ? $_ENV['EON_REDIS_AUTH'] : '';
        $redis->pconnect($dbServer, $dbPort, 2.5, $this->config->get('redis/id', 'eon'));
        if (!empty($auth)) {
            $redis->auth($auth);
        }

        return $redis;
    }

    /**
     * @return \MongoDB\Client
     */
    protected function initMongoDb(): Client
    {
        $dbServers = !empty($_ENV['EON_MONGO_DB']) ? explode(',', $_ENV['EON_MONGO_DB']) : [
            'mongodb:27017',
        ];

        $dbSettings = !empty($_ENV['EON_MONGO_SETTINGS']) ? $_ENV['EON_MONGO_SETTINGS'] : 'connectTimeoutMS=50000&socketTimeoutMS=50000';

        $connectString = 'mongodb://';
        foreach ($dbServers as $server) {
            $connectString .= $server . ',';
        }
        $connectString = rtrim($connectString, ',');
        $connectString .= '/?' . $dbSettings;

        // Options
        $mongoOptions = !empty($_ENV['EON_MONGO_OPTIONS']) ? explode(',', $_ENV['EON_MONGO_OPTIONS']) : [
            'mongodb',
        ];

        return new Client(
            $connectString,
            $mongoOptions,
            [
                'typeMap' => ['root' => 'object', 'document' => 'object', 'array' => 'object'],
                'connectTimeoutMS' => 6000, // 6s connection initiate timeout
                'socketTimeoutMS' => 60000, // 60s connection waiting for data timeout
            ]
        );
    }

    /**
     */
    public function ensureIndexes(): void
    {
        $client = $this->container->get('mongoConnection');
        $indexes = glob(__DIR__ . '/../resources/database/options/*.yml');
        foreach ($indexes as $index) {
            $data = Yaml::parseFile($index);
            $databaseName = $data['database'];
            $collectionName = str_replace('.yml', '', basename($index));

            if ($databaseName !== null && $collectionName !== null) {
                foreach ($data['index'] as $data) {
                    try {
                        $client->selectCollection($databaseName, $collectionName)->createIndex($data['key'], ['maxTimeMS' => 999999999, 'unique' => $data['unique'] ?? false]);
                    } catch (\Exception $e) {
                    }
                }
            }
        }
    }

    /**
     * @return \Slim\App
     */
    public function getSlim(): App
    {
        return $this->app;
    }

    /**
     * @param bool $production
     */
    public function initSlim(bool $production = true): void
    {
        // Init Session handler
        session_set_save_handler($this->session, true);
        session_cache_limiter(false);

        // Add Twig
        $twig = new Twig($this->config);
        $this->container->add('twig', $twig);

        // Start PSR17 Factory
        $this->psr17Factory = new Psr17Factory();

        // Init App
        $this->app = AppFactory::create($this->psr17Factory, $this->container);

        // Error middleware (404 and 405)
        $errorMiddleware = new ErrorMiddleware(
            $this->app->getCallableResolver(),
            $this->app->getResponseFactory(),
            true,
            false,
            false
        );

        // 404
        $errorMiddleware->setErrorHandler(HttpNotFoundException::class, function ($request, $exception, $displayErrorDetails, $logErrors, $logErrorDetails) use ($twig) {
            $response = new Response();
            $body = Stream::create();
            $body->write($twig->render('404'));

            return $response->withBody($body);
        });

        // 405
        $errorMiddleware->setErrorHandler(HttpMethodNotAllowedException::class, function ($request, $exception, $displayErrorDetails, $logErrors, $logErrorDetails) use ($twig) {
            $response = new Response();
            $body = Stream::create();
            $body->write($twig->render('405'));

            return $response->withBody($body);
        });

        // Init the routes
        $this->initRoutes();

        // Add Response Time
        $this->app->add(new ResponseTime());

        // Add error middleware
        $this->app->add($errorMiddleware);

        // Add middleware
        $customMiddleware = glob(__DIR__ . '/Middleware/*.php');
        foreach ($customMiddleware as $middleware) {
            require_once($middleware);
            $class = '\\Eon\\Middleware\\' . str_replace('.php', '', basename($middleware));
            $middleware = new $class($this->container);
            $this->app->add($middleware);
        }

        if ($this->config->get('app/debug')) {
            $whoops = new Whoops();
            $whoops->handlerContainer(new WhoopsHandlerContainer());
            $this->app->add($whoops);
        }
    }

    /**
     *
     */
    protected function initRoutes(): void
    {
        // Set basepath
        $this->app->setBasePath('/');

        // Load controllers
        $controllers = array_merge(glob(__DIR__ . '/Controllers/*.php'), glob(__DIR__ . '/Controllers/*/*.php'));
        $routes = [];
        foreach ($controllers as $controller) {
            // This is temporary, once Vue is done all frontend will go away - including this..
            $isApi = strpos($controller, 'API') !== false;
            if ($isApi) {
                $class = '\\Eon\\Controllers\\API\\' . str_replace('.php', '', basename($controller));
            } else {
                $class = '\\Eon\\Controllers\\' . str_replace('.php', '', basename($controller));
            }

            $reader = new \DocBlockReader\Reader(file_get_contents($controller));
            $routeParameter = $reader->getParameter('route');
            $docRoutes = !is_array($routeParameter) ? [$routeParameter] : $routeParameter;
            foreach ($docRoutes as $docRoute) {
                $data = explode('|', $docRoute);
                $route = $isApi ? '/api/' . strtolower(explode('\\', $class)[4]) . trim($data[1] ?? '') : trim($data[1] ?? '');
                $type = trim($data[0] ?? '');
                $method = trim($data[2] ?? '');
                if (empty($type)) {
                    continue;
                }

                $routes[] = [
                    'api' => $isApi,
                    'class' => $class,
                    'route' => $route,
                    'type' => $type,
                    'method' => $method,
                ];
            }
        }
        foreach ($routes as $route) {
            $loaded = new $route['class']($this);
            $this->app->map([$route['type']], ltrim($route['route'], '/'), $loaded($route['method']));
        }
    }

    /**
     * @return \League\Container\Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Returns the requested element from the container
     *
     * @param $name
     *
     * @return array|mixed|object
     */
    public function __get($name)
    {
        if ($this->container->has($name)) {
            return $this->container->get($name);
        }
        return null;
    }

    /**
     * @param $name
     *
     * @return array|mixed|object
     */
    public function get($name)
    {
        if ($this->container->has($name)) {
            return $this->container->get($name);
        }
        return null;
    }
}
