<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Skins extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'skins';
    protected $indexField = 'skinID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByInternalName($fieldName): Collection
    {
        return $this->find(['internalName' => $fieldName]);
    }

    public function getAllBySkinID($skinID): Collection
    {
        return $this->find(['skinID' => $skinID]);
    }

    public function getAllBySkinMaterialID($skinMaterialID): Collection
    {
        return $this->find(['skinMaterialID' => $skinMaterialID]);
    }
}
