<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Celestials extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'celestials';
    protected $indexField = 'solarSystemID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
}
