<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class SkinLicenses extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'skinLicenses';
    protected $indexField = 'skinID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByLicenseTypeID($licenseTypeID): Collection
    {
        return $this->find(['licenseTypeID' => $licenseTypeID]);
    }

    public function getAllBySkinID($skinID): Collection
    {
        return $this->find(['skinID' => $skinID]);
    }
}
