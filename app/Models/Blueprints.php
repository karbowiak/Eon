<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Blueprints extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'blueprints';
    protected $indexField = 'blueprintTypeID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByBlueprintTypeId(int $blueprintTypeId)
    {
        return $this->find(['blueprintTypeID' => $blueprintTypeId]);
    }
}
