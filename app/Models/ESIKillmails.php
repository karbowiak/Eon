<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class ESIKillmails extends ModelInterface
{
    public $collectionName = 'killmails-esi';
    protected $indexField = 'killID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function exists(int $killID): bool
    {
        return $this->findOne(['killID' => $killID])->isNotEmpty();
    }

    public function getEsiData(int $killID): array
    {
        return (array) json_decode(json_encode($this->findOne(['killID' => $killID])->get('esidata')), true);
    }

    public function getUnparsed(int $limit = 10000): ?array
    {
        $unparsed = $this->find(['parsed' => false], ['limit' => $limit, 'sort' => ['killID' => -1]])->toArray();
        return $unparsed ?? null;
    }
}
