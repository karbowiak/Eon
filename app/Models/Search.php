<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Search extends ModelInterface
{
    public $collectionName = 'search';
    protected $indexField = '';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function search(
        string $searchTerm,
        array $searchIn = [
            'faction',
            'alliance',
            'corporation',
            'character',
            'system',
            'region',
            'item',
            'celestial',
            'constellation',
        ],
        int $limit = 5
    ): array {
        $valid = [
            'faction',
            'alliance',
            'corporation',
            'character',
            'system',
            'region',
            'item',
            'celestial',
            'constellation',
        ];
        $searchArray = [];
        if (!is_array($searchIn)) {
            $searchIn = [$searchIn];
        }
        foreach ($searchIn as $lookIn) {
            if (in_array($lookIn, $valid, false)) {
                $singleSearch = $this->$lookIn($searchTerm, 1);
                $multiSearch = $this->$lookIn($searchTerm, $limit);
                if (count($multiSearch) >= 1) {
                    $searchArray[$lookIn] = $multiSearch;
                } else {
                    $searchArray[$lookIn] = $singleSearch;
                }
            }
        }

        return $searchArray;
    }

    private function faction(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/factions');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function alliance(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/alliances');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function corporation(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/corporations');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function character(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/characters');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function item(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/typeids');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function system(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/solarsystems');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function region(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/regions');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function constellation(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/constellations');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }

    private function celestial(string $searchTerm, int $limit = 5): array
    {
        /** @var \Eon\Models\Factions $collection */
        $collection = $this->container->get('model/celestials');

        return $collection->find(['$text' => ['$search' => "\"$searchTerm\""]], [
            'score' => ['$meta' => 'textScore'],
            'sort' => ['$score' => -1],
            'projection' => ['_id' => 0],
            'limit' => $limit,
        ])->toArray();
    }
}
