<?php

namespace Eon\Models;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Alliances extends ModelInterface
{
    public $collectionName = 'alliances';
    protected $indexField = 'allianceID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getById(int $allianceID, int $depth = 0): Collection
    {
        /** @var Collection $result */
        $result = $this->findOne(['allianceID' => $allianceID]);
        if($result->isEmpty()) {
            $depth++;
            $result = $this->esi($allianceID, $depth);
            $this->setData($result->toArray());
            $this->save();
        }

        return $result;
    }

    public function getInformation(int $allianceId): Collection
    {
        return $this->findOne(['allianceID' => $allianceId], ['projection' => ['_id' => 0]]);
    }

    public function getMembers(int $allianceId): Collection
    {
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');

        return $characters->find(['allianceID' => $allianceId], ['projection' => ['_id' => 0]]);
    }

    public function esi(int $allianceID, int $depth = 0): Collection
    {
        /** @var \Eon\Helper\ESIHelper $esi */
        $esi = $this->container->get('esi');
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');
        /** @var \Eon\Models\Corporations $corporations */
        $corporations = $this->container->get('model/corporations');
        /** @var \Eon\Models\Factions $factions */
        $factions = $this->container->get('model/factions');

        $data = $esi->getAllianceData($allianceID);
        $creatorID = $data->creator_id ?? 0;
        $creatorCorpID = $data->creator_corporation_id ?? 0;
        $executorID = $data->executor_corporation_id ?? 0;
        $factionID = $data->faction_id ?? 0;
        $lastUpdated = $depth >= 2 ? (time() - 1209600) * 1000 : time() * 1000;
        return new Collection([
            'allianceID' => $allianceID,
            'allianceName' => $data->name,
            'factionID' => $factionID,
            'factionName' => $factionID > 0 ? $factions->findOne(['corporationID' => $factionID])->get('factionName') : '',
            'creatorID' => $creatorID,
            'creatorName' => $depth <= 2 ? $characters->getById($creatorID, $depth)->get('characterName', '') : '',
            'creatorCorporationID' => $creatorCorpID,
            'creatorCorporationName' => $depth <= 2 ? $corporations->getById($creatorCorpID, $depth)->get('corporationName', '') : '',
            'executorCorporationID' => $executorID,
            'executorCorporationName' => $depth <= 2 ? $corporations->getById($executorID, $depth)->get('corporationName', '') : '',
            'dateFounded' => new UTCDateTime(strtotime($data->date_founded) * 1000),
            'ticker' => $data->ticker,
            'lastUpdated' => new UTCDateTime($lastUpdated),
        ]);
    }
}
