<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Regions extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'regions';
    protected $indexField = 'regionID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByDescriptionID($descriptionID): Collection
    {
        return $this->find(['descriptionID' => $descriptionID]);
    }

    public function getAllByFactionID($factionID): Collection
    {
        return $this->find(['factionID' => $factionID]);
    }

    public function getAllByNameID($nameID): Collection
    {
        return $this->find(['nameID' => $nameID]);
    }

    public function getAllByRegionID($regionID): Collection
    {
        return $this->find(['regionID' => $regionID]);
    }

    public function getAllByRegionName($fieldName): Collection
    {
        return $this->find(['regionName' => $fieldName]);
    }
}
