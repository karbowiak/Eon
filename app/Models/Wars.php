<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Wars extends ModelInterface
{
    public $collectionName = 'wars';
    protected $indexField = 'warID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function exists(int $warID) {
        return $this->findOne(['warID' => $warID])->isNotEmpty();
    }

    public function needsUpdate(int $warID) {
        $war = $this->findOne(['warID' => $warID]);
        if ($war->get('timeFinished') !== null) {
            return false;
        }

        $timeDeclared = strtotime($war->get('timeDeclared'));

        return $timeDeclared > (time() - 2592000);
    }
}
