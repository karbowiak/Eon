<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Landmarks extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'landmarks';
    protected $indexField = 'landmarkID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByDescriptionId(int $descriptionId): Collection
    {
        return $this->find(['descriptionID' => $descriptionId]);
    }

    public function getAllByLandmarkId(int $landmarkId): Collection
    {
        return $this->find(['landmarkID' => $landmarkId]);
    }

    public function getAllByLandmarkNameId(int $landmarkNameId): Collection
    {
        return $this->find(['landmarkNameId' => $landmarkNameId]);
    }
}
