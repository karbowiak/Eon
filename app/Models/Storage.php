<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Storage extends ModelInterface
{
    public $collectionName = 'storage';
    protected $indexField = 'key';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function get(string $key, $default = null)
    {
        $data = $this->findOne(['key' => $key])->get('data');
        if ($data !== null) {
            return $data;
        }
        return $default;
    }

    public function set(string $key, $value)
    {
        $this->setData(['key' => $key, 'data' => $value]);
        $this->save();
    }
}
