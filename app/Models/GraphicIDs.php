<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class GraphicIDs extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'graphicIDs';
    protected $indexField = 'graphicsID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByGraphicsId(int $graphicsId): Collection
    {
        return $this->find(['graphicsID' => $graphicsId]);
    }
}
