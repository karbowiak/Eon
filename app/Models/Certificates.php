<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Certificates extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'certificates';
    protected $indexField = 'certificateID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByCertificateId(int $certificateId): Collection
    {
        return $this->find(['certificateID' => $certificateId]);
    }

    public function getAllByGroupId(int $groupId): Collection
    {
        return $this->find(['groupID' => $groupId]);
    }

    public function getAllByName(string $name): Collection
    {
        return $this->find(['name' => $name]);
    }
}
