<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class InvFlags extends ModelInterface
{
    public $collectionName = 'invFlags';
    public $databaseName = 'ccp';
    protected $indexField = 'solarSystemID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
}
