<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class GroupIDs extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'groupIDs';
    protected $indexField = 'groupID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByCategoryID(int $categoryID): Collection
    {
        return $this->find(['categoryID' => $categoryID]);
    }

    public function getAllByGroupID(int $groupID): Collection
    {
        return $this->findOne(['groupID' => $groupID]);
    }

    public function getAllByName(string $fieldName): Collection
    {
        return $this->find(['name.en' => $fieldName]);
    }

    public function getAllByGermanName(string $fieldName): Collection
    {
        return $this->find(['name.de' => $fieldName]);
    }

    public function getAllByEnglishName(string $fieldName): Collection
    {
        return $this->find(['name.en' => $fieldName]);
    }

    public function getAllByFrenchName(string $fieldName): Collection
    {
        return $this->find(['name.fr' => $fieldName]);
    }

    public function getAllByJapaneseName(string $fieldName): Collection
    {
        return $this->find(['name.ja' => $fieldName]);
    }

    public function getAllByRussianName(string $fieldName): Collection
    {
        return $this->find(['name.ru' => $fieldName]);
    }

    public function getAllByChineseName(string $fieldName): Collection
    {
        return $this->find(['name.zh' => $fieldName]);
    }
}
