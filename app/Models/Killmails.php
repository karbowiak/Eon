<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Killmails extends ModelInterface
{
    public $collectionName = 'killmails';
    protected $indexField = 'killID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function exists(int $killID): bool
    {
        return $this->findOne(['killID' => $killID])->isNotEmpty();
    }

    public function getKill(int $killID, string $hash): array
    {
        return json_decode(json_encode($this->findOne(['killID' => $killID, 'hash' => $hash])), true);
    }
}
