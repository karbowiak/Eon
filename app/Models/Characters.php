<?php

namespace Eon\Models;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Characters extends ModelInterface
{
    public $collectionName = 'characters';
    protected $indexField = 'characterID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getById(int $characterID, int $depth = 0): Collection
    {
        /** @var Collection $result */
        $result = $this->findOne(['characterID' => $characterID]);
        if ($result->isEmpty()) {
            $depth++;
            $result = $this->esi($characterID, $depth);
            if ($result !== null) {
                $this->setData($result->toArray());
                $this->save();
            }
        }

        return $result;
    }

    public function getByName(string $characterName): Collection
    {
        return $this->findOne(['characterName' => $characterName]);
    }

    public function getInformation(int $characterID): Collection
    {
        return $this->findOne(['characterID' => $characterID], ['projection' => ['_id' => 0]]);
    }

    public function esi(int $characterID, int $depth): ?Collection
    {
        /** @var \Eon\Helpers\ESIHelper $esi */
        $esi = $this->container->get('esi');
        /** @var \Eon\Models\Corporations $corporations */
        $corporations = $this->container->get('model/corporations');
        /** @var \Eon\Models\Alliances $alliances */
        $alliances = $this->container->get('model/alliances');

        $data = $esi->getCharacterData($characterID);
        if ($data === null) {
            return null;
        }

        $corporationID = $data->corporation_id ?? 0;
        $allianceID = $data->alliance_id ?? 0;

        $corporationInfo = $corporationID > 0 && $depth <= 2 ? $corporations->getById($corporationID, $depth) : new Collection();
        $allianceInfo = $allianceID > 0 && $depth <= 2 ? $alliances->getById($allianceID, $depth) : new Collection();
        $lastUpdated = $depth >= 2 ? (time() - 1209600) * 1000 : time() * 1000;
        return new Collection([
            'characterID' => $characterID,
            'characterName' => $data->name,
            'corporationID' => $data->corporation_id,
            'corporationName' => $corporationInfo->get('corporationName'),
            'allianceID' => $allianceID,
            'allianceName' => $allianceInfo->get('allianceName', ''),
            'securityStatus' => $data->security_status,
            'gender' => $data->gender,
            'lastUpdated' => new UTCDateTime($lastUpdated),
            'description' => $data->description,
            'history' => (new Collection($esi->handle('get', "/characters/{$characterID}/corporationhistory")->getArrayCopy()))->transform(function ($item) use ($corporations, $depth) {
                $corporationID = $item->corporation_id ?? 0;
                $corporationName = $depth <= 2 ? $corporations->getById($corporationID, $depth)->get('corporationName') : '';
                $res = [
                    'corporationID' => $corporationID,
                    'corporationName' => $corporationName,
                    'recordID' => $item->record_id,
                    'startDate' => new UTCDateTime(strtotime($item->start_date) * 1000),
                ];

                return $res;
            }),
        ]);
    }
}
