<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Factions extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'factions';
    protected $indexField = 'corporationID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
}
