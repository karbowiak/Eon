<?php

namespace Eon\Models;

use DateTime;
use Eon\Interfaces\ModelInterface;

//
class Participants extends ModelInterface
{
    public $collectionName = 'killmails';
    protected $indexField = 'killID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
    // Valid arguments
    public $validArguments = [
        'killTime',
        'solarSystemID',
        'solarSystemSecurity',
        'regionID',
        'shipValue',
        'fittingValue',
        'totalValue',
        'isNPC',
        'isSolo',
        'victim.shipTypeID',
        'victim.shipGroupID',
        'victim.characterID',
        'victim.corporationID',
        'victim.allianceID',
        'victim.factionID',
        'attackers.shipTypeID',
        'atttacker.shipGroupID',
        'attackers.weaponTypeID',
        'attackers.characterID',
        'attackers.corporationID',
        'attackers.allianceID',
        'attackers.factionID',
        'attackers.finalBlow',
        'items.typeID',
        'items.groupID',
        'items.categoryID',
    ];

    public function getByKillID(int $killID, int $cacheTime = 300): array
    {
        // Check if the killmail is in the cache, if it is, return it
        $md5 = md5(serialize($killID));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        // The killmail was not in the cache, time to get it from the db
        $killData = $this->findOne(['killID' => $killID]);
        $killData->killTime = date('Y-m-d H:i:s', (string) $killData->killTime / 1000);
        $this->redis->set($md5, $killData, $cacheTime);

        // Return the killmail
        return $killData;
    }

    public function getByHash(string $hash, int $cacheTime = 300): array
    {
        // Check if the killmail is in the cache, if it is, return it
        $md5 = md5(serialize($hash));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        // The killmail was not in the cache, time to get it from the db
        $killData = $this->findOne(['hash' => $hash]);
        $killData->killTime = date('Y-m-d H:i:s', (string) $killData->killTime / 1000);
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(md5(serialize($hash)), $killData, $cacheTime);

        // Return the killmail
        return $killData;
    }

    public function getByKillTime(
        $killTime,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($killTime . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $time = $this->makeTime($killTime);
        $extraArguments->killTime = $time;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->findOne($array);
        $killData->killTime = date('Y-m-d H:i:s', (string) $killData->killTime / 1000);
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getBySolarSystemID(
        int $solarSystemID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($solarSystemID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['solarSystemID'] = $solarSystemID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options']);
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        return $killData;
    }

    public function getByRegionID(
        int $regionID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($regionID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['regionID'] = $regionID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(
            md5(serialize($regionID . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getByVictimCharacterID(
        int $characterID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize('victim' . $characterID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['victim.characterID'] = $characterID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByAttackerCharacterID(
        int $characterID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($characterID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['attackers.characterID'] = $characterID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByVictimCorporationID(
        int $corporationID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize('victim' . $corporationID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['victim.corporationID'] = $corporationID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByAttackerCorporationID(
        int $corporationID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($corporationID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['attackers.corporationID'] = $corporationID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(
            md5(serialize($corporationID . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getByVictimAllianceID(
        int $allianceID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize('victim' . $allianceID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['victim.allianceID'] = $allianceID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByAttackerAllianceID(
        int $allianceID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($allianceID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }

        $extraArguments['attackers.allianceID'] = $allianceID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }

        $this->redis->set($md5, $killData, $cacheTime);
        return $killData;
    }

    public function getByVictimFactionID(
        int $factionID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize('victim' . $factionID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['victim.factionID'] = $factionID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByAttackerFactionID(
        int $factionID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($factionID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['attackers.factionID'] = $factionID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(
            md5(serialize($factionID . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getByVictimShipTypeID(
        int $shipTypeID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize('victim' . $shipTypeID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['victim.shipTypeID'] = $shipTypeID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getByAttackerShipTypeID(
        int $shipTypeID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($shipTypeID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['attackers.shipTypeID'] = $shipTypeID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(
            md5(serialize($shipTypeID . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getByAttackerWeaponTypeID(
        int $weaponTypeID,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($weaponTypeID . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $extraArguments['attackers.weaponTypeID'] = $weaponTypeID;
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);
        $this->redis->set(
            md5(serialize($weaponTypeID . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getAllKillsAfterDate(
        $afterDate = null,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($afterDate . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        if (is_int($afterDate)) {
            $time = $this->makeTimeFromUnixTime($afterDate);
        } else {
            $time = $this->makeTimeFromDateTime($afterDate);
        }
        $extraArguments->killTime = ['$gte' => $time];
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set(
            md5(serialize($afterDate . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getAllKillsBeforeDate(
        $beforeDate = null,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($beforeDate . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $time = $this->makeTime($beforeDate);
        $extraArguments->killTime = ['$lte' => $time];
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set(
            md5(serialize($beforeDate . implode('', $extraArguments) . $limit . $order . $offset)),
            $killData,
            $cacheTime
        );

        return $killData;
    }

    public function getAllKillsBetweenDates(
        $afterDate = null,
        $beforeDate = null,
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null
    ) {
        $md5 = md5(serialize($afterDate . $beforeDate . implode('', $extraArguments) . $limit . $order . $offset));
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $afterTime = $this->makeTime($afterDate);
        $beforeTime = $this->makeTime($beforeDate);
        $extraArguments->killTime = [
            '$gte' => $afterTime,
            '$lte' => $beforeTime,
        ];
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $killData = $this->find($array['filter'], $array['options'])->toArray();
        foreach ($killData as $key => $value) {
            $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    public function getAllKills(
        $extraArguments = [],
        $limit = 100,
        $cacheTime = 300,
        $order = 'DESC',
        $offset = null,
        array $extraOptions = []
    ) {
        $md5 = md5(serialize($extraArguments) . $limit . $order . $offset);
        if ($this->redis->has($md5)) {
            return $this->redis->get($md5, []);
        }
        $array = $this->generateQueryArray($extraArguments, $limit, $order, $offset);
        $options = array_merge($array['options'], $extraOptions);
        // @todo rewrite the rest of Participants to use Aggregate - for some reason mongodb is a billion times faster
        // @todo with aggregate (match + sort) than with find+sort.. don't ask me why.......
        if (isset($options['skip'])) {
            $aggregateArray = [
                ['$match' => $array['filter']],
                ['$sort' => $options['sort']],
                ['$skip' => $options['skip']],
                ['$limit' => $options['limit']],
            ];
        } else {
            $aggregateArray = [
                ['$match' => $array['filter']],
                ['$sort' => $options['sort']],
                ['$limit' => $options['limit']],
            ];
        }

        $killData = $this->aggregate($aggregateArray)->toArray();
        // Remove things we have in our projection
        if (isset($options['projection'])) {
            foreach ($killData as $key => $value) {
                foreach ($options['projection'] as $name => $proj) {
                    unset($killData[$key]->{$name});
                }
                // Fix the timestamp
                $killData[$key]->killTime = date(DateTime::ATOM, (string) $value->killTime / 1000);
            }
        }
        $this->redis->set($md5, $killData, $cacheTime);

        return $killData;
    }

    private function generateQueryArray(
        $extraArguments = [],
        int $limit = 100,
        string $order = 'DESC',
        int $offset = null
    ): array {
        // Mongo Array
        $queryArray = [];
        $dataArray = [];
        if (!empty($extraArguments)) {
            foreach ($this->validArguments as $arg) {
                if (isset($extraArguments[$arg])) {
                    $dataArray[$arg] = $extraArguments[$arg];
                }
            }
        }
        // Limit
        $queryArray['limit'] = $limit;
        // Order
        $queryArray['sort'] = ['killTime' => $order === 'DESC' ? -1 : 1];
        // Offset
        if ($offset > 0) {
            $queryArray['skip'] = $offset;
        }
        // Remove _id
        $queryArray['projection'] = ['_id' => 0];

        // Return the query array
        return ['filter' => $dataArray, 'options' => $queryArray];
    }

    public function verifyDate($date): bool
    {
        return (DateTime::createFromFormat('Y-m-d H:i:s', $date) !== false);
    }
}
