<?php

namespace Eon\Models;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Prices extends ModelInterface
{
    public $collectionName = 'prices';
    protected $indexField = 'typeID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getPriceByTypeId(int $typeID, string $date = null): Collection
    {
        $date = $date === null ? new UTCDateTime(time() * 1000) : new UTCDateTime(strtotime($date) * 1000);
        $data = $this->findOne(['typeID' => (int) $typeID, 'fromDate' => $date]);

        if ($data->isEmpty()) {
            $data = new Collection($this->aggregate([
                    ['$match' => ['typeID' => (int) $typeID]],
                    ['$sort' => ['fromDate' => -1]],
                    ['$limit' => 1],
                ])->toArray()[0] ?? []);
        }

        // If the price is empty, lets try and fetch it from ESI
        if ($data->get('price', 0) === 0 && $data->get('published') === true) {
            try {
                $data = json_decode(
                    file_get_contents('https://esi.evetech.net/latest/markets/10000002/history/?type_id=' . $typeID),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
                return new Collection(['price' => end($data)->lowest ?? 0]);
            } catch (\Exception $e) {
            }
        }
        return new Collection(['price' => $data->get('price', 0)]);
    }

    public function getAllByTypeId(int $typeID): Collection
    {
        return $this->findOne(['typeID' => (int) $typeID]);
    }
}
