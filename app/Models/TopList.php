<?php

namespace Eon\Models;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ModelInterface;

class TopList extends ModelInterface
{
    public $collectionName = 'killmails';
    protected $indexField = 'killID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function topCharacters(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topCharacters' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $data = $this->aggregate([
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'attackers.characterID' => ['$ne' => 0],
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$attackers.characterID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $character) {
            $data[$key]->name = $this->container->get('model/characters')->findOne(['characterID' => $character->id])->get('characterName');
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }

    public function topCorporations(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topCorporations' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $data = $this->aggregate([
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'attackers.corporationID' => ['$ne' => 0],
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$attackers.corporationID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $corporation) {
            $data[$key]->name = $this->container->get('model/corporations')->findOne(['corporationID' => $corporation->id])->get('corporationName');
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }

    public function topAlliances(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topAlliances' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $data = $this->aggregate([
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'attackers.allianceID' => ['$ne' => 0],
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$attackers.allianceID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $alliance) {
            $data[$key]->name = $this->container->get('model/alliances')->findOne(['allianceID' => $alliance->id])->get('allianceName');
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }

    public function topShips(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topShips' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $data = $this->aggregate([
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$attackers.shipTypeID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $shipType) {
            $typeName = (array) $this->container->get('model/typeids')->findOne(['typeID' => $shipType->id])->get('name');
            $data[$key]->name = $typeName['en'];
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }

    public function topSystems(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topSystems' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $aggregate = [
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$solarSystemID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ];
        $data = $this->aggregate($aggregate, ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $solarSystem) {
            $data[$key]->name = trim($this->container->get('model/solarsystems')->findOne(['solarSystemID' => $solarSystem->id])->get('solarSystemName'));
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }

    public function topRegions(string $attackerType, int $typeID, int $limit = 10): array
    {
        $md5 = md5('topRegions' . $attackerType . $typeID . $limit);
        if ($this->redis->has($md5)) {
            $returnData = $this->redis->get($md5, []);
            if (is_null($returnData)) {
                return [];
            }
            return $returnData;
        }
        $data = $this->aggregate([
            [
                '$match' => [
                    "attackers.{$attackerType}" => $typeID,
                    'killTime' => ['$gte' => new UTCDateTime((time() - 2592000) * 1000)],
                ],
            ],
            ['$unwind' => '$attackers'],
            ['$match' => ["attackers.{$attackerType}" => $typeID]],
            ['$group' => ['_id' => '$regionID', 'count' => ['$sum' => 1]]],
            ['$project' => ['_id' => 0, 'count' => '$count', 'id' => '$_id']],
            ['$sort' => ['count' => -1]],
            ['$limit' => $limit],
        ], ['allowDiskUse' => true, 'maxTimeMS' => 30000])->toArray();
        foreach ($data as $key => $region) {
            $data[$key]->name = trim($this->container->get('model/regions')->findOne(['regionID' => $region->id])->get('regionName'));
        }
        $this->redis->set($md5, $data, 3600);

        return $data;
    }
}
