<?php

namespace Eon\Models;

use Exception;
use RuntimeException;
use MongoDB\UpdateResult;
use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

/**
 * Class User
 * @package Eon\Models
 * @property $username Username
 * @property $email Email
 * @property $password Password
 */
class User extends ModelInterface
{
    public $collectionName = 'user';
    protected $indexField = 'email';
    protected $hidden = ['password'];
    protected $required = ['username', 'email', 'password'];
    protected $overrideHidden = false;

    /**
     * Get a single user by email
     *
     * @param string $email
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    public function get(string $email): Collection
    {
        return $this->find(['email' => $email]);
    }

    /**
     * Return the configuration array saved pr. user
     *
     * @param string $email
     *
     * @return mixed
     */
    public function getConfig(string $email)
    {
        return $this->find(['email' => $email])->get('config');
    }

    /**
     * Set a configuration for a user
     *
     * @param string $email
     * @param array $config
     *
     * @return \MongoDB\UpdateResult
     */
    public function setConfig(string $email, array $config = []): UpdateResult
    {
        $this->data = $config;
        $data = $this->find(['email' => $email]);
        $data['config'] = $config;

        return $this->update(['email' => $email], $data);
    }

    /**
     * Because the user has a password, we want to hash that password before we save it..
     *
     * @return \MongoDB\UpdateResult
     */
    public function save(): UpdateResult
    {
        $this->hasRequired();
        $this->hashPassword();
        try {
            return $this->collection->updateOne(
                ['email' => $this->data->get('email')],
                $this->data->toArray(),
                ['upsert' => true]
            );
        } catch (Exception $e) {
            throw new RuntimeException('Error: ' . $e->getMessage());
        }
    }

    /**
     * Password hashing function, simply just the default php password hashing function
     *
     * @param string|null $password
     *
     * @return string
     */
    public function hashPassword(?string $password = null): string
    {
        if ($password === null) {
            return $this->data['password'] = password_hash($this->data->get('password'), PASSWORD_DEFAULT);
        }

        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Password verifying function, simply just the default php password verifying function
     *
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function verifyPassword(string $email, string $password): bool
    {
        $hash = $this->find(['email' => $email], [], true)->get('password');

        return password_verify($password, $hash);
    }
}
