<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Comments extends ModelInterface
{
    public $collectionName = 'comments';
    protected $indexField = 'commentID';
    protected $hidden = [];
    protected $required = ['killID', 'name', 'comment', 'createdAt'];
    protected $overrideHidden = false;
}
