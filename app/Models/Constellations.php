<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Constellations extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'constellations';
    protected $indexField = 'constellationID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByConstellationId(int $constellationId): Collection
    {
        return $this->find(['constellationID' => $constellationId]);
    }

    public function getAllByConstellationName(string $name): Collection
    {
        return $this->find(['constellationName' => $name]);
    }

    public function getAllByNameId(int $nameId): Collection
    {
        return $this->find(['nameID' => $nameId]);
    }

    public function getAllByRegionId(int $regionId): Collection
    {
        return $this->find(['regionID' => $regionId]);
    }

    public function getAllByRegionName(string $name): Collection
    {
        return $this->find(['regionName' => $name]);
    }
}
