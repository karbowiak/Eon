<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class CategoryIDs extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'categoryIDs';
    protected $indexField = 'categoryID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByCategoryId(int $categoryId): Collection
    {
        return $this->find(['categoryID' => $categoryId]);
    }

    public function getAllByName(string $name): Collection
    {
        return $this->find(['name.en' => $name]);
    }

    public function getAllByGermanName(string $name): Collection
    {
        return $this->find(['name.de' => $name]);
    }

    public function getAllByEnglishName(string $name): Collection
    {
        return $this->find(['name.en' => $name]);
    }

    public function getAllByFrenchName(string $name): Collection
    {
        return $this->find(['name.fr' => $name]);
    }

    public function getAllByJapaneseName(string $name): Collection
    {
        return $this->find(['name.ja' => $name]);
    }

    public function getAllByRussianName(string $name): Collection
    {
        return $this->find(['name.ru' => $name]);
    }

    public function getAllByChineseName(string $name): Collection
    {
        return $this->find(['name.zh' => $name]);
    }
}
