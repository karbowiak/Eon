<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class SkinMaterials extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'skinMaterials';
    protected $indexField = 'skinMaterialID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByDisplayNameID($displayNameID): Collection
    {
        return $this->find(['displayNameID' => $displayNameID]);
    }

    public function getAllByMaterialSetID($materialSetID): Collection
    {
        return $this->find(['materialSetID' => $materialSetID]);
    }

    public function getAllBySkinMaterialID($skinMaterialID): Collection
    {
        return $this->find(['skinMaterialID' => $skinMaterialID]);
    }
}
