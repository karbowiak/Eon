<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class IconIDs extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'iconIDs';
    protected $indexField = 'iconID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByIconId(int $iconId): Collection
    {
        return $this->find(['iconID' => $iconId]);
    }
}
