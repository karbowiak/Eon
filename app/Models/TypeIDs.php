<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class TypeIDs extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'typeIDs';
    protected $indexField = 'typeID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByGroupID($groupID): Collection
    {
        return $this->find(['groupID' => $groupID]);
    }

    public function getAllByName($fieldName): Collection
    {
        return $this->findOne(['name.en' => $fieldName]);
    }

    public function getAllByTypeID($typeID): Collection
    {
        return $this->findOne(['typeID' => $typeID]);
    }

    public function getAllByGermanName($fieldName): Collection
    {
        return $this->findOne(['name.de' => $fieldName]);
    }

    public function getAllByEnglishName($fieldName): Collection
    {
        return $this->findOne(['name.en' => $fieldName]);
    }

    public function getAllByFrenchName($fieldName): Collection
    {
        return $this->findOne(['name.fr' => $fieldName]);
    }

    public function getAllByJapaneseName($fieldName): Collection
    {
        return $this->findOne(['name.ja' => $fieldName]);
    }

    public function getAllByRussianName($fieldName): Collection
    {
        return $this->findOne(['name.ru' => $fieldName]);
    }

    public function getAllByChineseName($fieldName): Collection
    {
        return $this->findOne(['name.zh' => $fieldName]);
    }
}
