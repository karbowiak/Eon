<?php

namespace Eon\Models;

use MongoDB\BSON\UTCDateTime;
use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class Corporations extends ModelInterface
{
    public $collectionName = 'corporations';
    protected $indexField = 'corporationID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getById(int $corporationID, int $depth = 0): Collection
    {
        /** @var Collection $result */
        $result = $this->findOne(['corporationID' => $corporationID]);
        if ($result->isEmpty()) {
            $depth++;
            $result = $this->esi($corporationID, $depth);
            $this->setData($result->toArray());
            $this->save();
        }

        return $result;
    }

    public function getByName(string $corporationName): Collection
    {
        return $this->findOne(['corporationName' => $corporationName]);
    }

    public function getInformation(int $corporationId): Collection
    {
        return $this->findOne(['corporationID' => $corporationId], ['projection' => ['_id' => 0]]);
    }

    public function getMembers(int $corporationId): Collection
    {
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');

        return $characters->find(['corporationID' => $corporationId], ['projection' => ['_id' => 0]]);
    }

    public function esi(int $corporationID, int $depth): Collection
    {
        /** @var \Eon\Helper\ESIHelper $esi */
        $esi = $this->container->get('esi');
        /** @var \Eon\Models\Characters $characters */
        $characters = $this->container->get('model/characters');
        /** @var \Eon\Models\Alliances $alliances */
        $alliances = $this->container->get('model/alliances');
        /** @var \Eon\Models\Factions $factions */
        $factions = $this->container->get('model/factions');

        $data = $esi->getCorporationData($corporationID);
        $allianceID = $data->alliance_id ?? 0;
        $factionID = $data->faction_id ?? 0;
        $dateFounded = $data->date_founded ?? '2003-01-01 00:00:00';

        $allianceInfo = $allianceID > 0 && $depth <= 2 ? $alliances->getById($allianceID, $depth) : new Collection();
        $factionInfo = $factionID > 0 ? $factions->findOne(['corporationID' => $factionID]) : new Collection();

        $ceoData = $data->ceo_id > 1999999 && $depth <= 2 ? $characters->getById($data->ceo_id, $depth): new Collection();
        $creatorData = $data->creator_id > 1999999 && $depth <= 2 ? $characters->getById($data->creator_id, $depth) : new Collection();
        $lastUpdated = $depth >= 2 ? (time() - 1209600) * 1000 : time() * 1000;
        return new Collection([
            'corporationID' => $corporationID,
            'corporationName' => $data->name,
            'allianceID' => $allianceID,
            'allianceName' => $allianceInfo->get('allianceName', ''),
            'factionID' => $factionID ?? 0,
            'factionName' => $factionInfo->get('factionName', ''),
            'ceoID' => $data->ceo_id,
            'ceoName' => $ceoData->get('characterName', ''),
            'creatorID' => $data->creator_id,
            'creatorName' => $creatorData->get('characterName', ''),
            'dateFounded' => new UTCDateTime(strtotime($dateFounded) * 1000),
            'ticker' => $data->ticker,
            'memberCount' => $data->member_count,
            'homeStationID' => $data->home_station_id,
            'homeStationName' => '',
            'taxRate' => $data->tax_rate,
            'warEligible' => $data->war_eligible ?? false,
            'description' => $data->description,
            'lastUpdated' => new UTCDateTime($lastUpdated),
        ]);
    }
}
