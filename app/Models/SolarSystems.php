<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class SolarSystems extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'solarSystems';
    protected $indexField = 'solarSystemID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByConstellationID($constellationID): Collection
    {
        return $this->find(['constellationID' => $constellationID]);
    }

    public function getAllByConstellationName($fieldName): Collection
    {
        return $this->find(['constellationName' => $fieldName]);
    }

    public function getAllByCorridor($corridor): Collection
    {
        return $this->find(['corridor' => $corridor]);
    }

    public function getAllByRegionID($regionID): Collection
    {
        return $this->find(['regionID' => $regionID]);
    }

    public function getAllByRegionName($fieldName): Collection
    {
        return $this->find(['regionName' => $fieldName]);
    }

    public function getAllBySolarSystemID($solarSystemID): Collection
    {
        return $this->findOne(['solarSystemID' => $solarSystemID]);
    }

    public function getAllBySolarSystemName($fieldName): Collection
    {
        return $this->find(['solarSystemName' => $fieldName]);
    }

    public function getAllBySolarSystemNameID($solarSystemNameID): Collection
    {
        return $this->find(['solarSystemNameID' => $solarSystemNameID]);
    }

    public function getAllByStarId($starId): Collection
    {
        return $this->find(['star.id' => $starId]);
    }

    public function getAllByStarTypeID($starTypeID): Collection
    {
        return $this->find(['star.typeID' => $starTypeID]);
    }

    public function getAllBySunTypeID($sunTypeID): Collection
    {
        return $this->find(['sunTypeID' => $sunTypeID]);
    }
}
