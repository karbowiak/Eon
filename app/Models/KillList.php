<?php

namespace Eon\Models;

use DateTime;

class KillList extends Participants
{
    public function getLatest($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $killData = $this->collection->find([], [
            'sort' => ['killTime' => -1],
            'projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0],
            'skip' => $offset,
            'limit' => $limit,
        ])->toArray();
        return $killData;
    }

    public function getBigKills($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        /** @var \Eon\Models\TypeIDs $typeIDs */
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        return $this->getAllKills(['victim.shipGroupID' => ['$in' => [547, 485, 513, 902, 941, 30, 659]]], $limit, 30, 'DESC', $offset, $extras);
    }

    public function getWSpace($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        /** @var \Eon\Models\TypeIDs $typeIDs */
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['regionID' => ['$gte' => 11000001, '$lte' => 11000033]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getHighSec($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        /** @var \Eon\Models\TypeIDs $typeIDs */
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['solarSystemSecurity' => ['$gte' => 0.45]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getAbyssal($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        /** @var \Eon\Models\TypeIDs $typeIDs */
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];
        
        $data = $this->getAllKills(['regionID' => ['$gte' => 12000000, '$lte' => 13000000]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getLowSec($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['solarSystemSecurity' => ['$lte' => 0.45, '$gte' => 0]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getNullSec($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['solarSystemSecurity' => ['$lte' => 0]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getSolo($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['isSolo' => true], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getNPC($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['isNPC' => true], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function get5b($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['totalValue' => ['$gte' => 5000000000]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function get10b($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['totalValue' => ['$gte' => 10000000000]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getCitadels($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [1657]]], $limit, 30, 'DESC', $offset, $extras);
        return $data;
    }

    public function getT1($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [419, 27, 29, 547, 26, 420, 25, 28, 941, 463, 237, 31]]], $limit, 30, 'DESC', $offset, $extras);
        return $data;
    }

    public function getT2($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [ 324, 898, 906, 540, 830, 893, 543, 541, 833, 358, 894, 831, 902, 832, 900, 834, 380 ]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getT3($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [963, 1305]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getFrigates($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [324, 893, 25, 831, 237]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getDestroyers($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [420, 541]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getCruisers($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [906, 26, 833, 358, 894, 832, 963]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getBattleCruisers($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [419, 540]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getBattleShips($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [27, 898, 900]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getCapitals($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [547, 485]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getFreighters($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [513, 902]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getSuperCarriers($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [659]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }

    public function getTitans($page = 1): array
    {
        $limit = 100;
        $offset = $limit * ($page - 1);
        $extras = ['projection' => ['_id' => 0, 'items' => 0, 'osmium' => 0]];

        $data = $this->getAllKills(['victim.shipGroupID' => ['$in' => [30]]], $limit, 30, 'DESC', $offset, $extras);
        
        return $data;
    }
}
