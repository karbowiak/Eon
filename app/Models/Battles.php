<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class Battles extends ModelInterface
{
    public $collectionName = 'battles';
    protected $indexField = 'battleID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
}
