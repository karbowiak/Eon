<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;
use Tightenco\Collect\Support\Collection;

class TournamentRuleSets extends ModelInterface
{
    public $databaseName = 'ccp';
    public $collectionName = 'tournamentRuleSets';
    protected $indexField = 'ruleSetID';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;

    public function getAllByRuleSetID($ruleSetID): Collection
    {
        return $this->find(['ruleSetID' => $ruleSetID]);
    }

    public function getAllByRuleSetName($fieldName): Collection
    {
        return $this->find(['ruleSetName' => $fieldName]);
    }
}
