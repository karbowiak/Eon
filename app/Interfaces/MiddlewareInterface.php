<?php

namespace Eon\Interfaces;

use League\Container\Container;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

abstract class MiddlewareInterface implements \Psr\Http\Server\MiddlewareInterface
{
    abstract public function __construct(Container $container);
    abstract public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface;
}
