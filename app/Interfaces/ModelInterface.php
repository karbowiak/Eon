<?php

namespace Eon\Interfaces;

use Eon\Helpers\Mongo;

abstract class ModelInterface extends Mongo
{
    public $collectionName;
    protected $hidden;
    protected $required;
    protected $overrideHidden;
}
