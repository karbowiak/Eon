<?php

namespace Eon\Interfaces;

use Swoole\Table;
use Eon\Bootstrap;
use Swoole\WebSocket\Server;

abstract class WebsocketInterface
{
    /**
     * @var string
     */
    public $endpoint = '';
    /**
     * @var \Eon\Bootstrap
     */
    protected $container;
    /**
     * @var \Swoole\Table
     */
    protected $clients;
    /**
     * @var \Swoole\WebSocket\Server
     */
    protected $server;

    /**
     * WebsocketInterface constructor.
     *
     * @param \Eon\Bootstrap $container
     * @param \Swoole\WebSocket\Server $server
     */
    public function __construct(Bootstrap $container, Server $server)
    {
        $this->container = $container;
        $this->clients = new Table(1024);
        $this->clients->column('fd', Table::TYPE_INT);
        $this->clients->column('data', Table::TYPE_STRING, 2048);
        $this->clients->create();
        $this->server = $server;
    }

    /**
     * @param $data
     */
    abstract public function handle($data): void;

    /**
     * @param $fd
     * @param $data
     */
    public function subscribe($fd, $data): void
    {
        $this->clients[$fd] = ['fd' => $fd, 'data' => json_encode($data)];
    }

    /**
     * @param $fd
     */
    public function unsubscribe($fd): void
    {
        $this->clients->del($fd);
    }

    /**
     * @param string $encodedData
     */
    public function sendAll($encodedData): void
    {
        foreach ($this->clients as $fd => $client) {
            if ($this->server->exist($fd)) {
                $this->server->push($fd, $encodedData);
            } else {
                $this->clients->del($fd);
            }
        }
    }

    /**
     * @param string $data
     *
     * @return bool
     */
    protected function isJson(string $data): bool
    {
        json_decode($data);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
