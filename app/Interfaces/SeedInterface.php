<?php

namespace Eon\Interfaces;

use PDO;
use Eon\Bootstrap;
use Symfony\Component\Yaml\Yaml;

abstract class SeedInterface
{
    protected $collectionName = '';
    protected $fileName = '';
    protected $container;

    public function __construct(Bootstrap $bootstrap)
    {
        $this->container = $bootstrap->getContainer();
    }

    /**
     * @param string $path
     *
     * @return mixed
     */
    public function getData(string $path = '', bool $dontUseSymfony = false)
    {
        if (!empty($path)) {
            $data = file_get_contents(__DIR__ . '/../../resources/cache/sde/fsd/' . $path);
        } else {
            $data = file_get_contents(__DIR__ . '/../../resources/cache/sde/fsd/' . $this->fileName . '.yaml');
        }

        if ($dontUseSymfony) {
            return yaml_parse($data);
        }
        return Yaml::parse($this->treatData($data));
    }

    /**
     * @return \PDO
     */
    public function getSqlite(): PDO
    {
        return new PDO('sqlite:' . __DIR__ . '/../../resources/cache/sqlite-latest.sqlite');
    }

    /**
     *
     */
    abstract public function execute(): void;

    public function treatData(string $data): string
    {
        return $data;
    }
}
