<?php

namespace Eon\Interfaces;

use Eon\Bootstrap;
use Know\Swoole\Queue\JobInterface;

abstract class QueueInterface implements JobInterface
{
    protected $data;
    public $logFile = 'queue.log';
    public $container = null;
    public $echo = false;
    public $queueName = '';
    public $cronTime = '';

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle(): void
    {
    }

    public function output(bool $enable)
    {
        $this->echo = $enable;
    }

    public function getContainer(): Bootstrap
    {
        if ($this->container === null) {
            require_once __DIR__ . '/../../vendor/autoload.php';
            $container = new Bootstrap();
            $container->initContainer();
            $this->container = $container;
        }

        return $this->container;
    }

    public function log(string $log, bool $echo = false): void
    {
        $boom = explode('\\', get_class($this));
        $currentClass = end($boom);
        $string = date('Y-m-d H:i:s') . ' | ' . $currentClass . ' | ' . $log . "\n";
        /** @var \Eon\Helpers\Config */
        $config = $this->getContainer()->get('config');
        $stdout = $config->get('app/logStdout', false);
        if ($this->echo === true || $stdout === true || $echo === true) {
            echo $string;
        } else {
            file_put_contents(__DIR__ . '/../../resources/logs/' . $this->logFile, $string, FILE_APPEND);
        }
    }
}
