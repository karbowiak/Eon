<?php

namespace Eon\Interfaces;

use Eon\Helpers\Command;
use Symfony\Component\Console\Helper\ProgressBar;

abstract class CommandsInterface extends Command
{
    protected string $signature;

    protected string $description;

    public function progressBar(int $count)
    {
        return new ProgressBar($this->output, $count);
    }

    public function handle(): void
    {
    }
}
