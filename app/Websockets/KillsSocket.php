<?php
namespace Eon\Websockets;

use Eon\Interfaces\WebsocketInterface;
use Tightenco\Collect\Support\Collection;

class KillsSocket extends WebsocketInterface
{
    public $endpoint = '/kills';

    /**
     * @param Collection $data
     */
    public function handle($data): void
    {
        $this->sendAll($data);
    }

    /**
     * {"type": "subscription", "data": ['victim.characterID:12345678', 'attackers.corporationID:87654321']}
     *
     * @param array $encodedData
     */
    public function sendAll($encodedData): void
    {
        foreach ($this->clients as $fd => $data) {
            $subscriptions = $this->isJson($data['data'] ?? null) ? json_decode($data['data'], true) : [];
            if (!empty($subscriptions)) {
                foreach ($subscriptions as $subscription) {
                    $exp = explode(':', $subscription);
                    $match = $exp[0] ?? null;
                    $id = $exp[1] ?? 0;
                    $col = new \Adbar\Dot($encodedData);
                    if ($this->server->exist($fd) && $col->get($match) == $id) {
                        $this->server->push($fd, json_encode($encodedData));
                    } elseif (!$this->server->exist($fd)) {
                        $this->clients->del($fd);
                    }
                }
            } else {
                if ($this->server->exist($fd)) {
                    $this->server->push($fd, json_encode($encodedData));
                } else {
                    $this->clients->del($fd);
                }
            }
        }
    }
}
