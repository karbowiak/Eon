<?php
namespace Eon\Websockets;

use Eon\Interfaces\WebsocketInterface;
use Tightenco\Collect\Support\Collection;

class CommentsSocket extends WebsocketInterface
{
    public $endpoint = '/comments';

    /**
     * @param Collection $data
     */
    public function handle($data): void
    {
        $this->sendAll(json_encode($data));
    }
}
