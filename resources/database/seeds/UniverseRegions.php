<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class UniverseRegions extends SeedInterface
{
    protected $collectionName = 'regions';
    protected $fileName = 'regions';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        $locations = glob(__DIR__ . '/../../cache/sde/fsd/universe/*/*/*.staticdata');
        foreach ($locations as $location) {
            $data = yaml_parse_file($location);
            $explosion = array_values(array_slice(explode('/', $location), -3, 3, true));
            $data = array_merge($data, [
                'regionName' => ltrim(preg_replace('/(?<! )[A-Z]/', ' $0', $explosion[1])),
            ]);
            $collection->setData($data);
            $collection->save();
        }
    }
}
