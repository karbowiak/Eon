<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class UniverseSystems extends SeedInterface
{
    protected $collectionName = 'solarsystems';
    protected $fileName = 'solarSystems';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        $locations = glob(__DIR__ . '/../../cache/sde/fsd/universe/*/*/*/*/*.staticdata');
        foreach ($locations as $location) {
            $data = yaml_parse_file($location);
            $explosion = array_values(array_slice(explode('/', $location), -5, 5, true));
            $regionData = yaml_parse_file(__DIR__ . "/../../cache/sde/fsd/universe/{$explosion[0]}/{$explosion[1]}/region.staticdata");
            $constellationData = yaml_parse_file(__DIR__ . "/../../cache/sde/fsd/universe/{$explosion[0]}/{$explosion[1]}/{$explosion[2]}/constellation.staticdata");
            $data = array_merge([
                'regionID' => $regionData['regionID'],
                'regionName' => ltrim(preg_replace('/(?<! )[A-Z]/', ' $0', $explosion[1])),
                'constellationID' => $explosion[2],
                'constellationName' => $constellationData['constellationID'],
                'solarSystemName' => $explosion[3],
            ], $data);
            $collection->setData($data);
            $collection->save();
        }
    }
}
