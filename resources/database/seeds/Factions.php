<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class Factions extends SeedInterface
{
    protected $collectionName = 'factions';
    protected $fileName = 'factions';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();

        $yaml = yaml_parse_file(__DIR__ . '/../../cache/sde/bsd/chrFactions.yaml');
        foreach ($yaml as $data) {
            $collection->setData($data);
            $collection->save();
        }
    }
}
