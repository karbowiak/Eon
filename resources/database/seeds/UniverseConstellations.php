<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class UniverseConstellations extends SeedInterface
{
    protected $collectionName = 'constellations';
    protected $fileName = 'constellations';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        $locations = glob(__DIR__ . '/../../cache/sde/fsd/universe/*/*/*/*.staticdata');
        foreach ($locations as $location) {
            $data = yaml_parse_file($location);
            $explosion = array_values(array_slice(explode('/', $location), -4, 4, true));
            $regionData = yaml_parse_file(__DIR__ . "/../../cache/sde/fsd/universe/{$explosion[0]}/{$explosion[1]}/region.staticdata");
            $data = array_merge([
                'regionID' => $regionData['regionID'],
                'regionName' => ltrim(preg_replace('/(?<! )[A-Z]/', ' $0', $explosion[1])),
                'constellationName' => $explosion[2],
            ], $data);
            $collection->setData($data);
            $collection->save();
        }
    }
}
