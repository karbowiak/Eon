<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class CategoryIDs extends SeedInterface
{
    protected $collectionName = 'categoryids';
    protected $fileName = 'categoryIDs';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['categoryID' => $key], $data));
            $collection->save();
        }
    }
}
