<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class TournamentRuleSets extends SeedInterface
{
    protected $collectionName = 'tournamentrulesets';
    protected $fileName = 'tournamentRuleSets';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $data) {
            $collection->setData($data);
            $collection->save();
        }
    }
}
