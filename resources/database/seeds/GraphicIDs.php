<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class GraphicIDs extends SeedInterface
{
    protected $collectionName = 'graphicids';
    protected $fileName = 'graphicIDs';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['graphicsID' => $key], $data));
            $collection->setData($data);
            $collection->save();
        }
    }
}
