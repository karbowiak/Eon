<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class IconIDs extends SeedInterface
{
    protected $collectionName = 'iconids';
    protected $fileName = 'iconIDs';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['iconID' => $key], $data));
            $collection->setData($data);
            $collection->save();
        }
    }
}
