<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class TypeIDs extends SeedInterface
{
    protected $collectionName = 'typeids';
    protected $fileName = 'typeIDs';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData('', true) as $key => $data) {
            $collection->setData(array_merge(['typeID' => $key], $data));
            $collection->save();
        }
    }
}
