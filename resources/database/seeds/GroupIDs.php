<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class GroupIDs extends SeedInterface
{
    protected $collectionName = 'groupids';
    protected $fileName = 'groupIDs';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['groupID' => $key], $data));
            $collection->setData($data);
            $collection->save();
        }
    }
}
