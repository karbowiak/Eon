<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class SkinMaterials extends SeedInterface
{
    protected $collectionName = 'skinmaterials';
    protected $fileName = 'skinMaterials';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['skinMaterialID' => $key], $data));
            $collection->setData($data);
            $collection->save();
        }
    }
}
