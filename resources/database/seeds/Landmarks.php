<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class Landmarks extends SeedInterface
{
    protected $collectionName = 'landmarks';
    protected $fileName = 'landmarks/landmarks.staticdata';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData($this->fileName) as $key => $data) {
            $collection->setData(array_merge(['landmarkID' => $key], $data));
            $collection->save();
        }
    }
}
