<?php

namespace Eon\Resources\Database\Seeds;

use PDO;
use Eon\Interfaces\SeedInterface;

class UniverseCelestials extends SeedInterface
{
    protected $collectionName = 'celestials';
    protected $fileName = 'celestials';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        $sqlite = $this->getSqlite();
        $query = 'SELECT
            `mapDenormalize`.`itemID` AS `itemID`,
            `mapDenormalize`.`itemName` AS `itemName`,
            `invTypes`.`typeName` AS `typeName`,
            `mapDenormalize`.`typeID` AS `typeID`,
            `mapSolarSystems`.`solarSystemName` AS `solarSystemName`,
            `mapDenormalize`.`solarSystemID` AS `solarSystemID`,
            `mapDenormalize`.`constellationID` AS `constellationID`,
            `mapDenormalize`.`regionID` AS `regionID`,
            `mapRegions`.`regionName` AS `regionName`,
            `mapDenormalize`.`orbitID` AS `orbitID`,
            `mapDenormalize`.`x` AS `x`,
            `mapDenormalize`.`y` AS `y`,
            `mapDenormalize`.`z` AS `z` from
            ((((`mapDenormalize`
                join `invTypes` on((`mapDenormalize`.`typeID` = `invTypes`.`typeID`)))
                join `mapSolarSystems` on((`mapSolarSystems`.`solarSystemID` = `mapDenormalize`.`solarSystemID`)))
                join `mapRegions` on((`mapDenormalize`.`regionID` = `mapRegions`.`regionID`)))
                join `mapConstellations` on((`mapDenormalize`.`constellationID` = `mapConstellations`.`constellationID`))
            )';
        $stmt = $sqlite->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        foreach ($result as $celestial) {
            $collection->setData([
                'itemID' => (int) $celestial['itemID'],
                'itemName' => $celestial['itemName'],
                'typeName' => $celestial['typeName'],
                'typeID' => (int) $celestial['typeID'],
                'solarSystemName' => $celestial['solarSystemName'],
                'solarSystemID' => (int) $celestial['solarSystemID'],
                'constellationID' => (int) $celestial['constellationID'],
                'regionID' => (int) $celestial['regionID'],
                'regionName' => ltrim(preg_replace('/(?<! )[A-Z]/', ' $0', $celestial['regionName'])),
                'orbitID' => (int) $celestial['orbitID'],
                'x' => (float) $celestial['x'],
                'y' => (float) $celestial['y'],
                'z' => (float) $celestial['z'],
            ]);
            $collection->save();
        }
    }
}
