<?php

namespace Eon\Resources\Database\Seeds;

use PDO;
use Eon\Interfaces\SeedInterface;

class InvFlags extends SeedInterface
{
    protected $collectionName = 'invflags';
    protected $fileName = 'invFlags';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        $sqlite = $this->getSqlite();
        $query = 'SELECT * FROM invFlags';
        $stmt = $sqlite->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        foreach ($result as $flag) {
            $collection->setData([
                'flagID' => (int)$flag[ 'flagID' ],
                'flagName' => $flag[ 'flagName' ],
                'flagText' => $flag[ 'flagText' ],
                'orderID' => (int)$flag[ 'orderID' ],
            ]);
            $collection->save();
        }
    }
}
