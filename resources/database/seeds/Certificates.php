<?php

namespace Eon\Resources\Database\Seeds;

use Eon\Interfaces\SeedInterface;

class Certificates extends SeedInterface
{
    protected $collectionName = 'certificates';
    protected $fileName = 'certificates';

    public function execute(): void
    {
        /** @var \Eon\Models\Blueprints $collection */
        $collection = $this->container->get("model/{$this->collectionName}");
        $collection->truncate();
        foreach ($this->getData() as $key => $data) {
            $collection->setData(array_merge(['certificateID' => $key], $data));
            $collection->setData($data);
            $collection->save();
        }
    }
}
