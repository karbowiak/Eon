<?php

namespace Eon\Cron;

use Eon\Interfaces\QueueInterface;
use League\Container\Container;

class <<CLASSNAME>> extends QueueInterface {
    /** @var array */
    protected $data;
    /** @var \League\Container\Container */
    protected $container;
    public function __construct($data, Container $container) {
        $this->data = $data;
        $this->container = $container;
    }

    public function handle(): void {
    }
}
