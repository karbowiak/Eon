<?php

namespace Eon\Controllers;

use Eon\Interfaces\ControllerInterface;
use Psr\Http\Message\ResponseInterface;

class <<CLASSNAME>> extends ControllerInterface {
    public function handle(): ResponseInterface {
    }
}
