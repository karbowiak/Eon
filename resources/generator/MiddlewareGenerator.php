<?php

namespace Eon\Middleware;

use Eon\Interfaces\MiddlewareInterface;
use League\Container\Container;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class <<CLASSNAME>> extends MiddlewareInterface {
    public function __construct(Container $container) {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $response = $handler->handle($request);

        // Your code here

        // Return response
        return $response;
    }
}
