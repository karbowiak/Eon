<?php

namespace Eon\Models;

use Eon\Interfaces\ModelInterface;

class <<CLASSNAME>> extends ModelInterface {
    public $collectionName = '<<COLLECTIONNAME>>';
    protected $hidden = [];
    protected $required = [];
    protected $overrideHidden = false;
}
